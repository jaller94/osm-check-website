---
title: "YouBikes on OpenStreetMap"
date: 2024-03-23T00:50:00+08:00
lastmod: 2024-03-23T02:15:00+08:00
tags: [YouBike, Public Transport]
areas: [Taiwan]
---

<!--more-->

* URL: https://overpass-api.de/api/interpreter
* License: ODbL
  * Compatible with OpenStreetMap: yes

## Query

```
/*
This has been generated by the overpass-turbo wizard.
The original search was:
“rental=city_bike in Taiwan”
*/
[out:json][timeout:25];
// fetch area “Taiwan” to search in
area(id:3600449220)->.searchArea;
// gather results
nwr["rental"="city_bike"](area.searchArea);
// print results
out geom;
```
