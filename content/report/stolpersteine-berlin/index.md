---
title: "Stolpersteine in Berlin"
date: 2024-09-16T20:00:00+02:00
lastmod: 2025-03-10T19:59:44.551Z
tags: [Memorials]
areas: [Germany]
datasets: [stolpersteine-berlin]
---

<!--START-->
An analysis of stolpersteine on OpenStreetMap in Berlin.
<!--more-->
* 3433 stolpersteine
## Missing tags
* 16 without a name
* 628 without an inscription
* 860 without a website
## High-quality tags
* 6 link to a Wikipedia article
* 295 link to a Wikidata entry of the stolperstein
* 106 link to a Wikidata entry of the person
## Tags to avoid
* 2 have an URL (please use `website` or `image`)
* 179 have an `addr:*` (please use `memorial:addr:*`)
## No name
* [1769856845](https://osm.org/node/1769856845)
* [1769856850](https://osm.org/node/1769856850)
* [1769856857](https://osm.org/node/1769856857)
* [1769856859](https://osm.org/node/1769856859)
* [1769856860](https://osm.org/node/1769856860)
* [3531015207](https://osm.org/node/3531015207)
* [5176773541](https://osm.org/node/5176773541)
* [6299277587](https://osm.org/node/6299277587)
* [9676046187](https://osm.org/node/9676046187)
* [12044069290](https://osm.org/node/12044069290)
* [12044072806](https://osm.org/node/12044072806)
* [12269162736](https://osm.org/node/12269162736)
* [12269171697](https://osm.org/node/12269171697)
* [12329220493](https://osm.org/node/12329220493)
* [12389823159](https://osm.org/node/12389823159)
* [12389823160](https://osm.org/node/12389823160)
## Grouped by website
* https://www.stolpersteine.eu/ (2 times)
  * [Fam. Jacoby](https://osm.org/node/10670080334)
  * [Fam. Adler](https://osm.org/node/10674077333)
## Grouped by URL

<!--END-->
