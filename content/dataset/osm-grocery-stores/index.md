---
title: "Grocery stores in Taiwan on OpenStreetMap"
date: 2024-04-29T23:50:00+08:00
lastmod: 2024-04-29T23:50:00+08:00
tags: [Businesses]
areas: [Taiwan]
---

<!--more-->

* URL: https://overpass-api.de/api/interpreter
* License: ODbL
  * Compatible with OpenStreetMap: yes

## Query

```
nwr["shop"="chemist"](area.searchArea);
nwr["shop"="convenience"](area.searchArea);
nwr["shop"="supermarket"](area.searchArea);
```
