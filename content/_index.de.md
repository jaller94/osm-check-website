Diese Webseite überprüft die Korrektheit von OpenStreetMap-Daten. Die Ergebnisse werden als [Reporte](/report) veröffentlicht.

## Hinter den Kulissen
Lerne über die [Datensätze](/dataset) oder [sende eine Idee oder melde ein Problem](https://gitlab.com/jaller94/osm-check-website/-/issues), um diese Webseite zu verbessern.
