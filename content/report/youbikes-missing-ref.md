---
title: "YouBike stations without ref"
date: 2024-03-19T10:00:00+08:00
lastmod: 2025-03-10T19:59:24.462Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
---

Does every on OpenStreetMap YouBike station have a ref=* value?

<!-- ✅ Yes, it does.

⚠️ No, there are 5 values being reused.

🗃️ This page has not been updated in a while.

🏗️ This page is new or broken. It's likely inaccurate. -->

<!--START-->
⚠️ No, 4 stations have no ref=* tag.

<!--more-->

* [中央陸橋 (中央一街)](https://osm.org/node/11536899498)
* [臺大禮賢樓西側](https://osm.org/node/12243937343)
* [繽紛之丘(青溪二路)](https://osm.org/node/12422523359)
* [Xinyonghe Market](https://osm.org/relation/18420616)

<!--END-->
