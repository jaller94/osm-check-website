---
title: "Grocery stores in Taiwan"
date: 2024-03-26T23:00:00+08:00
lastmod: 2025-03-10T19:59:27.201Z
tags: [Businesses]
areas: [Taiwan]
datasets: [osm-grocery-stores]
---

<!--START-->
An analysis of grocery stores in Taiwan.
<!--more-->
## Shop types
  * 12245 of convenience
  * 2657 of supermarket
## OpenStreetMap types
  * 14299 of node
  * 1714 of way
  * 5 of relation

## Brands
### `brand`
  * 5647 of 7-Eleven
  * 3399 of 全家便利商店
  * 1998 of undefined
  * 1110 of 萊爾富
  * 976 of 全聯福利中心
  * 577 of OK超商
  * 471 of 美廉社
  * 429 of 蝦皮店到店
  * 365 of 屈臣氏
  * 269 of 康是美
  * 200 of 寶雅
  * 174 of 家樂福超市
  * 93 of 家樂福
  * 91 of 丁丁藥局
  * 39 of 義美食品
  * 24 of 台灣楓康超市
  * 19 of 大潤發
  * 19 of 楓康超市
  * 15 of 小北百貨
  * 14 of 里仁
  * 12 of 愛買
  * 11 of 聖德科斯
  * 10 of 喜互惠
  * 8 of Jason's Marketplace
  * 6 of 頂好
  * 4 of 日藥本舖
  * 4 of Carrefour Market
  * 3 of Mia C'bon
  * 2 of 松本清
  * 2 of Watsons
  * 2 of Circle K
  * 2 of 台灣中油
  * 2 of 新東陽
  * 2 of 億客成
  * 2 of 裕毛屋
  * 2 of Costco
  * 2 of Carrefour
  * 2 of 大買家
  * 1 of 大樹藥局
  * 1 of POYA
  * 1 of 全家
  * 1 of 家樂福;Carrefour
  * 1 of 全聯
  * 1 of 生活工場
  * 1 of 仁愛眼鏡
  * 1 of 頂好超市
  * 1 of 南崁聯福利中心
  * 1 of 中聯百貨
  * 1 of ロピア
### `brand:en`
  * 5145 of 7-Eleven
  * 3399 of FamilyMart
  * 2681 of undefined
  * 1096 of Hi-Life
  * 910 of Pxmart
  * 579 of OK mart
  * 462 of Simple Mart
  * 429 of Shopee Xpress
  * 363 of Watsons
  * 263 of COSMED
  * 184 of POYA
  * 174 of Carrefour Market
  * 95 of Carrefour
  * 75 of Tin Tin Drugstore
  * 64 of PX mart
  * 19 of Funcom Supermarket
  * 17 of RT-Mart
  * 16 of Poya
  * 10 of Leezen
  * 9 of Santa Cruz
  * 8 of Watson
  * 5 of Wellcome
  * 5 of A.mart
  * 4 of jp medical
  * 2 of Matsumoto Kiyoshi
  * 2 of CPC Corporation, Taiwan
  * 1 of Great Tree Pharmacy
  * 1 of Simply Mart
### `brand:wikidata`
  * 5635 of Q259340
  * 3189 of Q10891564
  * 2551 of undefined
  * 1043 of Q11326216
  * 930 of Q7262792
  * 536 of Q10851968
  * 462 of Q15914017
  * 429 of Q109676747
  * 365 of Q7974785
  * 267 of Q11063876
  * 182 of Q15927734
  * 178 of Q2689639
  * 83 of Q3117359
  * 75 of Q108166589
  * 19 of Q126368246
  * 19 of Q10915548
  * 17 of Q7277802
  * 9 of Q3268010
  * 8 of Q4648764
  * 4 of Q131688453
  * 4 of Q706247
  * 4 of Q217599
  * 3 of Q8014776
  * 2 of Q1191685
  * 2 of Q698024
  * 1 of Q108167767
  * 1 of Q11350624

<!--END-->
