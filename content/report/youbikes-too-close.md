---
title: "Duplicate YouBike stations by their position"
date: 2024-03-19T10:00:00+08:00
lastmod: 2025-03-10T19:59:24.473Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
datasets: [YouBikes on OSM]
---

Does every YouBike station without a ref=* value have no nearby duplicates?

<!--START-->
✅ Yes, it does.

<!--more-->

Nice job! There are no apparent duplicates.
<!--END-->
