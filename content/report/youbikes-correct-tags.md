---
title: "No invalid values on YouBike stations"
date: 2024-03-22T02:10:00+08:00
lastmod: 2025-03-10T19:59:26.386Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
datasets: [osm-youbikes]
---

Does every YouBike station have valid values?

<!--START-->
✅ Yes, it does.

<!--more-->

Nice work! No invalid values on stations in OSM have been found.
<!--END-->
