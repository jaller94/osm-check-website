---
title: "Duplicate YouBike stations by ref"
date: 2024-03-18T22:07:00+08:00
lastmod: 2025-03-10T19:59:24.460Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
datasets: [osm-youbikes]
---

Does every YouBike station on OpenStreetMap have a unique ref=* value?

<!--START-->
⚠️ No, there are 44 values being reused.

<!--more-->

## 131
* [Labor Recreation Center](https://osm.org/node/3222270189)
* [Daodong Tutorial Academy](https://osm.org/node/12141762326)
## 132
* [Pingtung City Office](https://osm.org/node/3222270181)
* [Pingtung City Office](https://osm.org/node/11771503985)
* [Pingtung City Office](https://osm.org/node/11771503986)
* [Hedong Elementary School](https://osm.org/node/12141762325)
## 133
* [Pingtung Night Market](https://osm.org/node/3222270179)
* [Sanduo Park](https://osm.org/node/12141762324)
## 134
* [NPTU Pingshang Campus](https://osm.org/node/3222270165)
* [Sanhua Park](https://osm.org/node/12141762323)
## 135
* [Pingtung Art Museum](https://osm.org/node/3222270173)
* [National Yuanlin Senior High School](https://osm.org/node/12141762322)
## 136
* [Pingtung Girl’s Senior High School](https://osm.org/node/3222270172)
* [Triangle Park](https://osm.org/node/12141762321)
## 137
* [Pingtung County Library](https://osm.org/node/3222270169)
* [Yuanlin Precinct Changhua County Police Bureau](https://osm.org/node/12141762320)
## 138
* [永新公園站](https://osm.org/node/9361104152)
* [National Yuanlin ChungShih Industrial Vocational High School](https://osm.org/node/12141762319)
## 139
* [屏東大學附設實驗小學](https://osm.org/node/1553314326)
* [NPTU Linsen Campus](https://osm.org/node/3222270167)
* [Zhishan Park](https://osm.org/node/12141762318)
## 140
* [Performing Arts Center](https://osm.org/node/3222270166)
* [屏東菸廠站](https://osm.org/node/11846819162)
* [Yuanlin Train Station Front Plaza](https://osm.org/node/12141762317)
## 141
* [Yuhuang Temple](https://osm.org/node/3222270180)
* [Yuanlin City Office](https://osm.org/node/12141762316)
## 142
* [General House](https://osm.org/node/3222270174)
* [Yuanlin Post Office](https://osm.org/node/12141762315)
## 143
* [Pingtung County Government](https://osm.org/node/3222270175)
* [Di Yi Market](https://osm.org/node/12141762314)
## 144
* [Fuxing Park](https://osm.org/node/3222270188)
* [National Yuanlin Agricultural and Industrial Vocational High School](https://osm.org/node/12141762313)
## 145
* [Pingtung Welfare Service College Center](https://osm.org/node/3222270170)
* [Elderly Recreation Center](https://osm.org/node/12141762312)
## 146
* [Gallery Music Hall](https://osm.org/node/3222270177)
* [Taishan Parking](https://osm.org/node/12141762311)
## 147
* [Chonglan Environmental Protection Park](https://osm.org/node/3222270176)
* [Yuanlin Elementary School](https://osm.org/node/12141762310)
## 148
* [Chong Da Sin Cheng](https://osm.org/node/3222270190)
* [Changhua District Procecutors Office](https://osm.org/node/12141762309)
## 149
* [Chung Hsiao Elementary School](https://osm.org/node/3222270168)
* [Yuanlin Bus Station](https://osm.org/node/12141762308)
## 152
* [Global Mall](https://osm.org/node/4432343151)
* [YuanlinYuan Station](https://osm.org/node/12141762305)
## 153
* [Pingtung Senior High School](https://osm.org/node/4432343149)
* [ZhangShiYuan Station](https://osm.org/node/12141762304)
## 154
* [NPTU Minsheng Campus](https://osm.org/node/4432343146)
* [NPTU Minsheng Campus](https://osm.org/node/11888727720)
* [Bai Guo Mountain](https://osm.org/node/12141762303)
## 155
* [Millennium Park](https://osm.org/node/4432343145)
* [South Changhua Civil Sports Center](https://osm.org/node/12141762302)
## 159
* [I/O STUDIO](https://osm.org/node/9196168749)
* [GongShiSi Park](https://osm.org/node/12141739499)
## 160
* [Dong Shan He](https://osm.org/node/4432343150)
* [GongShiWu Park](https://osm.org/node/12141739498)
## 161
* [Pingtung Tutorial Academy(Confucian temple)](https://osm.org/node/4432343147)
* [Dah-Chin Commercial and Industrial Vocational High School](https://osm.org/node/12141739497)
## 162
* [明正國中站](https://osm.org/node/9361104150)
* [Huiming Parking](https://osm.org/node/12141739496)
## 165
* [Pingtung Station Front Plaza 1](https://osm.org/node/7630710241)
* [Changhua County Tourist Information Center](https://osm.org/node/12141739495)
## 166
* [Pingtung Station Front Plaza 2](https://osm.org/node/9196168748)
* [Lukang Elementary School](https://osm.org/node/12141739494)
## 167
* [Guilai Station](https://osm.org/node/9196168747)
* [Guihua Lane Artists Village](https://osm.org/node/12141739493)
## 168
* [Chaozhou Station](https://osm.org/node/9196179735)
* [Lukang First Liti Parking](https://osm.org/node/12141739492)
## 169
* [Chaozhou Township Parking Lot](https://osm.org/node/9196179736)
* [Lukang Library & Art Center](https://osm.org/node/12141739491)
## 170
* [Chao Jhou Junior High School](https://osm.org/node/9196179739)
* [Lukang Folk Arts Museum](https://osm.org/node/12141739490)
## 171
* [Chao-Chou Elementary School](https://osm.org/node/9196179737)
* [Fuxing First Parking](https://osm.org/node/12141739489)
## 172
* [Guanghua Elementary School](https://osm.org/node/9196179738)
* [National Lukang Senior High School](https://osm.org/node/12141739488)
## 173
* [Chaozhou Township Library](https://osm.org/node/9196179740)
* [Lukang Precinct Changhua County Police Bureau](https://osm.org/node/12141739487)
## 2016
* [Dapingding Park](https://osm.org/node/4932640175)
* [Dapingding Park](https://osm.org/node/9506980820)
## 2142
* [Zhongzheng 1st St. & Zhengkang 1st St. Intersection](https://osm.org/node/5063812285)
* [Zhongzheng 1st St. & Zhengkang 1st St. Intersection](https://osm.org/node/12342754519)
## 2255
* [TRA Fu Gung Station](https://osm.org/node/5913932084)
* [TRA Fu Gung Station](https://osm.org/node/11416192029)
## 2265
* [Rongxing Bridge](https://osm.org/node/6417935976)
* [Rongxing Bridge](https://osm.org/node/9506980818)
## 2400
* [Yangmei Yangming Park](https://osm.org/node/9506965915)
* [Yangmei Yangming Park](https://osm.org/node/11416192025)
## 2402
* [Xinfu Bridge](https://osm.org/node/9506965914)
* [新富橋](https://osm.org/node/11416192023)
## 501207074
* [Kaohsiung Veterans General Hospital](https://osm.org/node/8270288655)
* [榮耀兒童公園](https://osm.org/node/12039432234)
## 501316018
* [Chang-an Elementary School](https://osm.org/node/11337303260)
* [Changan Park](https://osm.org/node/12031219727)

<!--END-->
