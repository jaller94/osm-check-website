---
title: "YouBike stations in the wrong position"
date: 2024-03-22T10:30:00+08:00
lastmod: 2025-03-10T19:59:26.557Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
datasets: [osm-youbikes, youbikes2]
---

Is every YouBike station at the position reported by the official API?

🏗️ This page is new or broken. It's likely inaccurate.

<!--more-->
<!--START-->
⚠️ No, there are 12 stations not in the reported position.

<!--more-->

## 80.12 meters away: 500101015 Taipei Da-an Sports Center Parking lot
* [Taipei Da-an Sports Center Parking lot on OSM](https://osm.org/node/8256951707)
* [Taipei Da-an Sports Center Parking lot on the API](https://osm.org/#map=19/25.02089/121.54697)
## 80.68 meters away: 500108132 Neihu Sports Park
* [Neihu Sports Park on OSM](https://osm.org/node/10232801710)
* [Neihu Sports Park on the API](https://osm.org/#map=19/25.06745/121.57513)
## 84.74 meters away: 500109068 Fengnian Park_1
* [Fengnian Park_1 on OSM](https://osm.org/node/11453087544)
* [Fengnian Park_1 on the API](https://osm.org/#map=19/25.13529/121.49664)
## 86.53 meters away: 500107008 MRT Jiannan Rd. Station (Exit 2)
* [MRT Jiannan Rd. Station (Exit 2) on OSM](https://osm.org/node/8712737873)
* [MRT Jiannan Rd. Station (Exit 2) on the API](https://osm.org/#map=19/25.084751813541587/121.55509027010481)
## 87.82 meters away: 500113030 Dali Senior High School
* [Dali Senior High School on OSM](https://osm.org/node/9020115365)
* [Dali Senior High School on the API](https://osm.org/#map=19/25.03124/121.49075)
## 94.72 meters away: 500215005 Jhongjiao Bay
* [Jhongjiao Bay on OSM](https://osm.org/way/1289409302)
* [Jhongjiao Bay on the API](https://osm.org/#map=19/25.239/121.63234)
## 109.81 meters away: 501305012 MOHW Tainan Hospital
* [MOHW Tainan Hospital on OSM](https://osm.org/node/11046262168)
* [MOHW Tainan Hospital on the API](https://osm.org/#map=19/22.99601/120.20977)
## 122.32 meters away: 500619019 Bianzitouwei Bus Station
* [Bianzitouwei Bus Station on OSM](https://osm.org/node/11492963084)
* [Bianzitouwei Bus Station on the API](https://osm.org/#map=19/24.15257/120.53383)
## 129.75 meters away: 501323008 Gold Coast Visitor Center
* [Gold Coast Visitor Center on OSM](https://osm.org/node/6320846718)
* [Gold Coast Visitor Center on the API](https://osm.org/#map=19/22.93175/120.1763)
## 137.31 meters away: 500104030 MRT Jianta Station
* [MRT Jianta Station on OSM](https://osm.org/node/8712764930)
* [MRT Jianta Station on the API](https://osm.org/#map=19/25.08388/121.52535)
## 137.67 meters away: 500223063 LRT Hongshulin Station
* [LRT Hongshulin Station on OSM](https://osm.org/node/11449094914)
* [LRT Hongshulin Station on the API](https://osm.org/#map=19/25.155053/121.458868)
## 311.84 meters away: 501316018 Chang-an Elementary School
* [Changan Park on OSM](https://osm.org/node/12031219727)
* [Chang-an Elementary School on the API](https://osm.org/#map=19/23.06551/120.19775)

<!--END-->
