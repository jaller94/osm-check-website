This page checks the correctness of OpenStreetMap. The findings are published as [reports](/report).

## Behind the scenes
Learn about our [datasets](/dataset) or [submit an idea or issue](https://gitlab.com/jaller94/osm-check-website/-/issues) to improve this website.
