---
title: "No missing tags on YouBike stations"
date: 2024-03-22T02:00:00+08:00
lastmod: 2025-03-10T19:59:26.370Z
tags: [YouBike, Public Transport]
areas: [Taiwan]
datasets: [osm-youbikes]
---

Does every YouBike station have all common tags?

🏗️ This page is new or broken. It's likely inaccurate.

<!--more-->
<!--START-->
⚠️ No, there are 1921 YouBike stations with incomplete tags.

<!--more-->

* [Xinyonghe Market](https://osm.org/relation/18420616)
  * authentication:contactless
  * authentication:membership_card
  * fee
  * ref
* [East Gate Roundabout](https://osm.org/way/427779437)
  * authentication:contactless
  * authentication:membership_card
  * fee
  * network:wikidata
* [Hsinchu Park(Gongyuan Rd.)](https://osm.org/way/427779442)
  * authentication:contactless
  * authentication:membership_card
  * fee
  * network:wikidata
* [National Immigration Agency Training Center](https://osm.org/way/1066946938)
  * network:wikidata
* [Liuhu Park Underground Parking Lot](https://osm.org/way/1138596399)
  * network:wikidata
* [Fuji Fishing Port](https://osm.org/way/1289409299)
  * payment:ep_ipass
  * payment:mastercard
* [Shuangfu Community Development Association](https://osm.org/way/1364196118)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [屏東大學附設實驗小學](https://osm.org/node/1553314326)
  * name:en
  * network:wikidata
  * payment:ep_easycard
* [Banqiao IC;MRT Xinpu Minsheng Station](https://osm.org/node/2554338793)
  * amenity
  * authentication:contactless
  * capacity
  * fee
  * opening_hours
  * payment:credit_cards
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [NPTU Pingshang Campus](https://osm.org/node/3222270165)
  * network:wikidata
  * payment:ep_easycard
* [Performing Arts Center](https://osm.org/node/3222270166)
  * network:wikidata
  * payment:ep_easycard
* [NPTU Linsen Campus](https://osm.org/node/3222270167)
  * network:wikidata
  * payment:ep_easycard
* [Chung Hsiao Elementary School](https://osm.org/node/3222270168)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung County Library](https://osm.org/node/3222270169)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Welfare Service College Center](https://osm.org/node/3222270170)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Girl’s Senior High School](https://osm.org/node/3222270172)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Art Museum](https://osm.org/node/3222270173)
  * network:wikidata
  * payment:ep_easycard
* [General House](https://osm.org/node/3222270174)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung County Government](https://osm.org/node/3222270175)
  * network:wikidata
  * payment:ep_easycard
* [Chonglan Environmental Protection Park](https://osm.org/node/3222270176)
  * network:wikidata
  * payment:ep_easycard
* [Gallery Music Hall](https://osm.org/node/3222270177)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Night Market](https://osm.org/node/3222270179)
  * network:wikidata
  * payment:ep_easycard
* [Yuhuang Temple](https://osm.org/node/3222270180)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung City Office](https://osm.org/node/3222270181)
  * network:wikidata
  * payment:ep_easycard
* [Fuxing Park](https://osm.org/node/3222270188)
  * network:wikidata
  * payment:ep_easycard
* [Labor Recreation Center](https://osm.org/node/3222270189)
  * name:zh-Hant
  * network:wikidata
  * payment:ep_easycard
* [Chong Da Sin Cheng](https://osm.org/node/3222270190)
  * network:wikidata
  * payment:ep_easycard
* [MRT Xiaobitan Sta. (Exit.1)](https://osm.org/node/4235840390)
  * amenity
* [Millennium Park](https://osm.org/node/4432343145)
  * network:wikidata
  * payment:ep_easycard
* [NPTU Minsheng Campus](https://osm.org/node/4432343146)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Tutorial Academy(Confucian temple)](https://osm.org/node/4432343147)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Senior High School](https://osm.org/node/4432343149)
  * network:wikidata
  * payment:ep_easycard
* [Dong Shan He](https://osm.org/node/4432343150)
  * network:wikidata
  * payment:ep_easycard
* [Global Mall](https://osm.org/node/4432343151)
  * network:wikidata
  * payment:ep_easycard
* [Barclay Memorial Park Station](https://osm.org/node/4782902640)
  * fee
  * name:zh-Hant
  * network:wikidata
  * opening_hours
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [K-Bike](https://osm.org/node/4817866200)
  * network:wikidata
  * payment:jcb
* [Shangyi Airport](https://osm.org/node/4817973726)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863559452)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684300)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684301)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684302)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684303)
  * network:wikidata
  * payment:jcb
* [翟山坑道](https://osm.org/node/4863684304)
  * name:en
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684305)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684306)
  * network:wikidata
  * payment:jcb
* [慈湖三角堡](https://osm.org/node/4863684307)
  * name:en
  * network:wikidata
  * payment:jcb
* [和平紀念公園](https://osm.org/node/4863684308)
  * name:en
  * network:wikidata
  * payment:jcb
* [中山紀念林](https://osm.org/node/4863684309)
  * name:en
  * network:wikidata
  * payment:jcb
* [Houhu Waterfront Park](https://osm.org/node/4863684310)
  * network:wikidata
  * payment:jcb
* [森林遊樂區](https://osm.org/node/4863684311)
  * name:en
  * network:wikidata
  * payment:jcb
* [陽翟聚落](https://osm.org/node/4863684312)
  * name:en
  * network:wikidata
  * payment:jcb
* [沙美車站](https://osm.org/node/4863684313)
  * name:en
  * network:wikidata
  * payment:jcb
* [文化園區](https://osm.org/node/4863684314)
  * name:en
  * network:wikidata
  * payment:jcb
* [獅山砲陣地](https://osm.org/node/4863684315)
  * name:en
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684317)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863684318)
  * network:wikidata
  * payment:jcb
* [畜試所](https://osm.org/node/4863684319)
  * name:en
  * network:wikidata
  * payment:jcb
* [中正公園](https://osm.org/node/4863684320)
  * name:en
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863688021)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/4863688022)
  * network:wikidata
  * payment:jcb
* [桃園市政府文化局](https://osm.org/node/4932640220)
  * name:en
* [Zhongzheng 1st St. & Zhengkang 1st St. Intersection](https://osm.org/node/5063812285)
  * amenity
* [KSU Station](https://osm.org/node/5424771237)
  * fee
  * name:zh-Hant
  * network:wikidata
  * opening_hours
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [TRA Fu Gung Station](https://osm.org/node/5913932084)
  * network:wikidata
* [Shang Shun Plaza](https://osm.org/node/5913957037)
  * network:wikidata
* [National Health Research Institutes](https://osm.org/node/5913957039)
  * network:wikidata
* [Jianguo Rd. (Longfeng Temple)](https://osm.org/node/5913957040)
  * network:wikidata
* [Bei Miao Kang Yuan](https://osm.org/node/5913957041)
  * network:wikidata
* [Nan Miao Datong Elementary School](https://osm.org/node/5913957042)
  * network:wikidata
* [Yu Qing Temple](https://osm.org/node/5913957043)
  * network:wikidata
* [Miaoli City Office](https://osm.org/node/5913957044)
  * network:wikidata
* [The Public Library of Miaoli County](https://osm.org/node/5913957045)
  * network:wikidata
* [National Miaoli Senior High School](https://osm.org/node/5913957046)
  * network:wikidata
* [National Miao-Li Agricultural & Industrial Vocational High School](https://osm.org/node/5913957047)
  * network:wikidata
* [TRA Miaoli Station (West)](https://osm.org/node/5913957048)
  * network:wikidata
* [TRA Miaoli Station (East)](https://osm.org/node/5913957049)
  * network:wikidata
* [Toufen City Comprehensive Sports Park](https://osm.org/node/5913957050)
  * network:wikidata
* [Wei Gong Memorial Hospital](https://osm.org/node/5913957051)
  * network:wikidata
* [Toufen City Office](https://osm.org/node/5913957052)
  * network:wikidata
* [Zhunan Bo'ai Park](https://osm.org/node/5913957053)
  * network:wikidata
* [Northern Miaoli Art Center](https://osm.org/node/5913957054)
  * network:wikidata
* [Chung Hsing Commercial Industrial Vocational High School](https://osm.org/node/5913957055)
  * network:wikidata
* [Zhunan Township Office](https://osm.org/node/5913957056)
  * network:wikidata
* [TRA Zhunan Station (West)](https://osm.org/node/5913957057)
  * network:wikidata
* [TRA Zhunan Station (East)](https://osm.org/node/5913957058)
  * network:wikidata
* [Taiwan Miaoli District Court](https://osm.org/node/6034017068)
  * network:wikidata
* [Huanshi & Yanping Rd. Intersection](https://osm.org/node/6034017069)
  * network:wikidata
* [Zhongyang & Bade Rd. 1st Intersection](https://osm.org/node/6034017070)
  * network:wikidata
* [Toufen City Swimming Pool](https://osm.org/node/6034017071)
  * network:wikidata
* [Jianguo Elementary School (Hezuo St.)](https://osm.org/node/6034017072)
  * network:wikidata
* [Houzhuang Elementary School (Cueiheng Rd.)](https://osm.org/node/6034017073)
  * network:wikidata
* [Pan Tau Elementary School](https://osm.org/node/6034017074)
  * network:wikidata
* [Zhaonan Elementary School](https://osm.org/node/6034017075)
  * network:wikidata
* [Glory Square](https://osm.org/node/7136802845)
  * network:wikidata
* [Pingtung Station Front Plaza 1](https://osm.org/node/7630710241)
  * network:wikidata
  * payment:ep_easycard
* [National Chiayi Girls' Senior High School(Wenhua Park)](https://osm.org/node/8255166775)
  * payment:credit_cards
* [Chongwen Park](https://osm.org/node/8255171055)
  * network:wikidata
  * payment:credit_cards
* [Culture Center](https://osm.org/node/8255182743)
  * network:wikidata
  * payment:credit_cards
* [Wenhua Park Parking Lot](https://osm.org/node/8255183803)
  * payment:credit_cards
* [Central Plaza (Beirong St.)](https://osm.org/node/8255200822)
  * payment:credit_cards
* [Hinoki Village](https://osm.org/node/8255202354)
  * network:wikidata
  * payment:credit_cards
* [Mingtsu Parking Lot](https://osm.org/node/8255202513)
  * payment:credit_cards
* [Bank of Taiwan-Chiayi Branch](https://osm.org/node/8255209620)
  * payment:credit_cards
* [Central Fountain Stop](https://osm.org/node/8255215614)
  * payment:credit_cards
* [Chiayi Branch, Administrative Enforcement Agency, Ministry of Justice](https://osm.org/node/8255222581)
  * payment:credit_cards
* [Chiayi City Government](https://osm.org/node/8255228485)
  * payment:credit_cards
* [Xinsheng Rd. Parking Lot (Minzu)](https://osm.org/node/8263054450)
  * payment:credit_cards
* [Chiayi City Transit Center](https://osm.org/node/8263054451)
  * network:wikidata
  * payment:credit_cards
* [Chiayi Yushan Post Office (Youzhong Rd.)](https://osm.org/node/8263054452)
  * network:wikidata
  * payment:credit_cards
* [Gangping Sports Park (Datong Rd.)](https://osm.org/node/8263054453)
  * network:wikidata
  * payment:credit_cards
* [Beixianghu Park (You'ai Rd. Intersection)](https://osm.org/node/8263054454)
  * network:wikidata
  * payment:credit_cards
* [National Hua-Nan Vocational High School](https://osm.org/node/8263054455)
  * payment:credit_cards
* [Chiayi City Gymnasium (Mituo Rd.)](https://osm.org/node/8263054456)
  * network:wikidata
  * payment:credit_cards
* [National Chiayi Industrial Vocational High School](https://osm.org/node/8263054457)
  * payment:credit_cards
* [Tatung Institute of Commerce and Technology](https://osm.org/node/8263054458)
  * payment:credit_cards
* [ShinKong MitsuKoshi - Far Eastern Station](https://osm.org/node/8263054459)
  * payment:credit_cards
* [Chiayi City Municipal Stadium](https://osm.org/node/8263054460)
  * network:wikidata
  * payment:credit_cards
* [TRA Jiabei Sta.](https://osm.org/node/8263054461)
  * network:wikidata
  * payment:credit_cards
* [Children Welfare Service Center, Chiayi CIty](https://osm.org/node/8263054462)
  * payment:credit_cards
* [National Chiayi Economics Vocational High School](https://osm.org/node/8263054463)
  * payment:credit_cards
* [228 National Memorial Park (Ziqiang St.)](https://osm.org/node/8263054464)
  * network:wikidata
  * payment:credit_cards
* [Jinshan Yushan Rd. Intersection](https://osm.org/node/8263054465)
  * network:wikidata
  * payment:credit_cards
* [Chia Le Fu Night Market](https://osm.org/node/8263054466)
  * payment:credit_cards
* [Shixian Bade Sta. (Ziyou Rd.)](https://osm.org/node/8263054467)
  * network:wikidata
  * payment:credit_cards
* [You'ai Xingtong Rd. Intersection(De'an Parking Lot)](https://osm.org/node/8263054468)
  * network:wikidata
  * payment:credit_cards
* [Chiayi Hospital(Zhongxing Rd.)](https://osm.org/node/8263054469)
  * network:wikidata
  * payment:credit_cards
* [Chiayi City Council](https://osm.org/node/8263054470)
  * network:wikidata
  * payment:credit_cards
* [Old City Government(Xirong St. Intersection)](https://osm.org/node/8263054471)
  * payment:credit_cards
* [Chiayi Art Museum (Lanjing St.)](https://osm.org/node/8263054472)
  * payment:credit_cards
* [ShinKong MitsuKoshi & Far Eastern Sta. (Yonghe St. Intersection)](https://osm.org/node/8263054473)
  * payment:credit_cards
* [TRA Chiayi Sta.](https://osm.org/node/8263054474)
  * network:wikidata
  * payment:credit_cards
* [Chang Hwa Bank-Chiayi Branch](https://osm.org/node/8263054475)
  * network:wikidata
  * payment:credit_cards
* [National Chiayi Senior High School (Daya Rd.)](https://osm.org/node/8263054476)
  * payment:credit_cards
* [National Chiayi Senior High School (Zhihang St.)](https://osm.org/node/8263054477)
  * payment:credit_cards
* [National Chiayi Senior Commercial Vocational School (Gongyuan Police Sta.)](https://osm.org/node/8263054478)
  * payment:credit_cards
* [Dunhua Rd. / Kaixuan Rd.](https://osm.org/node/8594320178)
  * network:wikidata
* [The Ego Field (Qiaoda 8th St.)](https://osm.org/node/8594320179)
  * network:wikidata
* [The Touch Field (Jingmao 5th Rd.)](https://osm.org/node/8594320180)
  * network:wikidata
* [Qinghai Rd. / Shangshi Rd.](https://osm.org/node/8594320185)
  * network:wikidata
* [Xingfu Children's Park](https://osm.org/node/8595615079)
  * network:wikidata
* [Taiwan Blvd. / Wenxin Rd. (Northwest)](https://osm.org/node/8595615080)
  * network:wikidata
* [TMRT Wenxin Yinghua Sta.](https://osm.org/node/8595615085)
  * network:wikidata
* [TMRT Sihwei Elementary School Sta.](https://osm.org/node/8595615086)
  * network:wikidata
* [Dayou Park](https://osm.org/node/8595615087)
  * network:wikidata
* [TMRT Shui-an Temple Sta.](https://osm.org/node/8595615088)
  * network:wikidata
* [Gongyi Rd. / Liming Rd. (Southwest)](https://osm.org/node/8595615091)
  * network:wikidata
* [Beitun Children's Park Baseball Field](https://osm.org/node/8595615092)
  * network:wikidata
* [Jingmei Park (Junfu 18th Rd.)](https://osm.org/node/8595615093)
  * network:wikidata
* [Songrong Parking Lot](https://osm.org/node/8595615094)
  * network:wikidata
* [Gansu Park](https://osm.org/node/8595615095)
  * network:wikidata
* [Wenxin Rd. / Changping Rd.](https://osm.org/node/8595615096)
  * network:wikidata
* [TMRT Beitun Main Station](https://osm.org/node/8595615097)
  * network:wikidata
* [TMRT Jiushe Sta.](https://osm.org/node/8595615098)
  * network:wikidata
* [Zhongping Rd. / Jingmao Rd.](https://osm.org/node/8595615100)
  * network:wikidata
* [Wenxin Rd. / Gongyi Rd. (Southeast)](https://osm.org/node/8595615101)
  * network:wikidata
* [Huizhi Park](https://osm.org/node/8595615102)
  * network:wikidata
* [Liming Rd. / Daye Rd.](https://osm.org/node/8595615103)
  * network:wikidata
* [Huangcheng Dibao](https://osm.org/node/8595615106)
  * network:wikidata
* [Maple Garden (Henan Rd.)](https://osm.org/node/8595615107)
  * network:wikidata
* [Jianxing Rd. / Zhongtai E. Rd.](https://osm.org/node/8595615108)
  * network:wikidata
* [Gongyi Rd. / Dajin St. (Northeast)](https://osm.org/node/8595615110)
  * network:wikidata
* [Darong Park](https://osm.org/node/8595615112)
  * network:wikidata
* [Taiwan Blvd. / Meicun Rd. (Southeast)](https://osm.org/node/8595615115)
  * network:wikidata
* [Taichung Second Senior High School (Sec.3, Meichuan E. Rd.)](https://osm.org/node/8595615116)
  * network:wikidata
* [Gongxue N. Rd. / Gongxue Rd. (Northeast)](https://osm.org/node/8595701817)
  * network:wikidata
* [Shuinan Police Station](https://osm.org/node/8595701818)
  * network:wikidata
* [Taiwan Blvd. / Henan Rd. (Northwest)](https://osm.org/node/8595701819)
  * network:wikidata
* [Galong Rd. / Wenxin Rd. (Southeast)](https://osm.org/node/8595701821)
  * network:wikidata
* [TMRT Daqing Sta.](https://osm.org/node/8595701822)
  * network:wikidata
* [Huitai Park](https://osm.org/node/8595701823)
  * network:wikidata
* [Taichung Industrial High School](https://osm.org/node/8595701825)
  * network:wikidata
* [Chung-Ming Elementary School (Taiwan Blvd.)](https://osm.org/node/8595701826)
  * network:wikidata
* [Dongxing Parking Tower](https://osm.org/node/8595701827)
  * network:wikidata
* [Wenxin Rd. / Chongde Rd. (Northwest)](https://osm.org/node/8595701829)
  * network:wikidata
* [TMRT Nantun Sta.](https://osm.org/node/8595701833)
  * network:wikidata
* [Zheng Xin Children's Park (Dadun S. Rd.)](https://osm.org/node/8595701834)
  * network:wikidata
* [Dajin Park](https://osm.org/node/8595701835)
  * network:wikidata
* [TMRT Wenxin Chongde Sta.](https://osm.org/node/8595701836)
  * network:wikidata
* [TMRT Wenxin Zhongqing Sta.](https://osm.org/node/8595701837)
  * network:wikidata
* [Wanshou Park (Xiangshang S. Rd.)](https://osm.org/node/8595701839)
  * network:wikidata
* [Taichung City Government (Huizhong Rd.)](https://osm.org/node/8595701841)
  * network:wikidata
* [Tianjin Parking lot](https://osm.org/node/8595701842)
  * network:wikidata
* [Zhongming Rd. Temporary Parking](https://osm.org/node/8595701843)
  * network:wikidata
* [Gong 51 Parking](https://osm.org/node/8595701844)
  * network:wikidata
* [Taiping Elementary School (Zhonghua Rd.)](https://osm.org/node/8595701845)
  * network:wikidata
* [China Medical University (Shuinan Campus)](https://osm.org/node/8595701846)
  * network:wikidata
* [Paochueh Temple (Jianxing Rd.)](https://osm.org/node/8595701848)
  * network:wikidata
* [Taichung Branch, Bureau of Standards, Metrology and Inspection (Gongxue Rd.)](https://osm.org/node/8595701849)
  * network:wikidata
* [Lai Ming Park (Hankou Rd.)](https://osm.org/node/8595701850)
  * network:wikidata
* [TMRT Songzhu Sta.](https://osm.org/node/8595701851)
  * network:wikidata
* [Hankou Parking Tower](https://osm.org/node/8595701852)
  * network:wikidata
* [National Taiwan Museum Fine Arts (Wuquan W. Rd.)](https://osm.org/node/8595701854)
  * network:wikidata
* [Zhongxing St. Public Parking](https://osm.org/node/8595701857)
  * network:wikidata
* [Taichung Central Park (Visitor Center)](https://osm.org/node/8595701858)
  * network:wikidata
* [Ying Shi Park (Rixing St.)](https://osm.org/node/8595701867)
  * network:wikidata
* [Meichuan E. Rd. / Minsheng Rd.](https://osm.org/node/8595701868)
  * network:wikidata
* [Qingping Park](https://osm.org/node/8595701871)
  * network:wikidata
* [Jiujia Park](https://osm.org/node/8595701872)
  * network:wikidata
* [Taiwan Blvd. / Daren St. (Southeast)](https://osm.org/node/8595701873)
  * network:wikidata
* [Meichuan W. Rd. / Sec. 2, Taiwan Blvd.](https://osm.org/node/8595701874)
  * network:wikidata
* [Hui-Wen High School (Daye Rd.)](https://osm.org/node/8595701875)
  * network:wikidata
* [Hui-Wen High School (Gongyi Rd.)](https://osm.org/node/8595701876)
  * network:wikidata
* [Yingcai Park (Minquan Rd.)](https://osm.org/node/8595701878)
  * network:wikidata
* [Xuefu Rd. / Yonghe St.](https://osm.org/node/8595701879)
  * network:wikidata
* [Wenxin Rd. / Sichuan Rd.](https://osm.org/node/8595701880)
  * network:wikidata
* [Shizheng Park (Shizheng N. 3rd Rd.)](https://osm.org/node/8595701881)
  * network:wikidata
* [Huiyi Park](https://osm.org/node/8595701882)
  * network:wikidata
* [Wenxin Rd. / Huamei W. St. (Northwest)](https://osm.org/node/8595701883)
  * network:wikidata
* [Zhongming Park](https://osm.org/node/8595701885)
  * network:wikidata
* [Dong men Children's Park](https://osm.org/node/8595701887)
  * network:wikidata
* [Changan Park](https://osm.org/node/8595701889)
  * network:wikidata
* [Jingguo Parkway (Xiangshang N. Rd.)](https://osm.org/node/8595701891)
  * network:wikidata
* [Jingguo Parkway (Xiangshang Rd.)](https://osm.org/node/8595701892)
  * network:wikidata
* [Zili Parking Lot](https://osm.org/node/8595701894)
  * network:wikidata
* [Lecheng Park](https://osm.org/node/8595701896)
  * network:wikidata
* [Wen-Hua Senior High School (Wenxin Rd.)](https://osm.org/node/8595701898)
  * network:wikidata
* [Taichung Citizen Plaza (Zhongxing St.)](https://osm.org/node/8595701900)
  * network:wikidata
* [Calligraphy Greenway(Ln. 155 Gongyi Rd.)](https://osm.org/node/8595701901)
  * network:wikidata
* [Zhongren Park](https://osm.org/node/8595701903)
  * network:wikidata
* [Heping Park](https://osm.org/node/8595701905)
  * network:wikidata
* [Renai Park](https://osm.org/node/8595701906)
  * network:wikidata
* [Wenxin Rd. / Shanxi Rd. (Southwest)](https://osm.org/node/8595701911)
  * network:wikidata
* [Songyong Park](https://osm.org/node/8595701913)
  * network:wikidata
* [Dongxing Rd. / Wenxin S. Rd.](https://osm.org/node/8595701915)
  * network:wikidata
* [Chenping Park](https://osm.org/node/8595701916)
  * network:wikidata
* [Wenxin Rd. / Shanxi Rd. (Northeast)](https://osm.org/node/8595701917)
  * network:wikidata
* [Min Su Park (Dalina Rd.)](https://osm.org/node/8595701921)
  * network:wikidata
* [Shenyang Park](https://osm.org/node/8595701923)
  * network:wikidata
* [Zhongxiao Rd. / Guoguang Rd. (Southwest)](https://osm.org/node/8595701926)
  * network:wikidata
* [Dongying Parking Lot (Yixin St.)](https://osm.org/node/8595701927)
  * network:wikidata
* [Chongqing Park (Tianshui W. 6th St.)](https://osm.org/node/8595701928)
  * network:wikidata
* [Hui Lai Elementary School (Luoyang E. St.)](https://osm.org/node/8595701929)
  * network:wikidata
* [Qie Dong Jiao (Meichuan W. Rd.)](https://osm.org/node/8595701930)
  * network:wikidata
* [Jinye Park (Dongying 15th St.)](https://osm.org/node/8595701940)
  * network:wikidata
* [Sanxian Parking Lot](https://osm.org/node/8595701941)
  * network:wikidata
* [Xueshi Rd. / Yude Rd. (East)](https://osm.org/node/8595701944)
  * network:wikidata
* [Guang Shi Underground Parking Lot](https://osm.org/node/8595701946)
  * network:wikidata
* [Dongfu Park (Dongying 1st. St.)](https://osm.org/node/8595701947)
  * network:wikidata
* [Meichuan Park](https://osm.org/node/8595701948)
  * network:wikidata
* [Lai Cuo Elementary School (Taiyuan Rd.)](https://osm.org/node/8595701949)
  * network:wikidata
* [Old City Government Parking Lot (Minsheng N. Rd.)](https://osm.org/node/8693984052)
  * payment:credit_cards
* [Ln. 120, Minsheng S. Rd. Intersection](https://osm.org/node/8693984055)
  * payment:credit_cards
* [Gi 6 Parking Lot(Ln. 346, Zhongxiao Rd.)](https://osm.org/node/8763638010)
  * fee
  * network:wikidata
  * opening_hours
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [MRT Taipei 101/World Trade Center Sta. (Exit 5)](https://osm.org/node/8889580829)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Mingxue Park](https://osm.org/node/9196168746)
  * network:wikidata
  * payment:ep_easycard
* [Guilai Station](https://osm.org/node/9196168747)
  * network:wikidata
  * payment:ep_easycard
* [Pingtung Station Front Plaza 2](https://osm.org/node/9196168748)
  * network:wikidata
  * payment:ep_easycard
* [I/O STUDIO](https://osm.org/node/9196168749)
  * network:wikidata
  * payment:ep_easycard
* [Chaozhou Station](https://osm.org/node/9196179735)
  * network:wikidata
  * payment:ep_easycard
* [Chaozhou Township Parking Lot](https://osm.org/node/9196179736)
  * network:wikidata
  * payment:ep_easycard
* [Chao-Chou Elementary School](https://osm.org/node/9196179737)
  * network:wikidata
  * payment:ep_easycard
* [Guanghua Elementary School](https://osm.org/node/9196179738)
  * network:wikidata
  * payment:ep_easycard
* [Chao Jhou Junior High School](https://osm.org/node/9196179739)
  * network:wikidata
  * payment:ep_easycard
* [Chaozhou Township Library](https://osm.org/node/9196179740)
  * network:wikidata
  * payment:ep_easycard
* [Silin Elementary School](https://osm.org/node/9196179746)
  * network:wikidata
  * payment:ep_easycard
* [New Taipei City Hall (Xinzhan Rd)](https://osm.org/node/9230243084)
  * network:wikidata
* [New Taipei City Hall (Xinfu Rd.)](https://osm.org/node/9230243085)
  * network:wikidata
* [Huanhe Park](https://osm.org/node/9230243086)
  * network:wikidata
* [Sichuan & Yuandong Intersection](https://osm.org/node/9230243087)
  * network:wikidata
* [MRT Far Eastern Hospital Sta.(Exit.2)](https://osm.org/node/9230243088)
  * network:wikidata
* [MRT Qiaohe Sta.](https://osm.org/node/9230243090)
  * network:wikidata
* [New Taipei Municipal Zhonghe Senior High School](https://osm.org/node/9230243093)
  * network:wikidata
* [Jing’an / Bannan Intersection](https://osm.org/node/9230243094)
  * network:wikidata
* [Ln.161,Zhongzheng & Zhongzheng Intersection](https://osm.org/node/9230243095)
  * network:wikidata
* [Guanghua Bridge (Baoping Rd.)](https://osm.org/node/9230243097)
  * network:wikidata
* [Zhanghe Junior High School](https://osm.org/node/9230243098)
  * network:wikidata
* [Tianshan Park (Yong'an St.)](https://osm.org/node/9230243099)
  * network:wikidata
* [Yixing Dongshi Activity Center](https://osm.org/node/9230243100)
  * network:wikidata
* [MRT Jingan Station](https://osm.org/node/9230243101)
  * network:wikidata
* [Shuang Ho Hospital](https://osm.org/node/9230243102)
  * network:wikidata
* [Sanchong High School](https://osm.org/node/9230243104)
  * network:wikidata
* [Chongxin Rd. & Zhongan St. Intersection](https://osm.org/node/9230243105)
  * network:wikidata
* [Beixin Elementary School](https://osm.org/node/9230243106)
  * network:wikidata
* [Zhongxing Rd. & Baoqiao Rd. Intersection](https://osm.org/node/9230243107)
  * network:wikidata
* [Shuhong E. Rd. & Zhuweizi St. Intersection](https://osm.org/node/9230243108)
  * network:wikidata
* [MRT Sanchong Sta. (Exit 1)](https://osm.org/node/9230243110)
  * network:wikidata
* [Sanchong Civil Sports Center](https://osm.org/node/9230243111)
  * network:wikidata
* [Jianhua New Village](https://osm.org/node/9230243112)
  * network:wikidata
* [MRT Banxin Station](https://osm.org/node/9230243113)
  * network:wikidata
* [Xinghua Park](https://osm.org/node/9230243116)
  * network:wikidata
* [Guangxing Elementary School](https://osm.org/node/9230253917)
  * network:wikidata
* [MRT Shisizhang Sta.](https://osm.org/node/9230253918)
  * network:wikidata
* [MRT Xiaobitan Sta. (Exit 1)](https://osm.org/node/9230253919)
  * network:wikidata
* [Yangguang Bridge](https://osm.org/node/9230253920)
  * network:wikidata
* [Nanya Night Market (Jindu Bridge)](https://osm.org/node/9230253921)
  * network:wikidata
* [Huade Park](https://osm.org/node/9230253922)
  * network:wikidata
* [Xizhou Park Parking Lot](https://osm.org/node/9230253923)
  * network:wikidata
* [Sanguang Elementary School (Jin manyì)](https://osm.org/node/9230253924)
  * network:wikidata
* [Huanhe Rd. & Fude Rd. Intersection](https://osm.org/node/9230253925)
  * network:wikidata
* [Zhongzheng Elementary School](https://osm.org/node/9230253927)
  * network:wikidata
* [Hsin Tien Senior High School](https://osm.org/node/9230253928)
  * network:wikidata
* [SanChung Commercial and Indus (Houzhuwei St.)](https://osm.org/node/9230253929)
  * network:wikidata
* [Sanchong Sports Center](https://osm.org/node/9230253930)
  * network:wikidata
* [MRT Cailiao Sta. (Exit 3)](https://osm.org/node/9230253931)
  * network:wikidata
* [Sanchong District Admin. Center](https://osm.org/node/9230253932)
  * network:wikidata
* [Wenzhongxiao Parking Lot](https://osm.org/node/9230253933)
  * network:wikidata
* [Lianbang Park](https://osm.org/node/9230253934)
  * network:wikidata
* [Banqiao Xinyi Park](https://osm.org/node/9230253935)
  * network:wikidata
* [Fuzhou Heyi Housing (Heyi Rd. & He'an 1st Rd. Intersection)](https://osm.org/node/9230253936)
  * network:wikidata
* [Nanya Park](https://osm.org/node/9230253937)
  * network:wikidata
* [Zhongxiao Junior High School](https://osm.org/node/9230253938)
  * network:wikidata
* [Sioude Elementary School](https://osm.org/node/9230253939)
  * network:wikidata
* [Houde Park](https://osm.org/node/9230253940)
  * network:wikidata
* [Sanzhang Park](https://osm.org/node/9230253941)
  * network:wikidata
* [Liuhe Park](https://osm.org/node/9230253942)
  * network:wikidata
* [Helping Park](https://osm.org/node/9230253943)
  * network:wikidata
* [Houzhuwei Civil Activity Center](https://osm.org/node/9230253944)
  * network:wikidata
* [Lixing & Tayung Intersection](https://osm.org/node/9230253945)
  * network:wikidata
* [Piqian Park](https://osm.org/node/9230253946)
  * network:wikidata
* [Banqiao Renai Park](https://osm.org/node/9230253948)
  * network:wikidata
* [Banqiao Heping Park](https://osm.org/node/9230253950)
  * network:wikidata
* [Zhuangjing Park](https://osm.org/node/9230253951)
  * network:wikidata
* [Sichuan & Zhongxiao Intersection](https://osm.org/node/9230253952)
  * network:wikidata
* [Huanhe W. & Dahan Intersection](https://osm.org/node/9230253953)
  * network:wikidata
* [Banqiao Land Office](https://osm.org/node/9230253954)
  * network:wikidata
* [Banqiao Zenwu Temple](https://osm.org/node/9230253955)
  * network:wikidata
* [Anbang Park](https://osm.org/node/9230253956)
  * network:wikidata
* [Juguang Elementary School](https://osm.org/node/9230253957)
  * network:wikidata
* [Guangfu Senior High School](https://osm.org/node/9230253958)
  * network:wikidata
* [Xibei Park](https://osm.org/node/9230253959)
  * network:wikidata
* [Banqiao Fude Park](https://osm.org/node/9230253960)
  * network:wikidata
* [Fuzhou Train Station](https://osm.org/node/9230253961)
  * network:wikidata
* [Kunlun Square](https://osm.org/node/9230253962)
  * network:wikidata
* [Baoshun & Huanhe W. Intersection](https://osm.org/node/9230253963)
  * network:wikidata
* [Minli Bridge](https://osm.org/node/9230253964)
  * network:wikidata
* [MRT Jingping Sta.](https://osm.org/node/9230253965)
  * network:wikidata
* [LRT Binhai Shalun Sta.](https://osm.org/node/9230253966)
  * network:wikidata
* [Fuhua Park](https://osm.org/node/9230253967)
  * network:wikidata
* [Yuantong & Jinhe Intersection](https://osm.org/node/9230253968)
  * network:wikidata
* [Bihe Temple](https://osm.org/node/9230253969)
  * network:wikidata
* [Minle Jenhsing Square](https://osm.org/node/9230253970)
  * network:wikidata
* [Zhonghe & Anhoe Rd.](https://osm.org/node/9230253971)
  * network:wikidata
* [Taipei University of Marine Technology](https://osm.org/node/9230253972)
  * network:wikidata
* [Yishan & Sec. 1, Xinshi 1st Intersection](https://osm.org/node/9230253973)
  * network:wikidata
* [Miranew Dan-Hai Cinema](https://osm.org/node/9230253974)
  * network:wikidata
* [LRT Danhai New Town Sta.](https://osm.org/node/9230253975)
  * network:wikidata
* [Sec. 2, Xinshi 2nd Rd.](https://osm.org/node/9230253976)
  * network:wikidata
* [Yong-Ping High School](https://osm.org/node/9230253978)
  * network:wikidata
* [Sec. 4, Xinshi 2nd Rd.](https://osm.org/node/9230253979)
  * network:wikidata
* [LRT Tamsui District Center](https://osm.org/node/9230253980)
  * network:wikidata
* [Wenhua Parking Lot](https://osm.org/node/9230253981)
  * network:wikidata
* [LRT Shalun Station](https://osm.org/node/9230253982)
  * network:wikidata
* [Dachuang Park No.1](https://osm.org/node/9230253983)
  * network:wikidata
* [Chuwei Park](https://osm.org/node/9230253984)
  * network:wikidata
* [Zhung Yuan Park](https://osm.org/node/9230253985)
  * network:wikidata
* [Yong-an Library](https://osm.org/node/9230253986)
  * network:wikidata
* [The Luzhou Lee Family](https://osm.org/node/9230253987)
  * network:wikidata
* [Yong' an S. Rd. & Ln. 71, Jiuqiong St Intersection](https://osm.org/node/9230253988)
  * network:wikidata
* [Ln. 99, Sec. 1, Xinshi 1st Rd.](https://osm.org/node/9230253989)
  * network:wikidata
* [Banqiao First Stadium](https://osm.org/node/9230253991)
  * network:wikidata
* [Yong'an Park](https://osm.org/node/9230253992)
  * network:wikidata
* [Fuhe Temple](https://osm.org/node/9230253993)
  * network:wikidata
* [Zhongshan North & Xinchun Intersection](https://osm.org/node/9230253996)
  * network:wikidata
* [Ganzhen Civil Activity Center](https://osm.org/node/9230253997)
  * network:wikidata
* [MRT Hongshulin Sta.](https://osm.org/node/9230253998)
  * network:wikidata
* [MRT Zhuwei Sta.](https://osm.org/node/9230253999)
  * network:wikidata
* [LRT Fisherman's Wharf Station](https://osm.org/node/9230254001)
  * network:wikidata
* [MRT Tamsui Station](https://osm.org/node/9230254002)
  * network:wikidata
* [LRT Xinshi 1st Rd. Station](https://osm.org/node/9230254003)
  * network:wikidata
* [Renhua & Wenhua Intersection](https://osm.org/node/9230254004)
  * network:wikidata
* [Jiangcui Elementary School](https://osm.org/node/9230254005)
  * network:wikidata
* [Time Park](https://osm.org/node/9230254006)
  * network:wikidata
* [Yonghe Elementary School](https://osm.org/node/9230254008)
  * network:wikidata
* [Luzhou Zhonglu Park](https://osm.org/node/9230254009)
  * network:wikidata
* [Si Cian Park](https://osm.org/node/9230254010)
  * network:wikidata
* [MRT St.Ignatius High School Sta. (Exit 1)](https://osm.org/node/9230254011)
  * network:wikidata
* [Xiu Lang Elementary School](https://osm.org/node/9230254012)
  * network:wikidata
* [Fuhe Junior High School](https://osm.org/node/9230254013)
  * network:wikidata
* [Fuhe Junior High School Parking Lot](https://osm.org/node/9230254014)
  * network:wikidata
* [Yonghe Civil Sports Center](https://osm.org/node/9230254015)
  * network:wikidata
* [Banqiao Fruit and Vegetable Wholesale Market](https://osm.org/node/9230254016)
  * network:wikidata
* [LRT Tamkang University Station](https://osm.org/node/9230254017)
  * network:wikidata
* [MRT Banqiao Sta.(Exit.3)](https://osm.org/node/9230254018)
  * network:wikidata
* [LRT Danjin Beixin Station](https://osm.org/node/9230254019)
  * network:wikidata
* [Aly. 1, Ln. 182, Wenhua Rd.](https://osm.org/node/9230254020)
  * network:wikidata
* [Yiwen 2nd & Yongcui Intersection](https://osm.org/node/9230254021)
  * network:wikidata
* [Yongcui & Yiwen Intersection](https://osm.org/node/9230254022)
  * network:wikidata
* [Yongcui & Yiwen Intersection](https://osm.org/node/9230254023)
  * network:wikidata
* [Caihong Children's Park](https://osm.org/node/9230254024)
  * network:wikidata
* [Liuti Park](https://osm.org/node/9230254025)
  * network:wikidata
* [Minsheng & Changjiang Intersection](https://osm.org/node/9230254026)
  * network:wikidata
* [MRT Jiangzicui Sta.(Exit 3)](https://osm.org/node/9230254027)
  * network:wikidata
* [MRT Luzhou Sta.](https://osm.org/node/9230254028)
  * network:wikidata
* [Banqiao Junior High School](https://osm.org/node/9230254029)
  * network:wikidata
* [New Taipei City Art Center](https://osm.org/node/9230254030)
  * network:wikidata
* [MRT Zhogyuan Station](https://osm.org/node/9230254032)
  * network:wikidata
* [Sanmin Senior High School Sta. (Exit 1)](https://osm.org/node/9230254034)
  * network:wikidata
* [Sanmin Senior High School(Minquan Rd.)](https://osm.org/node/9230254035)
  * network:wikidata
* [Lin Jian Sheng Memorial Library](https://osm.org/node/9230254036)
  * network:wikidata
* [Renai Square](https://osm.org/node/9230254037)
  * network:wikidata
* [Luchou Sanmin Park](https://osm.org/node/9230254038)
  * network:wikidata
* [Huajiang 1st & Huajiang 5th Intersection](https://osm.org/node/9230254039)
  * network:wikidata
* [Aly. 11, Ln. 109, Huaxin St.](https://osm.org/node/9230254040)
  * network:wikidata
* [Mingzhi Junior High School](https://osm.org/node/9230254041)
  * network:wikidata
* [Sanbaozhu](https://osm.org/node/9230254042)
  * network:wikidata
* [Guangfu Park](https://osm.org/node/9230254044)
  * network:wikidata
* [San chong Yongfeng Park](https://osm.org/node/9230254045)
  * network:wikidata
* [Yongfeng Park Activity Center](https://osm.org/node/9230254046)
  * network:wikidata
* [Fuzhou Apartment(Lequn Rd.)](https://osm.org/node/9230254047)
  * network:wikidata
* [Siwei Park Parking Lot](https://osm.org/node/9230254049)
  * network:wikidata
* [Banqiao Siwei Park](https://osm.org/node/9230254050)
  * network:wikidata
* [Xianmin & Hansheng E. Intersection](https://osm.org/node/9230254051)
  * network:wikidata
* [Wanban & Wensheng Intersection](https://osm.org/node/9230254052)
  * network:wikidata
* [Daguan Junior High School](https://osm.org/node/9230254053)
  * network:wikidata
* [Ln. 7, Fuhe Rd.](https://osm.org/node/9230254054)
  * network:wikidata
* [Wenshan Junior High School](https://osm.org/node/9230254055)
  * network:wikidata
* [Anju Civil Service Housing](https://osm.org/node/9230254056)
  * network:wikidata
* [Anguang Park](https://osm.org/node/9230254057)
  * network:wikidata
* [Bitan Ferry](https://osm.org/node/9230254058)
  * network:wikidata
* [Chaicheng Civic Activity Center](https://osm.org/node/9230254059)
  * network:wikidata
* [An Kang High School](https://osm.org/node/9230254060)
  * network:wikidata
* [The Young Area Community](https://osm.org/node/9230254061)
  * network:wikidata
* [Hsin Tien Xinhe Elementary School](https://osm.org/node/9230254062)
  * network:wikidata
* [Fuhe Bridge](https://osm.org/node/9230254064)
  * network:wikidata
* [Zhongzheng Bridge](https://osm.org/node/9230254065)
  * network:wikidata
* [Xinhai & Changjiang Intersection](https://osm.org/node/9230254066)
  * network:wikidata
* [New Taipei City Hospital Banqiao Branch](https://osm.org/node/9230254067)
  * network:wikidata
* [Wende Elementary School(Gongguan St.)](https://osm.org/node/9230254068)
  * network:wikidata
* [Kaiyuan Civic Activity Center](https://osm.org/node/9230254071)
  * network:wikidata
* [Xinyi Park](https://osm.org/node/9230254072)
  * network:wikidata
* [Xianse Temple](https://osm.org/node/9230254073)
  * network:wikidata
* [Bihua Park](https://osm.org/node/9230254074)
  * network:wikidata
* [MRT Sanchong Elementary School Sta.](https://osm.org/node/9230254075)
  * network:wikidata
* [Dazhi Park](https://osm.org/node/9230254076)
  * network:wikidata
* [Zhengyi Park](https://osm.org/node/9230254077)
  * network:wikidata
* [Ku Pao Home Economics & Commercial High School](https://osm.org/node/9230254078)
  * network:wikidata
* [Xiang Yu Twin Towers](https://osm.org/node/9230254079)
  * network:wikidata
* [Jinan Park](https://osm.org/node/9230254081)
  * network:wikidata
* [Ci'ai Park](https://osm.org/node/9230254082)
  * network:wikidata
* [Zhongxing N. St. 175 Ln](https://osm.org/node/9230254083)
  * network:wikidata
* [Yuansheng Park](https://osm.org/node/9230254085)
  * network:wikidata
* [Zhonghe Land Office](https://osm.org/node/9230254086)
  * network:wikidata
* [Sec. 3, Huanhe W. Rd. (Rian Community)](https://osm.org/node/9230254087)
  * network:wikidata
* [Fuxiang Village (Zhonghe Ditch)](https://osm.org/node/9230254088)
  * network:wikidata
* [MRT Zhonghe Station](https://osm.org/node/9230254089)
  * network:wikidata
* [Ln. 34, Sec. 2, Xingnan Rd.](https://osm.org/node/9230254090)
  * network:wikidata
* [Jisui Junior High School](https://osm.org/node/9230254091)
  * network:wikidata
* [Shanbei Park](https://osm.org/node/9230254092)
  * network:wikidata
* [Aly. 33, Ln. 335, Yuantong Rd.](https://osm.org/node/9230254093)
  * network:wikidata
* [Anle & Yian Rd. Intersection](https://osm.org/node/9230254094)
  * network:wikidata
* [Feito Community(Xingnan Rd.)](https://osm.org/node/9230254095)
  * network:wikidata
* [Zhonghe District Office](https://osm.org/node/9230254096)
  * network:wikidata
* [Huafu Civil Activity Center](https://osm.org/node/9230254097)
  * network:wikidata
* [Zhongxiao Activity Center](https://osm.org/node/9230254099)
  * network:wikidata
* [Ln. 95, Sec. 1, Huanhe W. Rd.Intersection](https://osm.org/node/9230254100)
  * network:wikidata
* [Baoping Ln 214](https://osm.org/node/9230254101)
  * network:wikidata
* [Tinghsi Elementary School](https://osm.org/node/9230254102)
  * network:wikidata
* [San-Chung Commercial and Industrial Vocational High School](https://osm.org/node/9230254103)
  * network:wikidata
* [Jinlong Elementary School](https://osm.org/node/9230254104)
  * network:wikidata
* [Minquan Community Activity Center](https://osm.org/node/9230254105)
  * network:wikidata
* [Yonghe Ziyou St.](https://osm.org/node/9230254106)
  * network:wikidata
* [Yonghe-Sun Yat-Sen Memorial Hall](https://osm.org/node/9230254107)
  * network:wikidata
* [Luzhou Civil Sports Center](https://osm.org/node/9230254108)
  * network:wikidata
* [Er Chung Junior High School](https://osm.org/node/9230254109)
  * network:wikidata
* [Jiangcui Junior High School](https://osm.org/node/9230254110)
  * network:wikidata
* [MRT Fuzhong Sta.(Exit.1)](https://osm.org/node/9230254111)
  * network:wikidata
* [Nanxing Park](https://osm.org/node/9230254112)
  * network:wikidata
* [Lianxing & Long’an Intersection](https://osm.org/node/9230254113)
  * network:wikidata
* [Wenhua-Baiyun Park](https://osm.org/node/9230254114)
  * network:wikidata
* [Xiu de Village (Chenggong Rd. Sec. 2)](https://osm.org/node/9230254115)
  * network:wikidata
* [Shi Mao National Park Community](https://osm.org/node/9230254116)
  * network:wikidata
* [Chungde-Ciaodong Activity Center](https://osm.org/node/9230254117)
  * network:wikidata
* [Qiaodong Village (Zhongxiao E. Rd.)](https://osm.org/node/9230254118)
  * network:wikidata
* [Xizhi Railway Station](https://osm.org/node/9230254119)
  * network:wikidata
* [Minzu 2nd & Mingfeng Intersection](https://osm.org/node/9230254121)
  * network:wikidata
* [Fuzhou Appropriate House ( Heyi 1st. Road )](https://osm.org/node/9230254122)
  * network:wikidata
* [Banqiao Zhongshan Park](https://osm.org/node/9230254123)
  * network:wikidata
* [New Taipei City Art Center(Ln. 124, Sec. 2, Wenhua Rd.)](https://osm.org/node/9230254124)
  * network:wikidata
* [Chongqing Elementary School](https://osm.org/node/9230254125)
  * network:wikidata
* [Zhenyi Village Park](https://osm.org/node/9230254127)
  * network:wikidata
* [Zhong Shan Junior High Schoo](https://osm.org/node/9230254128)
  * network:wikidata
* [Shidiao Park](https://osm.org/node/9230254129)
  * network:wikidata
* [The Lin Family Mansion and Garden](https://osm.org/node/9230254131)
  * network:wikidata
* [National Taiwan University of Arts](https://osm.org/node/9230254132)
  * network:wikidata
* [Banqiao Civil Sports Cente](https://osm.org/node/9230254133)
  * network:wikidata
* [Zhengde Junior High School](https://osm.org/node/9230254134)
  * network:wikidata
* [Tamsui Customs Wharf Park](https://osm.org/node/9230254135)
  * network:wikidata
* [Tamkang University (Shuiyuan St.)](https://osm.org/node/9230254137)
  * network:wikidata
* [Anhe Activity Center](https://osm.org/node/9230254138)
  * network:wikidata
* [Jianguo Minquan Intersection](https://osm.org/node/9230254139)
  * network:wikidata
* [Zhongyang Market](https://osm.org/node/9230254140)
  * network:wikidata
* [MRT DaPingLin Station(Exit.5)](https://osm.org/node/9230254143)
  * network:wikidata
* [CEPP Qingtan Factory](https://osm.org/node/9230254144)
  * network:wikidata
* [An Kang Parking Lot](https://osm.org/node/9230254145)
  * network:wikidata
* [Qingtan Riverside Park](https://osm.org/node/9230254146)
  * network:wikidata
* [Ancheng & Ande Intersection](https://osm.org/node/9230254147)
  * network:wikidata
* [Green Lake Park](https://osm.org/node/9230254148)
  * network:wikidata
* [New Taipei City Library Xindian Branch](https://osm.org/node/9230254149)
  * network:wikidata
* [Guangming St.](https://osm.org/node/9230254151)
  * network:wikidata
* [Zhongxing & Zhongzheng Intersection](https://osm.org/node/9230254153)
  * network:wikidata
* [Huiguo Market](https://osm.org/node/9230254154)
  * network:wikidata
* [Zhongzheng & Minquan Intersection](https://osm.org/node/9230254155)
  * network:wikidata
* [Wufu Village (Wuhua St.)](https://osm.org/node/9230254156)
  * network:wikidata
* [New Taipei Senior High School](https://osm.org/node/9230254159)
  * network:wikidata
* [Jhongshan Civil Activity Center](https://osm.org/node/9230254160)
  * network:wikidata
* [Ln. 307, Kangning St.](https://osm.org/node/9230254161)
  * network:wikidata
* [Kuangfu Elementary School](https://osm.org/node/9230254162)
  * network:wikidata
* [Qing-Tan Elementary School](https://osm.org/node/9230254163)
  * network:wikidata
* [Luzhou Yanping Police Station](https://osm.org/node/9230254164)
  * network:wikidata
* [Shanglin Village (Huanhe E. Rd. Sec. 1 )](https://osm.org/node/9230254165)
  * network:wikidata
* [Xizhi Railway Station Parking Lot](https://osm.org/node/9230254166)
  * network:wikidata
* [Banqiao District Household Registration Office](https://osm.org/node/9230254167)
  * network:wikidata
* [MRT Xiulang Bridge Station](https://osm.org/node/9230254168)
  * network:wikidata
* [MRT Sanhe Junior High School Sta.](https://osm.org/node/9230254169)
  * network:wikidata
* [YongSheng Park](https://osm.org/node/9230254170)
  * network:wikidata
* [MRT Xindian Sta.](https://osm.org/node/9230254171)
  * network:wikidata
* [Tamsui Public Park (No.23)](https://osm.org/node/9230254172)
  * network:wikidata
* [Lantian Building](https://osm.org/node/9230254173)
  * network:wikidata
* [MRT Xianse Temple Sta.(Exit.2)](https://osm.org/node/9230254174)
  * network:wikidata
* [Baochang Park](https://osm.org/node/9230254175)
  * network:wikidata
* [Zhongxing N. St. 175 Ln](https://osm.org/node/9230254176)
  * network:wikidata
* [Ln. 70, Ren’ai St.](https://osm.org/node/9230254177)
  * network:wikidata
* [Lu Jiang square](https://osm.org/node/9230254178)
  * network:wikidata
* [Yongan Children's Park](https://osm.org/node/9230254179)
  * network:wikidata
* [Sanmin & Zhongshan 1st Rd. Intersection](https://osm.org/node/9230254180)
  * network:wikidata
* [Er Chong Elementary School](https://osm.org/node/9230254181)
  * network:wikidata
* [Tong An Park](https://osm.org/node/9230254182)
  * network:wikidata
* [LRT Kanding Station](https://osm.org/node/9230254183)
  * network:wikidata
* [Yong Fu Elementary School](https://osm.org/node/9230254184)
  * network:wikidata
* [Tung Hai Senior High School](https://osm.org/node/9230254185)
  * network:wikidata
* [Xiulang & Chenggong Intersection](https://osm.org/node/9230254186)
  * network:wikidata
* [MRT Yongan Market Sta.](https://osm.org/node/9230254187)
  * network:wikidata
* [Ren Ai Rd 151 Ln](https://osm.org/node/9230254188)
  * network:wikidata
* [Xizhi Dist. Office](https://osm.org/node/9230254189)
  * network:wikidata
* [Wudu Railway Station](https://osm.org/node/9230254190)
  * network:wikidata
* [Luzhou Chongyang Building](https://osm.org/node/9230254191)
  * network:wikidata
* [Banqiao Station](https://osm.org/node/9230254192)
  * network:wikidata
* [MRT Ding Sta(exit 2)](https://osm.org/node/9230254193)
  * network:wikidata
* [Xitou Park](https://osm.org/node/9230254194)
  * network:wikidata
* [Mengqide Park(Sec. 1, Wenhua Rd.)](https://osm.org/node/9349924887)
  * network:wikidata
* [Chong Yang Park](https://osm.org/node/9349924890)
  * network:wikidata
* [Yongping Park](https://osm.org/node/9349924891)
  * network:wikidata
* [Yongkang Park](https://osm.org/node/9349924892)
  * network:wikidata
* [Yamugang Pumping Station](https://osm.org/node/9349924893)
  * network:wikidata
* [Baogao Intelligent Industrial Park](https://osm.org/node/9349924894)
  * network:wikidata
* [Shisizhang & Yangbei 2nd Intersection](https://osm.org/node/9349924895)
  * network:wikidata
* [Hsin Tien Senior High School (Zhongyang Rd.)](https://osm.org/node/9349924896)
  * network:wikidata
* [MRT Qizhang Sta. (Exit 1)](https://osm.org/node/9349924898)
  * network:wikidata
* [Taipei Tzu Chi Hospital](https://osm.org/node/9349924899)
  * network:wikidata
* [Bitan West Coast](https://osm.org/node/9349924901)
  * network:wikidata
* [Xinhe Activity Center](https://osm.org/node/9349924902)
  * network:wikidata
* [Shen’ao Fishing Port](https://osm.org/node/9349924907)
  * network:wikidata
* [Fisherman's Wharf](https://osm.org/node/9349924912)
  * network:wikidata
* [Xinshi Elementary School](https://osm.org/node/9349924913)
  * network:wikidata
* [Tamsui Sports Center](https://osm.org/node/9349924914)
  * network:wikidata
* [Municipal Haishangaoji Junior High School](https://osm.org/node/9349924915)
  * network:wikidata
* [Ln. 111, Guotai St.](https://osm.org/node/9349924916)
  * network:wikidata
* [Huajiang 9th & Huajiang 1st Intersection](https://osm.org/node/9350007617)
  * network:wikidata
* [Xiangshe & Yiwen 1st Intersection](https://osm.org/node/9350007618)
  * network:wikidata
* [Yiwen & Yiwen 2nd Intersection](https://osm.org/node/9350007619)
  * network:wikidata
* [Huanhe W. & Xinyue 1st Intersection](https://osm.org/node/9350007620)
  * network:wikidata
* [Huajiang 1st & Huajiang 6th Intersection](https://osm.org/node/9350007621)
  * network:wikidata
* [Banqiao Chongqing Park](https://osm.org/node/9350007622)
  * network:wikidata
* [Guoguang Park](https://osm.org/node/9350007623)
  * network:wikidata
* [Guoguang Village (Guoguang Rd.)](https://osm.org/node/9350007624)
  * network:wikidata
* [Huajiang 1st Huajiang 9th Intersection](https://osm.org/node/9350007625)
  * network:wikidata
* [Yinyue Park](https://osm.org/node/9350007627)
  * network:wikidata
* [Fuhua Park](https://osm.org/node/9350007628)
  * network:wikidata
* [Fujhou Appropriate House ( He-an 1st. Road )](https://osm.org/node/9350007629)
  * network:wikidata
* [Xinyi Park](https://osm.org/node/9350007630)
  * network:wikidata
* [School Of Chunghwa Telecom](https://osm.org/node/9350007631)
  * network:wikidata
* [Zhongshan Spring Park](https://osm.org/node/9350007632)
  * network:wikidata
* [Zhongshan & Huanjin Intersection](https://osm.org/node/9350007636)
  * network:wikidata
* [NTU Hospital Jinshan Branch](https://osm.org/node/9350007637)
  * network:wikidata
* [Ln. 201, Fude 1st Rd.](https://osm.org/node/9350007639)
  * network:wikidata
* [Jinlong Elementary School](https://osm.org/node/9350007642)
  * network:wikidata
* [Chang An Bridge (Ln. 12, Jixiang St.)](https://osm.org/node/9350007643)
  * network:wikidata
* [Chongde Elementary School](https://osm.org/node/9350007644)
  * network:wikidata
* [Xizhi Park](https://osm.org/node/9350007645)
  * network:wikidata
* [Beigang Elementary School](https://osm.org/node/9350007646)
  * network:wikidata
* [Lantingji Xu Community](https://osm.org/node/9350007647)
  * network:wikidata
* [Qingshan Elementary and Junior High School](https://osm.org/node/9350007648)
  * network:wikidata
* [Yonghe Science Park](https://osm.org/node/9350007649)
  * network:wikidata
* [Yongfu Bridge](https://osm.org/node/9350007653)
  * network:wikidata
* [Ren Ai Park (Ren Ai Rd.)](https://osm.org/node/9350007654)
  * network:wikidata
* [Yonghe Cardinal Tien Hospital](https://osm.org/node/9350007655)
  * network:wikidata
* [Minyi Park](https://osm.org/node/9350007656)
  * network:wikidata
* [Ln. 22, Sec. 4, Chengtai Rd.](https://osm.org/node/9350007657)
  * network:wikidata
* [Ln. 1, Yushi Rd.](https://osm.org/node/9350007658)
  * network:wikidata
* [New Taipei City Labor Activity Center](https://osm.org/node/9350007659)
  * network:wikidata
* [Sec. 1, Chengtai Rd. (CPC)](https://osm.org/node/9350007660)
  * network:wikidata
* [Luguan Public Housing Complex](https://osm.org/node/9350007661)
  * network:wikidata
* [Taipei Deyi GO](https://osm.org/node/9350007662)
  * network:wikidata
* [Chengzhou Elementary School](https://osm.org/node/9350007663)
  * network:wikidata
* [New Taipei City Library Wugu-Chenggong Branch](https://osm.org/node/9350007664)
  * network:wikidata
* [DeTai Park](https://osm.org/node/9350007665)
  * network:wikidata
* [Ln. 158, Sec. 1, Lingyun Rd.](https://osm.org/node/9350007666)
  * network:wikidata
* [Fangzhou 8th Road](https://osm.org/node/9350007667)
  * network:wikidata
* [Wugu Gongwu Park](https://osm.org/node/9350007668)
  * network:wikidata
* [New Taipei City Exhibition Hall](https://osm.org/node/9350007669)
  * network:wikidata
* [Mao Shang Public Housing](https://osm.org/node/9350007670)
  * network:wikidata
* [Chengde Park](https://osm.org/node/9350007671)
  * network:wikidata
* [Wugu Gongshang Parking Lot](https://osm.org/node/9350007672)
  * network:wikidata
* [Deyin Elementary School](https://osm.org/node/9350007673)
  * network:wikidata
* [Minde Parking Lot](https://osm.org/node/9350007674)
  * network:wikidata
* [Jinghe Senior High School](https://osm.org/node/9350007675)
  * network:wikidata
* [Tekuang & Kuokuang Intersection](https://osm.org/node/9350007676)
  * network:wikidata
* [Zhonghe Youth Rental House](https://osm.org/node/9350007677)
  * network:wikidata
* [Zhonghe Civil Sports Center](https://osm.org/node/9350007678)
  * network:wikidata
* [Guangfu Police Substation](https://osm.org/node/9350007679)
  * network:wikidata
* [Lide & Qingyun Intersection](https://osm.org/node/9350007680)
  * network:wikidata
* [Yanhe Community Park](https://osm.org/node/9350007681)
  * network:wikidata
* [Yuanlin Community Park](https://osm.org/node/9350007682)
  * network:wikidata
* [Zhonghua & Ln. 242, Sec. 1, Zhongyang Intersection](https://osm.org/node/9350007683)
  * network:wikidata
* [Gincheng & Yumin Intersection(Zhing Zheng Junior High School)](https://osm.org/node/9350007684)
  * network:wikidata
* [Zhonghua &Yuhua Intersection](https://osm.org/node/9350007685)
  * network:wikidata
* [New Taipei Industrial Vocational High School](https://osm.org/node/9350007686)
  * network:wikidata
* [Xueshi & Xuefu Intersection](https://osm.org/node/9350007687)
  * network:wikidata
* [Ln. 451, Qingyun Rd.](https://osm.org/node/9350007688)
  * network:wikidata
* [Gincheng & Yumin Intersection](https://osm.org/node/9350007689)
  * network:wikidata
* [Zhongyang & Qingli Intersection](https://osm.org/node/9350007690)
  * network:wikidata
* [Changfeng Community](https://osm.org/node/9350007691)
  * network:wikidata
* [Zhongshan No. 1 Technology Headquarters](https://osm.org/node/9350007692)
  * network:wikidata
* [MRT HaiShan Sta.(Exit 3) Car Park](https://osm.org/node/9350007694)
  * network:wikidata
* [MRT Dingpu Sta.(Exit.2)](https://osm.org/node/9350007695)
  * network:wikidata
* [Peipo Community Park](https://osm.org/node/9350007696)
  * network:wikidata
* [Jincheng Parking Lot](https://osm.org/node/9350007697)
  * network:wikidata
* [Jincheng & Lide Intersection](https://osm.org/node/9350007698)
  * network:wikidata
* [Yazhou & Yuan'an Rd.](https://osm.org/node/9350007699)
  * network:wikidata
* [Jincheng & Zhongyi Intersection](https://osm.org/node/9350007701)
  * network:wikidata
* [MRT Tucheng Sta.(Exit 2)](https://osm.org/node/9350007702)
  * network:wikidata
* [Yuanhe Park](https://osm.org/node/9350007703)
  * network:wikidata
* [Xuecheng Park](https://osm.org/node/9350007704)
  * network:wikidata
* [MRT Haishan](https://osm.org/node/9350007705)
  * network:wikidata
* [Dingpu Market](https://osm.org/node/9350007707)
  * network:wikidata
* [Zhonghua & Chenglin Intersection](https://osm.org/node/9350007708)
  * network:wikidata
* [Ln. 292, Sec. 1, Zhongyang Rd. Intersection](https://osm.org/node/9350007709)
  * network:wikidata
* [Zhanlongshan Archaeological Park](https://osm.org/node/9350007710)
  * network:wikidata
* [Tucheng Renai Park](https://osm.org/node/9350007711)
  * network:wikidata
* [Qing Shui Community Park](https://osm.org/node/9350007712)
  * network:wikidata
* [Ln. 196, Xiwei St. & Renmei St. Intersection](https://osm.org/node/9350007714)
  * network:wikidata
* [Lane 193, Zhongzheng North Rd. (a.mart Sanchong)](https://osm.org/node/9350007715)
  * network:wikidata
* [Triple hydrophilic Park](https://osm.org/node/9350007716)
  * network:wikidata
* [li xing Underground Parking Lot](https://osm.org/node/9350007717)
  * network:wikidata
* [Chong Yang Elementary School](https://osm.org/node/9350007718)
  * network:wikidata
* [Dingkan Park](https://osm.org/node/9350007719)
  * network:wikidata
* [Ln. 112, Renhua St.](https://osm.org/node/9350007720)
  * network:wikidata
* [MRT Sanchong Sta. (Exit 3)](https://osm.org/node/9350007721)
  * network:wikidata
* [Longmen Park](https://osm.org/node/9350007722)
  * network:wikidata
* [New Taipei City Library Sanchong Branch](https://osm.org/node/9350007723)
  * network:wikidata
* [Ziqiang & Xiwei Intersection](https://osm.org/node/9350007724)
  * network:wikidata
* [Bihua Park(Jiyong Rd.)](https://osm.org/node/9350007725)
  * network:wikidata
* [Sante Park](https://osm.org/node/9350007726)
  * network:wikidata
* [Longmen & Longbin Intersection](https://osm.org/node/9350007727)
  * network:wikidata
* [Shihsanhang Museum of Archaeology](https://osm.org/node/9350007729)
  * network:wikidata
* [Long Xing Parking Tower](https://osm.org/node/9350007730)
  * network:wikidata
* [DaKan Elementary School](https://osm.org/node/9350007732)
  * network:wikidata
* [Bali Chung Chuang Market Multi-Purpose Building](https://osm.org/node/9350007733)
  * network:wikidata
* [Bali District Center](https://osm.org/node/9350007734)
  * network:wikidata
* [Shia Juang Market](https://osm.org/node/9350007735)
  * network:wikidata
* [National Chiayi University(Multi-Purpose Instruction Building)](https://osm.org/node/9355259861)
  * network:wikidata
* [National Chiayi University(Adminitration Building)](https://osm.org/node/9355259862)
  * network:wikidata
* [National Chiayi University(Gymnasiun)](https://osm.org/node/9355259863)
  * network:wikidata
* [National Chiayi University(Guest House)](https://osm.org/node/9355259864)
  * network:wikidata
* [National Chiayi University(Student Activity Center)](https://osm.org/node/9355259865)
  * network:wikidata
* [Bank of Taiwan-Chiabei Branch(NICE PLAZA Station)](https://osm.org/node/9355259866)
  * network:wikidata
* [Chang-yi garden square Community](https://osm.org/node/9355259868)
  * network:wikidata
* [Jingzhong Community](https://osm.org/node/9355259869)
  * network:wikidata
* [Zhongxiao N. St.](https://osm.org/node/9355259871)
  * network:wikidata
* [Wen-ya Elementary School](https://osm.org/node/9355259872)
  * network:wikidata
* [Chung Jen College of Nursing Health Sciences and Management](https://osm.org/node/9355259873)
  * network:wikidata
* [Shixian & Bao'an 4th Rd. Intersection](https://osm.org/node/9355259874)
  * network:wikidata
* [Shixian & Beigang Rd. Intersection](https://osm.org/node/9355259875)
  * network:wikidata
* [Linsen W. & Wenhua Rd. Intersection](https://osm.org/node/9355259876)
  * network:wikidata
* [Changjung Park](https://osm.org/node/9355259877)
  * network:wikidata
* [Taiwan Chiayi District Prosecutors Office](https://osm.org/node/9355259878)
  * network:wikidata
* [Chiayi Christian Hospital Baojian Building](https://osm.org/node/9355259879)
  * network:wikidata
* [Chiapei Elementary School](https://osm.org/node/9355259880)
  * network:wikidata
* [Junhui & Ln. 153, Xixing St. Rd. Intersection](https://osm.org/node/9355259881)
  * network:wikidata
* [Chiayi City Riverside Park](https://osm.org/node/9355259882)
  * network:wikidata
* [Xuanxin Park](https://osm.org/node/9355259883)
  * network:wikidata
* [Beishewei Park](https://osm.org/node/9355259884)
  * network:wikidata
* [Nanxing Park](https://osm.org/node/9355259885)
  * network:wikidata
* [Yu-shan Public Junior High School(Youzhong Rd.)](https://osm.org/node/9355259886)
  * network:wikidata
* [Jiayou Bicycle Trail(Xinmin Rd.)](https://osm.org/node/9355259887)
  * network:wikidata
* [The West Market(Guohua St.)](https://osm.org/node/9355259888)
  * network:wikidata
* [Zhongxing & Beigang Rd. Intersection](https://osm.org/node/9355259889)
  * network:wikidata
* [Li Jen Senior High School](https://osm.org/node/9355259891)
  * network:wikidata
* [Shixian & Wufeng S. Rd. Intersection](https://osm.org/node/9355259893)
  * network:wikidata
* [Xingjia Park (Shanghai Rd.)](https://osm.org/node/9355259894)
  * network:wikidata
* [Fanzigou Park](https://osm.org/node/9355259897)
  * network:wikidata
* [Xingye & Xinmin Rd. Intersection](https://osm.org/node/9355259901)
  * network:wikidata
* [Xingye & Xuzhou 6th St. Intersection](https://osm.org/node/9355259902)
  * network:wikidata
* [Chongqing & Nanjing Rd. Intersection](https://osm.org/node/9355259903)
  * network:wikidata
* [Chiayi City Vehicle impoundment](https://osm.org/node/9355259904)
  * network:wikidata
* [Chiayi City Labor Recreation Center](https://osm.org/node/9355259907)
  * network:wikidata
* [Dingzhuang Park](https://osm.org/node/9355259911)
  * network:wikidata
* [Chiayi Motor Vehicle Chiayi City Station](https://osm.org/node/9355259912)
  * network:wikidata
* [National Chiayi University (Lantan Campus)](https://osm.org/node/9355259915)
  * network:wikidata
* [Qiaoping Elementary School (Deming Rd. Intersection)](https://osm.org/node/9355259916)
  * network:wikidata
* [Shixian Bade Sta. (Bade Rd. Intersection)](https://osm.org/node/9355268317)
  * network:wikidata
* [Dr. Kuo Sie-Hien Memorial Museum](https://osm.org/node/9355268318)
  * network:wikidata
* [Bo'ai Swimming pool](https://osm.org/node/9355268319)
  * network:wikidata
* [Bo'ai Houyi St. Intersection](https://osm.org/node/9355268320)
  * network:wikidata
* [Ziyou Youai Sta.](https://osm.org/node/9355268321)
  * network:wikidata
* [Xingde Reading Room](https://osm.org/node/9360227644)
  * network:wikidata
* [K-Bike](https://osm.org/node/9360411829)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/9360411830)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/9360411831)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/9360411832)
  * network:wikidata
  * payment:jcb
* [K-Bike](https://osm.org/node/9360411833)
  * network:wikidata
  * payment:jcb
* [明正國中站](https://osm.org/node/9361104150)
  * capacity
  * fee
  * name:en
  * network:wikidata
  * opening_hours
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [國稅局站](https://osm.org/node/9361104151)
  * capacity
  * name:en
  * network:wikidata
  * payment:ep_easycard
* [永新公園站](https://osm.org/node/9361104152)
  * capacity
  * name:en
  * network:wikidata
  * payment:ep_easycard
* [Yingge Yongli St.](https://osm.org/node/9363036909)
  * network:wikidata
* [Guozhong & Erjia Intersection](https://osm.org/node/9363036910)
  * network:wikidata
* [Nanya & Yuzhi Rd. Intersection](https://osm.org/node/9363036911)
  * network:wikidata
* [Yongchang Village Children Park (No.1)](https://osm.org/node/9363036912)
  * network:wikidata
* [Jianshan Park](https://osm.org/node/9363036913)
  * network:wikidata
* [Yongchi elementary school](https://osm.org/node/9363053018)
  * network:wikidata
* [Yingge Junior High chool](https://osm.org/node/9363053019)
  * network:wikidata
* [New Taipei Municipal Yingge Vocational High School](https://osm.org/node/9363053020)
  * network:wikidata
* [Municipal Jianshan Junior High School](https://osm.org/node/9363053021)
  * network:wikidata
* [New Taipei City Library Yingge Branch](https://osm.org/node/9363053024)
  * network:wikidata
* [Shuei He Village Activity Center](https://osm.org/node/9363053026)
  * network:wikidata
* [5 Villages in Ganyuan Joint Activity Center](https://osm.org/node/9363053027)
  * network:wikidata
* [Shulin Da'an Activity Center](https://osm.org/node/9363053031)
  * network:wikidata
* [Sanduo Elementary School](https://osm.org/node/9363053033)
  * network:wikidata
* [Xueqin Dayou Rd. Intersection](https://osm.org/node/9363053034)
  * network:wikidata
* [Ln. 51, Sec. 1, Ganyuan St. Intersection](https://osm.org/node/9363053036)
  * network:wikidata
* [Lane 219, Junying St.](https://osm.org/node/9363053037)
  * network:wikidata
* [Zhongsha Liren Intersection](https://osm.org/node/9363053038)
  * network:wikidata
* [Forest Park](https://osm.org/node/9363053039)
  * network:wikidata
* [Xuecheng & Dayi Intersection](https://osm.org/node/9363053040)
  * network:wikidata
* [Shanjia Railway Station](https://osm.org/node/9363053042)
  * network:wikidata
* [St. Weiwang](https://osm.org/node/9363053048)
  * network:wikidata
* [Shulin South Station](https://osm.org/node/9363053049)
  * network:wikidata
* [Wenlin Elementary School](https://osm.org/node/9363053050)
  * network:wikidata
* [Yude Elementary School](https://osm.org/node/9363053051)
  * network:wikidata
* [Zhongshan & Xinxing Intersection](https://osm.org/node/9363053052)
  * network:wikidata
* [Shulin elementary school](https://osm.org/node/9363053054)
  * network:wikidata
* [Changshou Parent-child Park](https://osm.org/node/9363053056)
  * network:wikidata
* [Shulin Art & Administrative Center](https://osm.org/node/9363053058)
  * network:wikidata
* [Ganyuan Elementary School](https://osm.org/node/9363053059)
  * network:wikidata
* [Junying & Junbao Intersection](https://osm.org/node/9363053060)
  * network:wikidata
* [Shulin Civil Sports Center](https://osm.org/node/9363053061)
  * network:wikidata
* [Wenlin Elementary School( Tanxing St.)](https://osm.org/node/9363053062)
  * network:wikidata
* [Shulin Da’an](https://osm.org/node/9363053063)
  * network:wikidata
* [Shulin industrial zone](https://osm.org/node/9363053064)
  * network:wikidata
* [Guanghua Park](https://osm.org/node/9363053065)
  * network:wikidata
* [TBI MOTION Technology Company](https://osm.org/node/9363053066)
  * network:wikidata
* [Junxing & Sanlong Intersection](https://osm.org/node/9363053067)
  * network:wikidata
* [First of Sanduo Civil Activity Center](https://osm.org/node/9363053068)
  * network:wikidata
* [Honghui Siyuan Squar](https://osm.org/node/9363053069)
  * network:wikidata
* [Sixian Elementary School](https://osm.org/node/9363053070)
  * network:wikidata
* [wenzidi Wetland Park](https://osm.org/node/9363053071)
  * network:wikidata
* [Lane 101, Qionglin South Rd Intersection](https://osm.org/node/9363053072)
  * network:wikidata
* [Xintai Swimming Pool](https://osm.org/node/9363053073)
  * network:wikidata
* [Ln. 255, Xintai Rd.](https://osm.org/node/9363053074)
  * network:wikidata
* [Touqian Elementary School](https://osm.org/node/9363053075)
  * network:wikidata
* [Touqian Sports Park](https://osm.org/node/9363053076)
  * network:wikidata
* [Fumei Park](https://osm.org/node/9363053077)
  * network:wikidata
* [Fuji Park](https://osm.org/node/9363053078)
  * network:wikidata
* [Xintai Junior High School](https://osm.org/node/9363053079)
  * network:wikidata
* [Taipei Hospital](https://osm.org/node/9363053080)
  * network:wikidata
* [Xisheng Park](https://osm.org/node/9363053081)
  * network:wikidata
* [Xi Sheng Guan](https://osm.org/node/9363053082)
  * network:wikidata
* [Xinshu Park](https://osm.org/node/9363053083)
  * network:wikidata
* [XiSheng Square](https://osm.org/node/9363053084)
  * network:wikidata
* [MRT Huilong Sta.(Exit 3)](https://osm.org/node/9363053085)
  * network:wikidata
* [Xinhua Park](https://osm.org/node/9363053086)
  * network:wikidata
* [Min’an E.& Minquan Intersection](https://osm.org/node/9363053087)
  * network:wikidata
* [Xinzhuang Labors Center](https://osm.org/node/9363053088)
  * network:wikidata
* [Longfeng Park](https://osm.org/node/9363053089)
  * network:wikidata
* [Changping Park](https://osm.org/node/9363053092)
  * network:wikidata
* [Far East Power Corporate Headquarters](https://osm.org/node/9363053093)
  * network:wikidata
* [Dazhong Temple](https://osm.org/node/9363053094)
  * network:wikidata
* [Fu Jen Catholic University Hospital](https://osm.org/node/9363053095)
  * network:wikidata
* [Changlong Elementary School](https://osm.org/node/9363053096)
  * network:wikidata
* [Yijibang](https://osm.org/node/9363053097)
  * network:wikidata
* [Litai Village(Zhonghuan Rd. Sec. 2)](https://osm.org/node/9363053098)
  * network:wikidata
* [Jianfu Park](https://osm.org/node/9363053099)
  * network:wikidata
* [Guanghua dongdishi](https://osm.org/node/9363053100)
  * network:wikidata
* [Xintai Park](https://osm.org/node/9363053101)
  * network:wikidata
* [Huangcheng Park](https://osm.org/node/9363053102)
  * network:wikidata
* [Fuying Administration Center](https://osm.org/node/9363053103)
  * network:wikidata
* [Hougang Park](https://osm.org/node/9363053104)
  * network:wikidata
* [Feng-Nian Elementary School](https://osm.org/node/9363053105)
  * network:wikidata
* [Crescent Bridge](https://osm.org/node/9363053107)
  * network:wikidata
* [Zhongzheng Ren'ai St. Intersection](https://osm.org/node/9363053108)
  * network:wikidata
* [MRT Xinzhuang Sta. (Exit 2)](https://osm.org/node/9363053109)
  * network:wikidata
* [JhongPing Junior High School](https://osm.org/node/9363053111)
  * network:wikidata
* [MRT Touqianzhuang Sta.(Exit.1)](https://osm.org/node/9363053112)
  * network:wikidata
* [Zhonggang Vision Hall](https://osm.org/node/9363053113)
  * network:wikidata
* [Zhonghua & Zhongyuan Intersection](https://osm.org/node/9363053114)
  * network:wikidata
* [Fushou Park](https://osm.org/node/9363053115)
  * network:wikidata
* [Touqian Junior High School](https://osm.org/node/9363053116)
  * network:wikidata
* [Huacheng Park](https://osm.org/node/9363053117)
  * network:wikidata
* [Xinzhuang Culture and Arts Center](https://osm.org/node/9363053118)
  * network:wikidata
* [MRT New Taipei Industrial Park Sta.](https://osm.org/node/9363053119)
  * network:wikidata
* [Zhongxin Elementary School](https://osm.org/node/9363053120)
  * network:wikidata
* [MRT Xinzhuang Fuduxin Sta.](https://osm.org/node/9363053122)
  * network:wikidata
* [Honhui Plaza](https://osm.org/node/9363053123)
  * network:wikidata
* [Qionglin Park](https://osm.org/node/9363053124)
  * network:wikidata
* [Siwei Park](https://osm.org/node/9363053125)
  * network:wikidata
* [Xinzhuang Gymnasium](https://osm.org/node/9363053128)
  * network:wikidata
* [Zhonghua Village (Zhonghua Rd. Sec. 2 )](https://osm.org/node/9363053129)
  * network:wikidata
* [Xinzhuang Gymnasium](https://osm.org/node/9363053130)
  * network:wikidata
* [Xinzhuang Civil Sports Center](https://osm.org/node/9363053131)
  * network:wikidata
* [Xinzhuang Baseball Court](https://osm.org/node/9363053132)
  * network:wikidata
* [Shunde St.](https://osm.org/node/9363053135)
  * network:wikidata
* [Ln. 284, Zhongzheng Rd.](https://osm.org/node/9363053136)
  * network:wikidata
* [Hobe Art Park](https://osm.org/node/9363053137)
  * network:wikidata
* [shui dui Multistorey Car Park](https://osm.org/node/9363053138)
  * network:wikidata
* [Xinshi san Yishan Rd. Intersection](https://osm.org/node/9363053139)
  * network:wikidata
* [Sanmin Cuihua St.Intersection](https://osm.org/node/9363053140)
  * network:wikidata
* [Xinkun Park](https://osm.org/node/9363053141)
  * network:wikidata
* [New Taipei City Library](https://osm.org/node/9363053142)
  * network:wikidata
* [Huacheng Bridge (Ln. 61, Kangning St.)](https://osm.org/node/9363053144)
  * network:wikidata
* [Shuanghe Bridge](https://osm.org/node/9363053145)
  * network:wikidata
* [Bao’an & Yongping Intersection](https://osm.org/node/9363053146)
  * network:wikidata
* [Renai Park](https://osm.org/node/9363053147)
  * network:wikidata
* [Wugu Post Office](https://osm.org/node/9363053148)
  * network:wikidata
* [Wugu Civil Sports Center](https://osm.org/node/9363053149)
  * network:wikidata
* [Xiushan Elementary School](https://osm.org/node/9363053150)
  * network:wikidata
* [Neutralize Ziqiang Park](https://osm.org/node/9363053151)
  * network:wikidata
* [Zhonghe Park](https://osm.org/node/9363053152)
  * network:wikidata
* [Deguang & Juguang Intersection](https://osm.org/node/9363053153)
  * network:wikidata
* [Chiahe Park](https://osm.org/node/9363053154)
  * network:wikidata
* [Wayao Ditch (Fuzhen Village)](https://osm.org/node/9363053155)
  * network:wikidata
* [Yuanxiong Left Bank](https://osm.org/node/9363053156)
  * network:wikidata
* [Zhongyuan E St. ( Left bank Peony Garden )](https://osm.org/node/9363053157)
  * network:wikidata
* [Qing Shui Senior High School](https://osm.org/node/9363053158)
  * network:wikidata
* [Qingshan Square](https://osm.org/node/9363053159)
  * network:wikidata
* [Dingxin-Zutian Civic Activity Center](https://osm.org/node/9363053160)
  * network:wikidata
* [Dingpu Elementary School](https://osm.org/node/9363053161)
  * network:wikidata
* [Tucheng Junior High School](https://osm.org/node/9363053162)
  * network:wikidata
* [Qinghe Village(Qingyun Rd.)](https://osm.org/node/9363053163)
  * network:wikidata
* [Dazhi Wenhua Rd.Intersection](https://osm.org/node/9363053165)
  * network:wikidata
* [Longpu Park](https://osm.org/node/9363053166)
  * network:wikidata
* [Sanxia District Jiaoxi Administration Center](https://osm.org/node/9363053169)
  * network:wikidata
* [Xuecheng Dayi Rd.Intersection (Xuecheng Rd. 1)](https://osm.org/node/9363053171)
  * network:wikidata
* [Zhonghua Ai'guo Rd. Intersection](https://osm.org/node/9363053172)
  * network:wikidata
* [Longpu elementary school](https://osm.org/node/9363053177)
  * network:wikidata
* [Sanxia Junior High School](https://osm.org/node/9363053179)
  * network:wikidata
* [Fuxing & Minsheng St. Intersection](https://osm.org/node/9363053180)
  * network:wikidata
* [Zhongyuan elementary school](https://osm.org/node/9363053181)
  * network:wikidata
* [Sanxia District Office(Sanxia Old Street)](https://osm.org/node/9363053182)
  * network:wikidata
* [Sanxia Guoguang Youth Rental House](https://osm.org/node/9363053183)
  * network:wikidata
* [Anxi Junior high School](https://osm.org/node/9363053184)
  * network:wikidata
* [Jieshou Elementary School](https://osm.org/node/9363053185)
  * network:wikidata
* [Anxi Elementary School](https://osm.org/node/9363053186)
  * network:wikidata
* [Natural Park](https://osm.org/node/9363053188)
  * network:wikidata
* [Liuzhang Park](https://osm.org/node/9363053189)
  * network:wikidata
* [Sanchong Baseball Court](https://osm.org/node/9363053190)
  * network:wikidata
* [San-Chung Commercial and Industrial Vocational High School basement parking](https://osm.org/node/9363053191)
  * network:wikidata
* [Taiyang Park](https://osm.org/node/9363053192)
  * network:wikidata
* [Sanchong Wuchang Park](https://osm.org/node/9363053193)
  * network:wikidata
* [Sixian Elementary School(Ziyou St.)](https://osm.org/node/9410139071)
  * network:wikidata
* [Ln. 252, Min'an W. Rd.](https://osm.org/node/9410139072)
  * network:wikidata
* [Long'an Chenggong St. Intersection](https://osm.org/node/9410139073)
  * network:wikidata
* [Zhongzheng Shuangfeng Rd. Intersection](https://osm.org/node/9410139074)
  * network:wikidata
* [Ln. 347, Zhongzheng Rd.](https://osm.org/node/9410139075)
  * network:wikidata
* [Ln. 90, Jianzhong St.](https://osm.org/node/9410139076)
  * network:wikidata
* [New Taipei City Xinzhuang Civil Sports Center (Hexing St.)](https://osm.org/node/9410139077)
  * network:wikidata
* [Shuangfeng Park](https://osm.org/node/9410139078)
  * network:wikidata
* [Xingfu Zhonghuan Rd. Intersection](https://osm.org/node/9410139079)
  * network:wikidata
* [Changping Elementary School](https://osm.org/node/9410139080)
  * network:wikidata
* [Taiwan Film and Audiovisual Institute](https://osm.org/node/9410139081)
  * network:wikidata
* [Rongfu Elementary School](https://osm.org/node/9410139082)
  * network:wikidata
* [Danfeng Police Substation](https://osm.org/node/9410139083)
  * network:wikidata
* [New Taipei Blvd. & Changping St. Intersection(Test Station)](https://osm.org/node/9410139084)
  * network:wikidata
* [Anhe Park](https://osm.org/node/9410139085)
  * network:wikidata
* [Mingzhi Elementary School](https://osm.org/node/9410139086)
  * network:wikidata
* [Ming Chi University Of Technology](https://osm.org/node/9410139087)
  * network:wikidata
* [MRT Taishan Guihe Sta.](https://osm.org/node/9410139088)
  * network:wikidata
* [Taipei Garden Community](https://osm.org/node/9410139090)
  * network:wikidata
* [MRT Taishan Sta.](https://osm.org/node/9410139091)
  * network:wikidata
* [Guihe Civil Activity Center](https://osm.org/node/9410139092)
  * network:wikidata
* [Tongxing Park](https://osm.org/node/9410139093)
  * network:wikidata
* [Taishan Elementary School](https://osm.org/node/9410139094)
  * network:wikidata
* [Yixue Elementary School](https://osm.org/node/9410139095)
  * network:wikidata
* [Xinwutai Civil Sports Center](https://osm.org/node/9410139096)
  * network:wikidata
* [Fengshujiao Park](https://osm.org/node/9410139097)
  * network:wikidata
* [Xinyue 2nd Luti St. Intersection](https://osm.org/node/9410139098)
  * network:wikidata
* [Xinmin Minzu Rd. Intersection](https://osm.org/node/9410139099)
  * network:wikidata
* [Lane 20, Daming St.](https://osm.org/node/9410139100)
  * network:wikidata
* [Banqiao 2nd Stadium](https://osm.org/node/9410139101)
  * network:wikidata
* [Zhongzheng & Xinyue 2nd Intersection](https://osm.org/node/9410139102)
  * network:wikidata
* [Linkou Maternity and Child Park](https://osm.org/node/9410139103)
  * network:wikidata
* [Zhongshan Jialin Rd. Intersection](https://osm.org/node/9410139104)
  * network:wikidata
* [LiLin Market](https://osm.org/node/9410139105)
  * network:wikidata
* [Linkou Jixiang Park](https://osm.org/node/9410139106)
  * network:wikidata
* [Chunglin Junior High School](https://osm.org/node/9410139107)
  * network:wikidata
* [Linkou District Office](https://osm.org/node/9410139108)
  * network:wikidata
* [Dalun Park](https://osm.org/node/9410139109)
  * network:wikidata
* [Linkou Elementary School](https://osm.org/node/9410139110)
  * network:wikidata
* [Linkou Ziqiang Park](https://osm.org/node/9410139111)
  * network:wikidata
* [Linkou Community Sports Park(Gongyuan Rd.)](https://osm.org/node/9410139112)
  * network:wikidata
* [Linkou Community Sports Park](https://osm.org/node/9410139113)
  * network:wikidata
* [Xinliyuan Community](https://osm.org/node/9410139114)
  * network:wikidata
* [Linkou Post Office](https://osm.org/node/9410139115)
  * network:wikidata
* [Ren-ai & Wenhua 2nd Rd. Intersection](https://osm.org/node/9410158417)
  * network:wikidata
* [FTV Digital Media Headquarter](https://osm.org/node/9410158418)
  * network:wikidata
* [Touhou Elementary School](https://osm.org/node/9410158419)
  * network:wikidata
* [Hunan Civil Activity Center](https://osm.org/node/9410158420)
  * network:wikidata
* [Fugui Park](https://osm.org/node/9410158421)
  * network:wikidata
* [Linkou Civil Sports Center](https://osm.org/node/9410158422)
  * network:wikidata
* [Linkou Senior High School](https://osm.org/node/9410158423)
  * network:wikidata
* [Zhulinshan Guanyin Temple](https://osm.org/node/9410158424)
  * network:wikidata
* [Linkou Old Street](https://osm.org/node/9410158425)
  * network:wikidata
* [NTNU-Linkou Campus](https://osm.org/node/9410158426)
  * network:wikidata
* [New TaipeiCity Linkou Administrative Park](https://osm.org/node/9410158427)
  * network:wikidata
* [MITSUI OUTLET PARK](https://osm.org/node/9410158428)
  * network:wikidata
* [Guangting Sec. Parking Lot](https://osm.org/node/9410158429)
  * network:wikidata
* [Fulun Park](https://osm.org/node/9410158430)
  * network:wikidata
* [Lide Park](https://osm.org/node/9410158431)
  * network:wikidata
* [Nanshih Civil Activity Center](https://osm.org/node/9410158432)
  * network:wikidata
* [Liyan Park](https://osm.org/node/9410158433)
  * network:wikidata
* [Xinlin Elementary School](https://osm.org/node/9410158434)
  * network:wikidata
* [MRT Linkou Sta. (Exit.1)](https://osm.org/node/9410158435)
  * network:wikidata
* [Yonghe Lixing St. Intersection](https://osm.org/node/9410158437)
  * network:wikidata
* [Ren'ai Xinyi Rd. Intersection](https://osm.org/node/9410158438)
  * network:wikidata
* [Yonghe Xinsheng Market](https://osm.org/node/9410158439)
  * network:wikidata
* [Yongzheng Shuiyuan St. Intersection](https://osm.org/node/9410158440)
  * network:wikidata
* [Fuhe & Ln. 28, Ziqiang St. Intersection](https://osm.org/node/9410158441)
  * network:wikidata
* [Baoping Sec 1, Yonghe Rd.Intersection](https://osm.org/node/9410158442)
  * network:wikidata
* [No. 492, Bannan Rd.](https://osm.org/node/9410158443)
  * network:wikidata
* [Yuantong Green Way](https://osm.org/node/9410158444)
  * network:wikidata
* [Lane 56, Nanhua Rd](https://osm.org/node/9410158445)
  * network:wikidata
* [Ln. 291, Yanji St.](https://osm.org/node/9410158446)
  * network:wikidata
* [Hua Nan Dingpu Technology Building](https://osm.org/node/9410158447)
  * network:wikidata
* [Miaoli Hakka Roundhouse(HSR Miaoli Station)](https://osm.org/node/9457185661)
  * network:wikidata
* [Wei Jhen Junior High School](https://osm.org/node/9457185662)
  * network:wikidata
* [TRA Houlong Station](https://osm.org/node/9457185663)
  * network:wikidata
* [Jenteh Junior College of Medicine, Nursing and Management](https://osm.org/node/9457185664)
  * network:wikidata
* [Ching Hai Temple](https://osm.org/node/9457185665)
  * network:wikidata
* [Gong Tian Temple](https://osm.org/node/9457185666)
  * network:wikidata
* [TRA Baishatun Station](https://osm.org/node/9457185667)
  * network:wikidata
* [Taiyen Tung-Hsiao Tourism Park](https://osm.org/node/9457185668)
  * network:wikidata
* [TRA Tongxiao Station](https://osm.org/node/9457185669)
  * network:wikidata
* [Tungshiau Township Office](https://osm.org/node/9457185670)
  * network:wikidata
* [National Yuanli Senior High School](https://osm.org/node/9457185671)
  * network:wikidata
* [TRA Yuanli Station](https://osm.org/node/9457185672)
  * network:wikidata
* [Yuanli Township Public Library](https://osm.org/node/9457185673)
  * network:wikidata
* [Yuanli Stadium](https://osm.org/node/9457185674)
  * network:wikidata
* [Shanjiao Elementary School](https://osm.org/node/9457185675)
  * network:wikidata
* [Miaoli Cultural Park](https://osm.org/node/9457185676)
  * network:wikidata
* [Gongguan Junior High School](https://osm.org/node/9457185677)
  * network:wikidata
* [Gongguan Township Public Library](https://osm.org/node/9457185681)
  * network:wikidata
* [Gongguan Night Market Parking Lot](https://osm.org/node/9457185682)
  * network:wikidata
* [Gongguan Peasant Association](https://osm.org/node/9457185683)
  * network:wikidata
* [Xinfu Bridge](https://osm.org/node/9506965914)
  * network:wikidata
  * payment:credit_cards
* [Yangmei Yangming Park](https://osm.org/node/9506965915)
  * network:wikidata
  * payment:credit_cards
* [Rongxing Bridge](https://osm.org/node/9506980818)
  * network:wikidata
  * payment:credit_cards
* [Dapingding Park](https://osm.org/node/9506980820)
  * network:wikidata
  * payment:credit_cards
* [Gongyi Rd/ Kezhuan 6th Rd(Datong Senior High School)](https://osm.org/node/9630638726)
  * network:wikidata
* [Jianguo Rd/Zhongxing Rd(Jian Guo Junior High School)](https://osm.org/node/9630638727)
  * network:wikidata
* [Tou Fen Junior High School](https://osm.org/node/9630638729)
  * network:wikidata
* [Longshan Rd/Jiabei 2nd St(Parking Lot)](https://osm.org/node/9630638730)
  * network:wikidata
* [Long Fong Fishing Port](https://osm.org/node/9630638731)
  * network:wikidata
* [Jue Ming Temple(Ln. 53, Fengnian St.)](https://osm.org/node/9630963958)
  * network:wikidata
* [Zhongzheng Bao'an St. Intersection](https://osm.org/node/9630963959)
  * network:wikidata
* [No. 35, Fude 2nd St.](https://osm.org/node/9630963960)
  * network:wikidata
* [Zhitan Community Center](https://osm.org/node/9630963961)
  * network:wikidata
* [Zhitan Purification Plant(Zhitan 2nd St.)](https://osm.org/node/9630963962)
  * network:wikidata
* [Yangbei 3rd & Zhongyang Rd. Intersection](https://osm.org/node/9630963963)
  * network:wikidata
* [LRT Danjin Denggong Sta.](https://osm.org/node/9630963964)
  * network:wikidata
* [Zhongzheng Danhai Rd. Intersection](https://osm.org/node/9630963965)
  * network:wikidata
* [Taishan Senior High School](https://osm.org/node/9630963966)
  * network:wikidata
* [Banxin & Sec.1, Zhongshan Rd. Intersection](https://osm.org/node/9630963967)
  * network:wikidata
* [Shuangshi Wanban Rd. Intersection](https://osm.org/node/9630963968)
  * network:wikidata
* [Xi'an Civic Activity Center](https://osm.org/node/9630963969)
  * network:wikidata
* [Xianmin Blvd & Minzu Intersection](https://osm.org/node/9630963970)
  * network:wikidata
* [Zhuhele Community](https://osm.org/node/9630963971)
  * network:wikidata
* [Jingquan Park](https://osm.org/node/9630963972)
  * network:wikidata
* [Ln. 249, Sec. 1, Wenhua 3rd. Rd.](https://osm.org/node/9630963973)
  * network:wikidata
* [Lixing Park](https://osm.org/node/9630963974)
  * network:wikidata
* [No. 91, Sec. 1, Wenhua 1st. Rd. Intersection](https://osm.org/node/9630963975)
  * network:wikidata
* [Ren'ai Infant Day Care Center](https://osm.org/node/9630963976)
  * network:wikidata
* [Siwei Wenhua 1st Rd. Intersection](https://osm.org/node/9630963977)
  * network:wikidata
* [Ln. 151, Xinglin Rd. Intersection](https://osm.org/node/9630963978)
  * network:wikidata
* [Zumeng Sport Park](https://osm.org/node/9630963979)
  * network:wikidata
* [Jinshan Parking Lot(Zhongxing Rd)](https://osm.org/node/9630963980)
  * network:wikidata
* [No. 24, Aly. 16, Ln. 24, Sec. 2, Xiulang Rd.](https://osm.org/node/9630963981)
  * network:wikidata
* [Wayao Interception & Pumping Plant](https://osm.org/node/9630963982)
  * network:wikidata
* [Aly. 18, Ln. 306, Ren'ai Rd. Intersection](https://osm.org/node/9630963983)
  * network:wikidata
* [Bao'fu & Ln. 133, Sec. 2, Bao'fu Rd. Intersection](https://osm.org/node/9630963984)
  * network:wikidata
* [Bao'an & Sec. 2, Bao'fu Rd. Intersection](https://osm.org/node/9630963985)
  * network:wikidata
* [Ln. 666, Zhongzheng Rd.](https://osm.org/node/9630963986)
  * network:wikidata
* [Yonghe Junior High School(Guozhong Rd.)](https://osm.org/node/9630963987)
  * network:wikidata
* [Xincheng 1st. Rd.](https://osm.org/node/9630963989)
  * network:wikidata
* [Xiushan Park(North)](https://osm.org/node/9630963990)
  * network:wikidata
* [Anping bridge](https://osm.org/node/9630963991)
  * network:wikidata
* [Shoude New Village](https://osm.org/node/9630963992)
  * network:wikidata
* [Hwa Hsia University of Technology](https://osm.org/node/9630963993)
  * network:wikidata
* [Tucheng Elementary School(Xingcheng Rd.)](https://osm.org/node/9630963994)
  * network:wikidata
* [Zhongyang Zhonghua Rd. Intersection(Jincheng)](https://osm.org/node/9630963995)
  * network:wikidata
* [Yumin Rd. Intersection(Qingshui Rd.)](https://osm.org/node/9630963996)
  * network:wikidata
* [National Chung-Shan Institute of Science & Technology](https://osm.org/node/9630963997)
  * network:wikidata
* [廈門立體停車場](https://osm.org/node/9684718828)
  * name:en
* [Yishu St. / Ln. 90, Yishu St.](https://osm.org/node/9721985251)
  * authentication:contactless
  * authentication:membership_card
* [Coast Guard Administration (South)](https://osm.org/node/9796887049)
  * network:wikidata
* [Shingjia Suspension Bridge](https://osm.org/node/9843104575)
  * fee
  * network:wikidata
* [Zhisheng Xinrong Road](https://osm.org/node/9843104576)
  * fee
* [Liucuo No.2 Park](https://osm.org/node/9843104577)
  * fee
  * network:wikidata
* [Yuan Yi Temple](https://osm.org/node/9843104578)
  * fee
  * network:wikidata
* [Xinmin Yong'an St. Intersection](https://osm.org/node/9843104579)
  * fee
* [Tougang Zhenwu Temple](https://osm.org/node/9843104580)
  * fee
  * network:wikidata
* [Kangle Renai Rd. Intersection](https://osm.org/node/9843104581)
  * fee
* [Chiayi Branch ,Taichung Veterans General Hospital(Behind)](https://osm.org/node/9843104582)
  * fee
  * network:wikidata
* [Huguang 10 Parking Lot](https://osm.org/node/9843104583)
  * fee
  * network:wikidata
* [Daya,Yazhu Rd.Intersection](https://osm.org/node/9843104584)
  * fee
* [Lane 368, Section 1, Daya Rd.](https://osm.org/node/9843104585)
  * fee
  * network:wikidata
* [Chiayi City Fruit and Vegetable Market](https://osm.org/node/9843104586)
  * fee
  * network:wikidata
* [Chongqing Section 3, Shixian Rd.Intersection](https://osm.org/node/9843104587)
  * fee
  * network:wikidata
* [Chung Cheng Park](https://osm.org/node/9843104588)
  * fee
  * network:wikidata
* [Wenhua Park(Minzu Rd.)](https://osm.org/node/9843104589)
  * fee
* [Ziyou Yuandong St.Intersection](https://osm.org/node/9843104590)
  * fee
  * network:wikidata
* [Daxicuo](https://osm.org/node/9843104591)
  * fee
  * network:wikidata
* [Shixian Zhongxiao Rd.Intersection](https://osm.org/node/9843104592)
  * fee
* [Linsen Sinrong Rd.Intersection](https://osm.org/node/9843104593)
  * fee
  * network:wikidata
* [Ciyou Kindergarten](https://osm.org/node/9843104594)
  * fee
  * network:wikidata
* [National Chung Cheng University Ching Jiang Learning Center](https://osm.org/node/9843104595)
  * fee
  * network:wikidata
* [Zhongchneg Parking Lot](https://osm.org/node/9843515167)
  * network:wikidata
  * payment:jcb
* [Ln. 65, Yonghe St. Intersection](https://osm.org/node/9843515168)
  * payment:jcb
* [Yingge Old Street 2nd Parking Lot](https://osm.org/node/9843515169)
  * network:wikidata
  * payment:jcb
* [Ln. 16, Sec. 2, Yong'an N. Rd. Intersection](https://osm.org/node/9843515170)
  * network:wikidata
  * payment:jcb
* [Zhongshan 1St Heping Rd. Intersection](https://osm.org/node/9843515171)
  * network:wikidata
  * payment:jcb
* [MRT Luzou Sta.(Exit 1)](https://osm.org/node/9843515172)
  * network:wikidata
  * payment:jcb
* [Samin Changrong Rd. Intersection](https://osm.org/node/9843515173)
  * network:wikidata
  * payment:jcb
* [11](https://osm.org/node/9843515174)
  * network:wikidata
  * payment:jcb
* [MRT Sanmin Senior High School Sta.(Exit 2)](https://osm.org/node/9843515175)
  * network:wikidata
  * payment:jcb
* [Ln. 883, Zhongzheng Rd. Intersection](https://osm.org/node/9843515176)
  * network:wikidata
  * payment:jcb
* [Danfeng Elementary School](https://osm.org/node/9843515177)
  * network:wikidata
  * payment:jcb
* [Touqian Elementary School(Huacheng Rd.)](https://osm.org/node/9843515178)
  * network:wikidata
  * payment:jcb
* [Wenzidi Wetland Park(Zhonghua Rd.)](https://osm.org/node/9843515179)
  * network:wikidata
  * payment:jcb
* [Dafeng Park Parking](https://osm.org/node/9843515180)
  * network:wikidata
  * payment:jcb
* [Shisizhang Park(South)](https://osm.org/node/9843515181)
  * network:wikidata
  * payment:jcb
* [Ln. 23, Yongping St.](https://osm.org/node/9843515182)
  * network:wikidata
  * payment:jcb
* [Shuiguan Park](https://osm.org/node/9843515183)
  * network:wikidata
  * payment:jcb
* [Sec. 3, Xinshi 2nd Rd/Ln. 202, Sec. 2, Binhai Rd.](https://osm.org/node/9843515184)
  * network:wikidata
  * payment:jcb
* [Xinshi 2nd & Houzhou Rd. Intersection](https://osm.org/node/9843515185)
  * network:wikidata
  * payment:jcb
* [Xinshi 3rd & Kanding 5th Rd. Intersection](https://osm.org/node/9843515186)
  * network:wikidata
  * payment:jcb
* [Kanding Fude Temple](https://osm.org/node/9843515187)
  * network:wikidata
  * payment:jcb
* [Ln. 92, Qiaozhong 2nd St. Intersection](https://osm.org/node/9843515188)
  * network:wikidata
  * payment:jcb
* [Ln. 212, Sec. 1, Wenhua Rd. Intersection](https://osm.org/node/9843515189)
  * network:wikidata
  * payment:jcb
* [Sec. 2, Minsheng Rd. & Ln. 286, Sec. 1, Wenhua Rd. Intersection](https://osm.org/node/9843515190)
  * payment:jcb
* [Houpu Elementary School](https://osm.org/node/9843515191)
  * network:wikidata
  * payment:jcb
* [Haishang High School(Hansheng E. Rd.)](https://osm.org/node/9843515192)
  * payment:jcb
* [Xinfu Quyun Rd. Intersection](https://osm.org/node/9843515193)
  * network:wikidata
  * payment:jcb
* [Xinyi Heping Rd. Intersection](https://osm.org/node/9843515194)
  * network:wikidata
  * payment:jcb
* [Huangshi Market](https://osm.org/node/9843515195)
  * network:wikidata
  * payment:jcb
* [Ln. 163, Xinyi Rd. Intersection](https://osm.org/node/9843515196)
  * network:wikidata
  * payment:jcb
* [Zhangshu 1st. & Shanguang Rd. Intersection](https://osm.org/node/9843515197)
  * payment:jcb
* [Xiufeng High School(Zhongxiao E. Rd.)](https://osm.org/node/9843515198)
  * network:wikidata
  * payment:jcb
* [Yongzheng Civic Activity Center](https://osm.org/node/9843515199)
  * network:wikidata
  * payment:jcb
* [Jingben Village Activity Center](https://osm.org/node/9843515200)
  * payment:jcb
* [Shuang ho Hospital(Ln. 252, Yuantong Rd. Intersection)](https://osm.org/node/9843515201)
  * payment:jcb
* [Oven ground Under the mountain Basketball court](https://osm.org/node/9843515202)
  * network:wikidata
  * payment:jcb
* [Ln. 291, Yanji St. Intersection](https://osm.org/node/9843515203)
  * network:wikidata
  * payment:jcb
* [No. 105, Heping Rd.](https://osm.org/node/9843515204)
  * network:wikidata
  * payment:jcb
* [No. 33, Sec. 4, Zhongyang Rd.(North)](https://osm.org/node/9843515205)
  * network:wikidata
  * payment:jcb
* [Guangfu Elementary School](https://osm.org/node/9843515206)
  * network:wikidata
  * payment:jcb
* [New Taipei Industrial Vocational High School(Mingde Rd.)](https://osm.org/node/9843515207)
  * network:wikidata
  * payment:jcb
* [Aly. 1, Ln. 111, Yanji St.](https://osm.org/node/9843515208)
  * network:wikidata
  * payment:jcb
* [Qingshui High School(Guoji Rd.)](https://osm.org/node/9843515209)
  * network:wikidata
  * payment:jcb
* [Victoria Community(Activity Center)](https://osm.org/node/9843515210)
  * network:wikidata
  * payment:jcb
* [Victoria Community(Guard Booth)](https://osm.org/node/9843515211)
  * network:wikidata
  * payment:jcb
* [Guoqing Sanshu Rd. Intersection](https://osm.org/node/9843515212)
  * network:wikidata
  * payment:jcb
* [Zhongyuan Elementary School(Minquan St.)](https://osm.org/node/9843515213)
  * network:wikidata
  * payment:jcb
* [Sanshu Dade Rd. Intersection](https://osm.org/node/9843515214)
  * network:wikidata
  * payment:jcb
* [Xuecheng Dade Rd. Intersection](https://osm.org/node/9843515215)
  * network:wikidata
  * payment:jcb
* [Xueqin Daguan Rd. Intersection](https://osm.org/node/9843515216)
  * payment:jcb
* [Dade Xueqin Rd. Intersection](https://osm.org/node/9843524117)
  * payment:jcb
* [Xuefu Dade Rd. Intersection](https://osm.org/node/9843524118)
  * network:wikidata
  * payment:jcb
* [Xuefu Daxue Rd. Intersection](https://osm.org/node/9843524119)
  * network:wikidata
  * payment:jcb
* [Waziwei Ecological Reserves](https://osm.org/node/9843524120)
  * network:wikidata
  * payment:jcb
* [Bali Activity Center](https://osm.org/node/9843524121)
  * network:wikidata
  * payment:jcb
* [後庄文林站](https://osm.org/node/9998042710)
  * name:en
  * name:zh
  * name:zh-Hant
  * network:wikidata
* [Gongyi Rd. / Jingcheng Rd. (Northwest)](https://osm.org/node/10097635478)
  * authentication:contactless
  * authentication:membership_card
* [桃園煉油廠南門綠地](https://osm.org/node/10102664447)
  * name:en
* [Rueijing Trail](https://osm.org/node/10104689117)
  * authentication:contactless
  * authentication:membership_card
* [官澳](https://osm.org/node/10143128647)
  * name:en
  * network:wikidata
  * payment:jcb
* [Shanjiao Elementary School](https://osm.org/node/10143217428)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yuanli Stadium](https://osm.org/node/10143217429)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yuanli Township Public Library](https://osm.org/node/10143217430)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Yuanli Senior High School](https://osm.org/node/10143217431)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [TRA Yuanli Station](https://osm.org/node/10143217432)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Health Research Institutes](https://osm.org/node/10143217433)
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gongyi Rd./ Kezhuan 6th Rd.(Datong Senior High School)](https://osm.org/node/10143217434)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Longshan Rd./Jiabei 2nd St.(Parking Lot)](https://osm.org/node/10143217435)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Long Fong Fishing Port](https://osm.org/node/10143217436)
  * network:wikidata
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Rinan Xingfu Park](https://osm.org/node/10648004290)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong E. Rd. / Junfu 18th Rd.](https://osm.org/node/10666057605)
  * authentication:contactless
  * authentication:membership_card
* [Nanxing Parking Lot](https://osm.org/node/10666088005)
  * authentication:contactless
  * authentication:membership_card
* [Hanxiang Rd. / Fuxing N. Rd.](https://osm.org/node/11169738702)
  * authentication:contactless
  * authentication:membership_card
* [Douliu Train Station Front Station](https://osm.org/node/11250344659)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Minsheng Park](https://osm.org/node/11250344660)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minde Rd. Intersection](https://osm.org/node/11252306197)
  * network:wikidata
* [Jia Hou Parking Lot](https://osm.org/node/11492919325)
  * authentication:contactless
  * authentication:membership_card
* [Tung Shing Junior High School](https://osm.org/node/11492919326)
  * authentication:contactless
  * authentication:membership_card
* [Xi Ting Guang 1 Parking Lot](https://osm.org/node/11492919327)
  * authentication:contactless
  * authentication:membership_card
* [Dajia District Public Health Center](https://osm.org/node/11492919328)
  * authentication:contactless
  * authentication:membership_card
* [Rinan Park](https://osm.org/node/11492919329)
  * authentication:contactless
  * authentication:membership_card
* [Zhongxinling Parking Lot](https://osm.org/node/11492919330)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Xinshe Branch](https://osm.org/node/11492919331)
  * authentication:contactless
  * authentication:membership_card
* [Ah-Tsung-Shin Taro Culture Museum](https://osm.org/node/11492919332)
  * authentication:contactless
  * authentication:membership_card
* [Dongxi 2nd Rd. Ln. 51](https://osm.org/node/11492919333)
  * authentication:contactless
  * authentication:membership_card
* [Ding-an Community](https://osm.org/node/11492919334)
  * authentication:contactless
  * authentication:membership_card
* [Daan Yong An Elementary School](https://osm.org/node/11492919335)
  * authentication:contactless
  * authentication:membership_card
* [Shigang Elementary School](https://osm.org/node/11492919336)
  * authentication:contactless
  * authentication:membership_card
* [Songhe Community Activity Center](https://osm.org/node/11492919337)
  * authentication:contactless
  * authentication:membership_card
* [Waipu Tucheng Village Activity Center](https://osm.org/node/11492919338)
  * authentication:contactless
  * authentication:membership_card
* [Ming Hu Village](https://osm.org/node/11492919339)
  * authentication:contactless
  * authentication:membership_card
* [Changsheng Rd. No.5 (East)](https://osm.org/node/11492919340)
  * authentication:contactless
  * authentication:membership_card
* [Futian Temple Baogong Shi](https://osm.org/node/11492919341)
  * authentication:contactless
  * authentication:membership_card
* [Tie-Shan Primary School](https://osm.org/node/11492919342)
  * authentication:contactless
  * authentication:membership_card
* [Taichung City International Softball Sports Park](https://osm.org/node/11492919343)
  * authentication:contactless
  * authentication:membership_card
* [Shuimei Community Park](https://osm.org/node/11492919344)
  * authentication:contactless
  * authentication:membership_card
* [Yongfeng Community Environmental Education Park](https://osm.org/node/11492919345)
  * authentication:contactless
  * authentication:membership_card
* [DaDong Park](https://osm.org/node/11492919346)
  * authentication:contactless
  * authentication:membership_card
* [Longfeng Park](https://osm.org/node/11492919347)
  * authentication:contactless
  * authentication:membership_card
* [Chongguang Park](https://osm.org/node/11492919348)
  * authentication:contactless
  * authentication:membership_card
* [Longdong Police Station](https://osm.org/node/11492919349)
  * authentication:contactless
  * authentication:membership_card
* [Long-Hai Elementary School](https://osm.org/node/11492919350)
  * authentication:contactless
  * authentication:membership_card
* [Szu Chen Junior High School](https://osm.org/node/11492919351)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Longjing Shanding Branch](https://osm.org/node/11492919352)
  * authentication:contactless
  * authentication:membership_card
* [Zhukeng Nanliao Mountain Pathway Parking Lot](https://osm.org/node/11492919353)
  * authentication:contactless
  * authentication:membership_card
* [Nanliao Mountain Pathway](https://osm.org/node/11492919354)
  * authentication:contactless
  * authentication:membership_card
* [Longjing District Library](https://osm.org/node/11492919356)
  * authentication:contactless
  * authentication:membership_card
* [Tanzi Technology Industrial Park (Fuxing Rd. / Jianguo Rd.)](https://osm.org/node/11492919357)
  * authentication:contactless
  * authentication:membership_card
* [Hongfu Park (Children's 6th)](https://osm.org/node/11492919358)
  * authentication:contactless
  * authentication:membership_card
* [Ta-Ming High School (Tanzi Art Campus)](https://osm.org/node/11492919359)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. / Jhongsing Rd. (Southwest)](https://osm.org/node/11492919360)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong E. Rd. / Zhongshan Rd. (Northeast)](https://osm.org/node/11492919361)
  * authentication:contactless
  * authentication:membership_card
* [Tanzi Detian Temple](https://osm.org/node/11492919362)
  * authentication:contactless
  * authentication:membership_card
* [Tzu Chi Chinese Medicine Hospital](https://osm.org/node/11492919363)
  * authentication:contactless
  * authentication:membership_card
* [Chongde Rd. / Tanfu Rd.](https://osm.org/node/11492919364)
  * authentication:contactless
  * authentication:membership_card
* [Chongde Rd. / Ln 183 Chongde Rd. Sec. 4](https://osm.org/node/11492919365)
  * authentication:contactless
  * authentication:membership_card
* [Sweeten Fenghua Station](https://osm.org/node/11492919366)
  * authentication:contactless
  * authentication:membership_card
* [Jhongtou E Rd. / Fengzheng Rd.](https://osm.org/node/11492919367)
  * authentication:contactless
  * authentication:membership_card
* [Legislative Yuan Democracy Lawn](https://osm.org/node/11492919368)
  * authentication:contactless
  * authentication:membership_card
* [Si De Elementary School](https://osm.org/node/11492963069)
  * authentication:contactless
  * authentication:membership_card
* [Dingtai Village Activity Center](https://osm.org/node/11492963070)
  * authentication:contactless
  * authentication:membership_card
* [Beishih Activity Center](https://osm.org/node/11492963071)
  * authentication:contactless
  * authentication:membership_card
* [Wugong 1st Rd. / Wugong 5th Rd.](https://osm.org/node/11492963072)
  * authentication:contactless
  * authentication:membership_card
* [Houli Junior High School](https://osm.org/node/11492963073)
  * authentication:contactless
  * authentication:membership_card
* [Taiping Village Lianhe Village Joint Activity Center](https://osm.org/node/11492963074)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Animal Shelter Houli Park](https://osm.org/node/11492963075)
  * authentication:contactless
  * authentication:membership_card
* [Houli Environmental Park](https://osm.org/node/11492963076)
  * authentication:contactless
  * authentication:membership_card
* [Jiushe Community Activity Center](https://osm.org/node/11492963077)
  * authentication:contactless
  * authentication:membership_card
* [Hou-Zong Senior High School](https://osm.org/node/11492963078)
  * authentication:contactless
  * authentication:membership_card
* [Dafeng、Shangfeng Community Center](https://osm.org/node/11492963079)
  * authentication:contactless
  * authentication:membership_card
* [Hengshan Community Basketball Court](https://osm.org/node/11492963080)
  * authentication:contactless
  * authentication:membership_card
* [Sanhe Park](https://osm.org/node/11492963081)
  * authentication:contactless
  * authentication:membership_card
* [Daya District Ting 4 Parking Lot](https://osm.org/node/11492963082)
  * authentication:contactless
  * authentication:membership_card
* [Liubao Park](https://osm.org/node/11492963083)
  * authentication:contactless
  * authentication:membership_card
* [Bianzitouwei Bus Station](https://osm.org/node/11492963084)
  * authentication:contactless
  * authentication:membership_card
* [Dadu Yonghe Temple](https://osm.org/node/11492963085)
  * authentication:contactless
  * authentication:membership_card
* [遊園路二段170巷62號(東側)](https://osm.org/node/11492963086)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Dadu Rueijing Branch](https://osm.org/node/11492963087)
  * authentication:contactless
  * authentication:membership_card
* [Riguang Jun Community Center](https://osm.org/node/11492963088)
  * authentication:contactless
  * authentication:membership_card
* [Fufu Park](https://osm.org/node/11492963089)
  * authentication:contactless
  * authentication:membership_card
* [Qinci Park](https://osm.org/node/11492963090)
  * authentication:contactless
  * authentication:membership_card
* [Shengang Fire Park](https://osm.org/node/11492963091)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Shengang Branch](https://osm.org/node/11492963092)
  * authentication:contactless
  * authentication:membership_card
* [Feng Jou Elementary School](https://osm.org/node/11492963093)
  * authentication:contactless
  * authentication:membership_card
* [Gaotie E. 1st Rd. / Gaotie E. Rd.](https://osm.org/node/11492963094)
  * authentication:contactless
  * authentication:membership_card
* [Changshou Rd. / Jianxing Rd.](https://osm.org/node/11492963095)
  * authentication:contactless
  * authentication:membership_card
* [Wuri District Guang Ting 6 Parking Lot](https://osm.org/node/11492963096)
  * authentication:contactless
  * authentication:membership_card
* [三榮路二段/三榮十五路口(南側)](https://osm.org/node/11492963097)
  * authentication:contactless
  * authentication:membership_card
* [Zhenghe Park (Sanrong 13th Rd.)](https://osm.org/node/11492963098)
  * authentication:contactless
  * authentication:membership_card
* [Wuguang Village Elderly Hall](https://osm.org/node/11492963099)
  * authentication:contactless
  * authentication:membership_card
* [Dongyuan Village Daotian Park](https://osm.org/node/11492963100)
  * authentication:contactless
  * authentication:membership_card
* [Yongxing Temple](https://osm.org/node/11492963101)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong Road Bridge Walking Trail](https://osm.org/node/11492963102)
  * authentication:contactless
  * authentication:membership_card
* [TMRT HSR Taichung Station (Gaotie 1st Rd.)](https://osm.org/node/11492963103)
  * authentication:contactless
  * authentication:membership_card
* [Siwei Activity Center](https://osm.org/node/11492963104)
  * authentication:contactless
  * authentication:membership_card
* [Si Wei Elementary School](https://osm.org/node/11492963105)
  * authentication:contactless
  * authentication:membership_card
* [Sinan Police Station](https://osm.org/node/11492963106)
  * authentication:contactless
  * authentication:membership_card
* [Chung Gang Senior High School (Wenchang Rd.)](https://osm.org/node/11492963107)
  * authentication:contactless
  * authentication:membership_card
* [Wenchang Rd. / Zhongyang Rd. (Southeast)](https://osm.org/node/11492963108)
  * authentication:contactless
  * authentication:membership_card
* [Yung-Ning Elementary School](https://osm.org/node/11492963109)
  * authentication:contactless
  * authentication:membership_card
* [Ln 195 Dazhi Rd. Sec. 2 (West)](https://osm.org/node/11492963110)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Wubei Rd. (Southwest)](https://osm.org/node/11492963111)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Wubei Rd. (Northwest)](https://osm.org/node/11492963112)
  * authentication:contactless
  * authentication:membership_card
* [Zhongheng 1st Rd. Park](https://osm.org/node/11492963113)
  * authentication:contactless
  * authentication:membership_card
* [Dingliao Cultural Square](https://osm.org/node/11492963114)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Minhe Rd.](https://osm.org/node/11492963115)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Siwei E. Rd.](https://osm.org/node/11492963116)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Zhongyang Rd. (Northeast)](https://osm.org/node/11492963117)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Min'an Rd. (South)](https://osm.org/node/11492963118)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Rd. / Min'an Rd. (North)](https://osm.org/node/11492963119)
  * authentication:contactless
  * authentication:membership_card
* [Changchun Rd. / Minzu Rd.](https://osm.org/node/11492963120)
  * authentication:contactless
  * authentication:membership_card
* [Changchun Rd. / Zhonghua Rd.](https://osm.org/node/11492963121)
  * authentication:contactless
  * authentication:membership_card
* [Wuqi Zhongzheng Track and Field Stadium](https://osm.org/node/11492963122)
  * authentication:contactless
  * authentication:membership_card
* [Damaopu Parking Lot](https://osm.org/node/11492963123)
  * authentication:contactless
  * authentication:membership_card
* [Dongshi Forest District Office (Shuang Qi Work Station)](https://osm.org/node/11492963124)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Hakka Wellness Park](https://osm.org/node/11492963125)
  * authentication:contactless
  * authentication:membership_card
* [Da-Jia Primary School](https://osm.org/node/11492963126)
  * authentication:contactless
  * authentication:membership_card
* [De Yuan Temple](https://osm.org/node/11492963127)
  * authentication:contactless
  * authentication:membership_card
* [Shuen Tian Elementary School(Xinmei St.)](https://osm.org/node/11492963128)
  * authentication:contactless
  * authentication:membership_card
* [Xianren Rd. / Nong'an Rd. (Southwest)](https://osm.org/node/11492963129)
  * authentication:contactless
  * authentication:membership_card
* [Jiuzhangli Park](https://osm.org/node/11492963130)
  * authentication:contactless
  * authentication:membership_card
* [Dong Ming Elementary School](https://osm.org/node/11492963131)
  * authentication:contactless
  * authentication:membership_card
* [Dong'an Rd. / Wenqu Rd.](https://osm.org/node/11492963132)
  * authentication:contactless
  * authentication:membership_card
* [Wenwu Elementary School](https://osm.org/node/11492963133)
  * authentication:contactless
  * authentication:membership_card
* [Rilnan Elementary School](https://osm.org/node/11492963134)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. Sec.1 Ln. 568](https://osm.org/node/11492963135)
  * authentication:contactless
  * authentication:membership_card
* [Dajia District Ting 2 Parking Lot](https://osm.org/node/11492963136)
  * authentication:contactless
  * authentication:membership_card
* [Wenwu Park](https://osm.org/node/11492963138)
  * authentication:contactless
  * authentication:membership_card
* [Chih-Yung Senior High School](https://osm.org/node/11492963139)
  * authentication:contactless
  * authentication:membership_card
* [Giant Manufacturing Co., Ltd. Dajia Branch](https://osm.org/node/11492963140)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. Hongzhu Ln. Intersection](https://osm.org/node/11492963141)
  * authentication:contactless
  * authentication:membership_card
* [Nanshixi Kou](https://osm.org/node/11492963142)
  * authentication:contactless
  * authentication:membership_card
* [Nandou Temple](https://osm.org/node/11492963143)
  * authentication:contactless
  * authentication:membership_card
* [Kung Ming Elementary School](https://osm.org/node/11492963144)
  * authentication:contactless
  * authentication:membership_card
* [Shatian Rd. / Tianren St.](https://osm.org/node/11492963145)
  * authentication:contactless
  * authentication:membership_card
* [Doudi Community Center](https://osm.org/node/11492963146)
  * authentication:contactless
  * authentication:membership_card
* [Mingxiu Park](https://osm.org/node/11492963147)
  * authentication:contactless
  * authentication:membership_card
* [Qingquan Community Center](https://osm.org/node/11492963148)
  * authentication:contactless
  * authentication:membership_card
* [Xiangshang Rd. Sec. 7 / Taiping Rd. (Southwest)](https://osm.org/node/11492963149)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Municipal Gong-Ming Junior High School](https://osm.org/node/11492963150)
  * authentication:contactless
  * authentication:membership_card
* [Lu Fung Elementary School (Xinghe Rd.)](https://osm.org/node/11492963151)
  * authentication:contactless
  * authentication:membership_card
* [Lufeng Park](https://osm.org/node/11492963152)
  * authentication:contactless
  * authentication:membership_card
* [Shalu District Gongguan Park](https://osm.org/node/11492963153)
  * authentication:contactless
  * authentication:membership_card
* [Qingshan Park](https://osm.org/node/11492963154)
  * authentication:contactless
  * authentication:membership_card
* [Qingshui Station (Rear Station Parking Lot)](https://osm.org/node/11492963155)
  * authentication:contactless
  * authentication:membership_card
* [Qiaojiang N. St. / Zhenping Rd.](https://osm.org/node/11492963156)
  * authentication:contactless
  * authentication:membership_card
* [Qingshui Minzu Rd. / Minhe Rd.](https://osm.org/node/11492963157)
  * authentication:contactless
  * authentication:membership_card
* [Kanglang Community Center](https://osm.org/node/11492963158)
  * authentication:contactless
  * authentication:membership_card
* [Jian-Guo Elementary School](https://osm.org/node/11492963159)
  * authentication:contactless
  * authentication:membership_card
* [Zhongyang Rd. / Zhenping Rd.](https://osm.org/node/11492963160)
  * authentication:contactless
  * authentication:membership_card
* [Renai Rd. / Xinyi Rd.](https://osm.org/node/11492963161)
  * authentication:contactless
  * authentication:membership_card
* [Renai Rd. / Minhe Rd.](https://osm.org/node/11492963162)
  * authentication:contactless
  * authentication:membership_card
* [Jianan Rd. / Zhongshan Rd. (Southwest)](https://osm.org/node/11492963163)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Qingshui Branch](https://osm.org/node/11492963164)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. / Zhongzheng St.](https://osm.org/node/11492963165)
  * authentication:contactless
  * authentication:membership_card
* [Zhonghua Rd. / Minzu Rd.](https://osm.org/node/11492963166)
  * authentication:contactless
  * authentication:membership_card
* [Zhonghua Rd. / Nanhua Rd.](https://osm.org/node/11492963167)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. / Xinghe Rd.](https://osm.org/node/11492963168)
  * authentication:contactless
  * authentication:membership_card
* [Niupuzi Sports Park (Minquan 1st St.)](https://osm.org/node/11492963169)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan Rd. / Wuquan S. Rd.](https://osm.org/node/11492963170)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan S. Rd. / Pingdeng Rd.](https://osm.org/node/11492963171)
  * authentication:contactless
  * authentication:membership_card
* [Yugang Rd. / Wenxing Rd.](https://osm.org/node/11492963172)
  * authentication:contactless
  * authentication:membership_card
* [Yugang Rd. / Haibin Rd.](https://osm.org/node/11492963173)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan Rd. / Minxiang Rd.](https://osm.org/node/11492963174)
  * authentication:contactless
  * authentication:membership_card
* [Qiaotou Community Center](https://osm.org/node/11492963175)
  * authentication:contactless
  * authentication:membership_card
* [Hsi-Ning Elementary School (Wuquan E. Rd.)](https://osm.org/node/11492963176)
  * authentication:contactless
  * authentication:membership_card
* [Shengwang Temple](https://osm.org/node/11492963177)
  * authentication:contactless
  * authentication:membership_card
* [Zhongshan Rd. / Zhongxing St.](https://osm.org/node/11492963178)
  * authentication:contactless
  * authentication:membership_card
* [Wu-Tso Elementary School](https://osm.org/node/11492963179)
  * authentication:contactless
  * authentication:membership_card
* [Flight Line Recreation Relay Station](https://osm.org/node/11492963180)
  * authentication:contactless
  * authentication:membership_card
* [Dayang Fuel Depot Recreation Park](https://osm.org/node/11492963181)
  * authentication:contactless
  * authentication:membership_card
* [Shilaitou Park](https://osm.org/node/11492963182)
  * authentication:contactless
  * authentication:membership_card
* [Zhonghua Rd. / Zhongqing Rd.](https://osm.org/node/11492963183)
  * authentication:contactless
  * authentication:membership_card
* [Heping New Community](https://osm.org/node/11492963184)
  * authentication:contactless
  * authentication:membership_card
* [Kuang-Hwa Vocational Senior High School](https://osm.org/node/11492963185)
  * authentication:contactless
  * authentication:membership_card
* [Taiping District Guang 1 Parking Lot](https://osm.org/node/11492963186)
  * authentication:contactless
  * authentication:membership_card
* [Yixin Park](https://osm.org/node/11492963187)
  * authentication:contactless
  * authentication:membership_card
* [Dexing Park (Xin'an St.)](https://osm.org/node/11492963188)
  * authentication:contactless
  * authentication:membership_card
* [Sinan Park](https://osm.org/node/11492963189)
  * authentication:contactless
  * authentication:membership_card
* [Taiping Village Yongfeng Park](https://osm.org/node/11492963190)
  * authentication:contactless
  * authentication:membership_card
* [Jianguo Park](https://osm.org/node/11492963191)
  * authentication:contactless
  * authentication:membership_card
* [Wayao Rd. / Dali Rd. (Southwest)](https://osm.org/node/11492963192)
  * authentication:contactless
  * authentication:membership_card
* [Tianyou Temple](https://osm.org/node/11492963193)
  * authentication:contactless
  * authentication:membership_card
* [Ln. 1316, Wenxin S. Rd. (Northwest)](https://osm.org/node/11492963194)
  * authentication:contactless
  * authentication:membership_card
* [Xiangxing Rd. / Zhongming S. Rd. Ln. 1240](https://osm.org/node/11492963195)
  * authentication:contactless
  * authentication:membership_card
* [Dongming Park (South)](https://osm.org/node/11492963196)
  * authentication:contactless
  * authentication:membership_card
* [Donghu Park (Keji Rd.)](https://osm.org/node/11492963197)
  * authentication:contactless
  * authentication:membership_card
* [Shuangwen Junior High School (Yonglong Rd.)](https://osm.org/node/11492963198)
  * authentication:contactless
  * authentication:membership_card
* [Provincial Highway 3 / Ln. 2 Sec. 1 Zhongxing Rd](https://osm.org/node/11492963199)
  * authentication:contactless
  * authentication:membership_card
* [Liyuan Rd. / Liren Rd.](https://osm.org/node/11492963200)
  * authentication:contactless
  * authentication:membership_card
* [Jiati 6th St. Park](https://osm.org/node/11492963201)
  * authentication:contactless
  * authentication:membership_card
* [Dajhih Rd. / Qiaocheng Rd. (Southeast)](https://osm.org/node/11492963202)
  * authentication:contactless
  * authentication:membership_card
* [Shijiujia Jiankang Park](https://osm.org/node/11492963203)
  * authentication:contactless
  * authentication:membership_card
* [Liancun Rd. / Xianghe E. Rd.](https://osm.org/node/11492963204)
  * authentication:contactless
  * authentication:membership_card
* [Wengzi Park](https://osm.org/node/11492963205)
  * authentication:contactless
  * authentication:membership_card
* [Fengshi Rd. / Wannian Rd.](https://osm.org/node/11492963206)
  * authentication:contactless
  * authentication:membership_card
* [Fengyuan Wannian Palace Temple](https://osm.org/node/11492963207)
  * authentication:contactless
  * authentication:membership_card
* [Yuanhuan E. Rd. / Taichung Overpass](https://osm.org/node/11492963208)
  * authentication:contactless
  * authentication:membership_card
* [Fengyuan Huludun Post Office](https://osm.org/node/11492963209)
  * authentication:contactless
  * authentication:membership_card
* [Fengyuan Zhongzheng Park (Shuiyuan Rd.)](https://osm.org/node/11492963210)
  * authentication:contactless
  * authentication:membership_card
* [Ln. 460 Fengyuan Blvd.](https://osm.org/node/11492963211)
  * authentication:contactless
  * authentication:membership_card
* [Fengyuan Blvd. / Fengke Rd.](https://osm.org/node/11492963212)
  * authentication:contactless
  * authentication:membership_card
* [Fengyuan Wuming Shan Park](https://osm.org/node/11492963213)
  * authentication:contactless
  * authentication:membership_card
* [Yuying Parking Lot](https://osm.org/node/11492963214)
  * authentication:contactless
  * authentication:membership_card
* [Taichung City Fengyuan District Stadium](https://osm.org/node/11492963215)
  * authentication:contactless
  * authentication:membership_card
* [Zunliao Village Activity Center](https://osm.org/node/11492963216)
  * authentication:contactless
  * authentication:membership_card
* [Ping An Park](https://osm.org/node/11492963217)
  * authentication:contactless
  * authentication:membership_card
* [Shengli Fangchengshi](https://osm.org/node/11492963218)
  * authentication:contactless
  * authentication:membership_card
* [Songzhu 5th Rd. / Junrong 5th St.](https://osm.org/node/11492963219)
  * authentication:contactless
  * authentication:membership_card
* [Jingfu Park (Jingxian 2nd Rd.)](https://osm.org/node/11492963220)
  * authentication:contactless
  * authentication:membership_card
* [Nanxing 2nd Rd. / Nanxing Rd. (Northeast)](https://osm.org/node/11492963221)
  * authentication:contactless
  * authentication:membership_card
* [Hanxi W. Rd. / Nanxing 2nd Rd. (East)](https://osm.org/node/11492963222)
  * authentication:contactless
  * authentication:membership_card
* [Budingzi Park](https://osm.org/node/11492963223)
  * authentication:contactless
  * authentication:membership_card
* [Park 2-3](https://osm.org/node/11492963224)
  * authentication:contactless
  * authentication:membership_card
* [Songzhu Rd. / Hanxi E. Rd. (Southwest)](https://osm.org/node/11492963225)
  * authentication:contactless
  * authentication:membership_card
* [823 Memorial Park (Chongde 10th Rd.)](https://osm.org/node/11492963226)
  * authentication:contactless
  * authentication:membership_card
* [Jianhe Rd. / Junfu 11th Rd.](https://osm.org/node/11492963227)
  * authentication:contactless
  * authentication:membership_card
* [Dunfu 1st St. / Dunfu Rd. (Southwest)](https://osm.org/node/11492963228)
  * authentication:contactless
  * authentication:membership_card
* [Beitun Rd. / Nanxing N. 1st Rd. (Southeast)](https://osm.org/node/11492963229)
  * authentication:contactless
  * authentication:membership_card
* [Beitun Rd. / Nanxing N. 1st Rd. (Northeast)](https://osm.org/node/11492963230)
  * authentication:contactless
  * authentication:membership_card
* [Dunfu 6th St. / Nanxing Rd.](https://osm.org/node/11492963231)
  * authentication:contactless
  * authentication:membership_card
* [Taiyuan Football Stadium](https://osm.org/node/11492963232)
  * authentication:contactless
  * authentication:membership_card
* [Mayuan E. St. / Shenyang Rd.](https://osm.org/node/11492963233)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin Rd. / Beitun Rd. (Northwest)](https://osm.org/node/11492963234)
  * authentication:contactless
  * authentication:membership_card
* [Er 45 Green Space](https://osm.org/node/11492963235)
  * authentication:contactless
  * authentication:membership_card
* [Shuijing Meigui Park](https://osm.org/node/11492963236)
  * authentication:contactless
  * authentication:membership_card
* [Sanguang Children's Play Park](https://osm.org/node/11492963237)
  * authentication:contactless
  * authentication:membership_card
* [Taihe 6th St. / Jianhe Rd.](https://osm.org/node/11492963238)
  * authentication:contactless
  * authentication:membership_card
* [Taiyuan Rd. / Buzi Rd. (Southeast)](https://osm.org/node/11492963239)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong Rd. / Fengle Rd. (Southeast)](https://osm.org/node/11492963240)
  * authentication:contactless
  * authentication:membership_card
* [Fengle Rd. Parking Lot](https://osm.org/node/11492963241)
  * authentication:contactless
  * authentication:membership_card
* [Dongshan Motorcycle Parking Lot](https://osm.org/node/11492963242)
  * authentication:contactless
  * authentication:membership_card
* [Dongshan Rd. / Jhongsing Ln.](https://osm.org/node/11492963243)
  * authentication:contactless
  * authentication:membership_card
* [Junfu Park (Junfu 18th Rd.)](https://osm.org/node/11492963244)
  * authentication:contactless
  * authentication:membership_card
* [Taishun Rd. / Taihe Rd.](https://osm.org/node/11492963245)
  * authentication:contactless
  * authentication:membership_card
* [Erfenpu Ecology Park (Shanxi Rd.)](https://osm.org/node/11492963246)
  * authentication:contactless
  * authentication:membership_card
* [Jiushe Park (Beitun Rd. Ln. 439-17)](https://osm.org/node/11492963247)
  * authentication:contactless
  * authentication:membership_card
* [Chongde Rd. / Beiping Rd. (Southwest)](https://osm.org/node/11492963248)
  * authentication:contactless
  * authentication:membership_card
* [Chongde Rd. / Qingdao Rd. (Southeast)](https://osm.org/node/11492963249)
  * authentication:contactless
  * authentication:membership_card
* [Chongde Park](https://osm.org/node/11492963250)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin Rd. / Rehe Rd.](https://osm.org/node/11492963251)
  * authentication:contactless
  * authentication:membership_card
* [Shuinan Park](https://osm.org/node/11492963252)
  * authentication:contactless
  * authentication:membership_card
* [Zhongqing Rd. / Wenxin Rd. (Southeast)](https://osm.org/node/11492963253)
  * authentication:contactless
  * authentication:membership_card
* [Chongde 8th Rd. / Meichuan E. Rd.](https://osm.org/node/11492963254)
  * authentication:contactless
  * authentication:membership_card
* [Sanguang Parking Lot](https://osm.org/node/11492963255)
  * authentication:contactless
  * authentication:membership_card
* [Xiangshun Rd. / Junfu 7th Rd.](https://osm.org/node/11492963256)
  * authentication:contactless
  * authentication:membership_card
* [Bo-ai Park](https://osm.org/node/11492963257)
  * authentication:contactless
  * authentication:membership_card
* [Sin Dou Ecology Park (Hexiang 5th St.)](https://osm.org/node/11492963258)
  * authentication:contactless
  * authentication:membership_card
* [Jianhe Rd. / Dongshan Rd.](https://osm.org/node/11492963259)
  * authentication:contactless
  * authentication:membership_card
* [Songzhu N. 1st St. / Songzhu Rd. Sec. 3](https://osm.org/node/11492963260)
  * authentication:contactless
  * authentication:membership_card
* [Songzhu Village Neighborhood Watch Team](https://osm.org/node/11492963261)
  * authentication:contactless
  * authentication:membership_card
* [Shuijing Park](https://osm.org/node/11492963262)
  * authentication:contactless
  * authentication:membership_card
* [Buzi Elementary School](https://osm.org/node/11492963263)
  * authentication:contactless
  * authentication:membership_card
* [Xiangshun Rd. / Taishun Rd. (East)](https://osm.org/node/11492963264)
  * authentication:contactless
  * authentication:membership_card
* [Songzhu 5th Rd. / Shuijing St.](https://osm.org/node/11492963265)
  * authentication:contactless
  * authentication:membership_card
* [Jingxian Park](https://osm.org/node/11492963266)
  * authentication:contactless
  * authentication:membership_card
* [Indigenous People's Park](https://osm.org/node/11492963267)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong E. Rd. / Taiyuan Rd.](https://osm.org/node/11492963268)
  * authentication:contactless
  * authentication:membership_card
* [Guang Jian Ting 8 Parking Lot](https://osm.org/node/11492963269)
  * authentication:contactless
  * authentication:membership_card
* [Eco Outdoor Education Park](https://osm.org/node/11492963270)
  * authentication:contactless
  * authentication:membership_card
* [Xiangshun Rd. / Taishun Rd.](https://osm.org/node/11492963271)
  * authentication:contactless
  * authentication:membership_card
* [Central Taiwan University of Science and Technology (Southwest)](https://osm.org/node/11492963272)
  * authentication:contactless
  * authentication:membership_card
* [Ecological water purification experience park](https://osm.org/node/11492963273)
  * authentication:contactless
  * authentication:membership_card
* [Dajin Parking lot](https://osm.org/node/11492963274)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong Rd. Xinmin Ln. Intersection](https://osm.org/node/11492963275)
  * authentication:contactless
  * authentication:membership_card
* [Dadun Rd. / Dadun 11th St. (Northwest)](https://osm.org/node/11492963276)
  * authentication:contactless
  * authentication:membership_card
* [Sanyi Children's Park](https://osm.org/node/11492963277)
  * authentication:contactless
  * authentication:membership_card
* [Li Xin Park (Longfu Rd.)](https://osm.org/node/11492963278)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan W. Rd. / Ln. 870, Sec.2, Nantun Rd. (Southeast)](https://osm.org/node/11492963279)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan W. Rd. / Sancuo St. (Northwest)](https://osm.org/node/11492963280)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan W. Rd. / Ln. 870, Sec.2, Nantun Rd. (Southwest)](https://osm.org/node/11492963281)
  * authentication:contactless
  * authentication:membership_card
* [High Speed Rail New Town Ecological Park (Longfu 7th Rd.)](https://osm.org/node/11492963282)
  * authentication:contactless
  * authentication:membership_card
* [High Speed Rail New Town Ecological Park (Longfu 9th Rd.)](https://osm.org/node/11492963283)
  * authentication:contactless
  * authentication:membership_card
* [Yongchun E. 3rd Rd. / Yongshun Rd.](https://osm.org/node/11492963284)
  * authentication:contactless
  * authentication:membership_card
* [Baoshan Village Activity Center (West)](https://osm.org/node/11492963285)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Fish Market](https://osm.org/node/11492963286)
  * authentication:contactless
  * authentication:membership_card
* [Yongchun Rd. / Xinfu Rd. (South)](https://osm.org/node/11492963287)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan W. Rd. / Wenxin S. 5th Rd.](https://osm.org/node/11492963288)
  * authentication:contactless
  * authentication:membership_card
* [Longfu 17th Rd. / Wenxin S. 5th Rd.](https://osm.org/node/11492963289)
  * authentication:contactless
  * authentication:membership_card
* [Xiangshang Rd. / Huanzhong Rd. (Northeast)](https://osm.org/node/11492963290)
  * authentication:contactless
  * authentication:membership_card
* [Fazi Riverway (South)](https://osm.org/node/11492963291)
  * authentication:contactless
  * authentication:membership_card
* [Li Xin Park (Longfu 20th Rd.)](https://osm.org/node/11492963292)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin S. 9th Rd. / Yongchun E. 1st Rd.](https://osm.org/node/11492963293)
  * authentication:contactless
  * authentication:membership_card
* [Fengfu Park (Jingcheng S. Rd. / Wenxin S. 5th Rd.)](https://osm.org/node/11492963294)
  * authentication:contactless
  * authentication:membership_card
* [Xinfu 2nd St. Park](https://osm.org/node/11492963295)
  * authentication:contactless
  * authentication:membership_card
* [Aly. 3 Ln. 115 Gancheng St.](https://osm.org/node/11492963296)
  * authentication:contactless
  * authentication:membership_card
* [Bo-ai St. / Daye Rd. (Southwest)](https://osm.org/node/11492963297)
  * authentication:contactless
  * authentication:membership_card
* [Huaide Basketball Court](https://osm.org/node/11492963298)
  * authentication:contactless
  * authentication:membership_card
* [Dadun 12th St. Public Parking Lot](https://osm.org/node/11492963299)
  * authentication:contactless
  * authentication:membership_card
* [Yiwen 2nd St. / Yongchun Rd.](https://osm.org/node/11492963300)
  * authentication:contactless
  * authentication:membership_card
* [Nanyuan Park (Wenxin S. 3rd Rd.)](https://osm.org/node/11492963301)
  * authentication:contactless
  * authentication:membership_card
* [The Taichung City Precision Machinery Innovation Technology Park](https://osm.org/node/11492963302)
  * authentication:contactless
  * authentication:membership_card
* [Rainbow Village](https://osm.org/node/11492963303)
  * authentication:contactless
  * authentication:membership_card
* [Wen-Xin Forest Park (Dadun 7th St.)](https://osm.org/node/11492963304)
  * authentication:contactless
  * authentication:membership_card
* [Yishan Temple (Next to the Park)](https://osm.org/node/11492963305)
  * authentication:contactless
  * authentication:membership_card
* [Sie He Elementary School](https://osm.org/node/11492963306)
  * authentication:contactless
  * authentication:membership_card
* [Ln. 654 Fuxing Rd.](https://osm.org/node/11492963307)
  * authentication:contactless
  * authentication:membership_card
* [Anhe Rd. / Xiehe S. Lane](https://osm.org/node/11492963308)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Metro Park](https://osm.org/node/11492963309)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Exprimental Deer Farm](https://osm.org/node/11492963310)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Student Residence Hall, Second Teaching Area](https://osm.org/node/11492963311)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_College of Management](https://osm.org/node/11492963312)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Tunghai Dairy](https://osm.org/node/11492963313)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Women's Residence Hall](https://osm.org/node/11492963314)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Basic Science Building](https://osm.org/node/11492963315)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Luce Chapel](https://osm.org/node/11492963316)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Men's Residence Hall](https://osm.org/node/11492963317)
  * authentication:contactless
  * authentication:membership_card
* [Tunghai University_Languge Building](https://osm.org/node/11492963318)
  * authentication:contactless
  * authentication:membership_card
* [Fulin Rd. / Guo-an 2nd Rd.](https://osm.org/node/11492963319)
  * authentication:contactless
  * authentication:membership_card
* [Fu Ke Rd. / Fu Ke Rd. Ln. 658](https://osm.org/node/11492963320)
  * authentication:contactless
  * authentication:membership_card
* [Wenxiao 91 Football Stadium](https://osm.org/node/11492963321)
  * authentication:contactless
  * authentication:membership_card
* [AnHer Junior High School (Shuiwei Ln.)](https://osm.org/node/11492963322)
  * authentication:contactless
  * authentication:membership_card
* [Da Ren Elementary School](https://osm.org/node/11492963323)
  * authentication:contactless
  * authentication:membership_card
* [Huanzhong Rd. / Shizheng S. 1st Rd. (Northwest)](https://osm.org/node/11492963324)
  * authentication:contactless
  * authentication:membership_card
* [Malongtan Parking Lot](https://osm.org/node/11492963325)
  * authentication:contactless
  * authentication:membership_card
* [Huizhong Rd. Parking Lot](https://osm.org/node/11492963326)
  * authentication:contactless
  * authentication:membership_card
* [Jingmao 5th Rd. / Qlaoda 8th St.](https://osm.org/node/11492963327)
  * authentication:contactless
  * authentication:membership_card
* [Anhe Park](https://osm.org/node/11492963328)
  * authentication:contactless
  * authentication:membership_card
* [Shangshi Rd. / Huilai Rd.](https://osm.org/node/11492963329)
  * authentication:contactless
  * authentication:membership_card
* [He-an Village Activity Center](https://osm.org/node/11492963330)
  * authentication:contactless
  * authentication:membership_card
* [Xitun Yongxing Temple](https://osm.org/node/11492963331)
  * authentication:contactless
  * authentication:membership_card
* [Bazhangli Water Treatment Plant](https://osm.org/node/11492963332)
  * authentication:contactless
  * authentication:membership_card
* [Dapeng New Community](https://osm.org/node/11492963333)
  * authentication:contactless
  * authentication:membership_card
* [Huamei Park](https://osm.org/node/11492963334)
  * authentication:contactless
  * authentication:membership_card
* [Taiwan Blvd. / Sichuan 5th St.](https://osm.org/node/11492963335)
  * authentication:contactless
  * authentication:membership_card
* [Huizhong Rd. / Shizheng Rd. (Northeast)](https://osm.org/node/11492963336)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin Rd. / Shizheng N. 1st Rd.](https://osm.org/node/11492963337)
  * authentication:contactless
  * authentication:membership_card
* [Huishun Park](https://osm.org/node/11492963338)
  * authentication:contactless
  * authentication:membership_card
* [Zhongqing Rd. / Guangxing Ln.](https://osm.org/node/11492963339)
  * authentication:contactless
  * authentication:membership_card
* [Zhongke Rd. / Fuya Rd.](https://osm.org/node/11492963340)
  * authentication:contactless
  * authentication:membership_card
* [Sanjiaopu (Fuya Rd.)](https://osm.org/node/11492963341)
  * authentication:contactless
  * authentication:membership_card
* [Chaogui Park](https://osm.org/node/11492963342)
  * authentication:contactless
  * authentication:membership_card
* [Chung Shan Junior High School](https://osm.org/node/11492963343)
  * authentication:contactless
  * authentication:membership_card
* [Huilai Park](https://osm.org/node/11492963344)
  * authentication:contactless
  * authentication:membership_card
* [Xian St. Ln. 277 Aly. 6 (North)](https://osm.org/node/11492963345)
  * authentication:contactless
  * authentication:membership_card
* [Zhonggong 3rd Rd. / Tianyou St.](https://osm.org/node/11492963346)
  * authentication:contactless
  * authentication:membership_card
* [Jingmao 5th Rd. / Qiaoda Rd.](https://osm.org/node/11492963347)
  * authentication:contactless
  * authentication:membership_card
* [Chaoma Parking Lot](https://osm.org/node/11492963348)
  * authentication:contactless
  * authentication:membership_card
* [Hongen Park](https://osm.org/node/11492963349)
  * authentication:contactless
  * authentication:membership_card
* [Longtan Village Activity Center](https://osm.org/node/11492963350)
  * authentication:contactless
  * authentication:membership_card
* [Guofong Guorui St. Intersection](https://osm.org/node/11492963351)
  * authentication:contactless
  * authentication:membership_card
* [Shangde St. / Yude Rd.](https://osm.org/node/11492963352)
  * authentication:contactless
  * authentication:membership_card
* [Qingdao Rd. / Shanxi Rd. (West)](https://osm.org/node/11492963353)
  * authentication:contactless
  * authentication:membership_card
* [Wuyi St. / Chongde Rd.](https://osm.org/node/11492963354)
  * authentication:contactless
  * authentication:membership_card
* [Shanxi Rd. / Taiyuan Rd.](https://osm.org/node/11492963355)
  * authentication:contactless
  * authentication:membership_card
* [Sheeng San Elementary School](https://osm.org/node/11492963356)
  * authentication:contactless
  * authentication:membership_card
* [Zhushi Yuanman Park](https://osm.org/node/11492963357)
  * authentication:contactless
  * authentication:membership_card
* [Xueshi Rd. / Dehua St.](https://osm.org/node/11492963358)
  * authentication:contactless
  * authentication:membership_card
* [Taichung University of Science and Technology (Jinping St.)](https://osm.org/node/11492963359)
  * authentication:contactless
  * authentication:membership_card
* [Li-Jen Junior High School (Tianjin Rd.)](https://osm.org/node/11492963360)
  * authentication:contactless
  * authentication:membership_card
* [Du-Xing Elementary School (Yingcai Rd.)](https://osm.org/node/11492963361)
  * authentication:contactless
  * authentication:membership_card
* [Dongcheng 3rd St. / Dongguang 5th St.](https://osm.org/node/11492963362)
  * authentication:contactless
  * authentication:membership_card
* [Dongguang Rd. / Dongguang E. St.](https://osm.org/node/11492963363)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Public Library Jingwu Branch](https://osm.org/node/11492963364)
  * authentication:contactless
  * authentication:membership_card
* [Dongguang Rd. / Dongcheng Rd. (West)](https://osm.org/node/11492963365)
  * authentication:contactless
  * authentication:membership_card
* [Shuangshi Rd. / Yule St.](https://osm.org/node/11492963366)
  * authentication:contactless
  * authentication:membership_card
* [Daqing St. Sec.2 Ln. 7 / Daqing St. (Southwest)](https://osm.org/node/11492963367)
  * authentication:contactless
  * authentication:membership_card
* [Xichuan 2nd Rd. / Xichuan 1st Rd. (Southwest)](https://osm.org/node/11492963368)
  * authentication:contactless
  * authentication:membership_card
* [Daqing St. Sec. 1 Ln. 75 (East)](https://osm.org/node/11492963369)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Overpass (Dongxing Rd.)](https://osm.org/node/11492963370)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin S. Rd. / Wenxin S. 10th Rd.](https://osm.org/node/11492963371)
  * authentication:contactless
  * authentication:membership_card
* [Zhongming S. Rd. / Gongxue N. Rd.](https://osm.org/node/11492963372)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Industrial High School (Gaogong Rd. / Gongxue 6th St.)](https://osm.org/node/11492963373)
  * authentication:contactless
  * authentication:membership_card
* [Wenxin S. Rd. / Dazhong S. St.](https://osm.org/node/11492963374)
  * authentication:contactless
  * authentication:membership_card
* [Zhongming S. Rd. / Nanhe Rd.](https://osm.org/node/11492963375)
  * authentication:contactless
  * authentication:membership_card
* [Zhongxiao Rd. Ln. 55](https://osm.org/node/11492963376)
  * authentication:contactless
  * authentication:membership_card
* [Daqing Park](https://osm.org/node/11492963377)
  * authentication:contactless
  * authentication:membership_card
* [Ln. 114 Youheng St.](https://osm.org/node/11492963378)
  * authentication:contactless
  * authentication:membership_card
* [Liuchuan W. Rd. / Zhongming S. Rd.](https://osm.org/node/11492963379)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan Rd. / Jianguo N. Rd. (Southeast)](https://osm.org/node/11492963380)
  * authentication:contactless
  * authentication:membership_card
* [Wuquan Rd. / Jianguo N. Rd. (Southwest)](https://osm.org/node/11492963381)
  * authentication:contactless
  * authentication:membership_card
* [Ln. 777 Zhongming S. Rd.](https://osm.org/node/11492963382)
  * authentication:contactless
  * authentication:membership_card
* [Sanmin W. Rd. / Liuchuan W. Rd,](https://osm.org/node/11492963383)
  * authentication:contactless
  * authentication:membership_card
* [Dazhong S. St. / Sanmin W. Rd.](https://osm.org/node/11492963384)
  * authentication:contactless
  * authentication:membership_card
* [Liuchuan E. Rd. / Sanmin W. Rd.](https://osm.org/node/11492963385)
  * authentication:contactless
  * authentication:membership_card
* [Xingda Rd. / Meicun Rd.](https://osm.org/node/11492963386)
  * authentication:contactless
  * authentication:membership_card
* [Zhongxing Land Office](https://osm.org/node/11492963387)
  * authentication:contactless
  * authentication:membership_card
* [Chungming Senior High School (Huamei St.)](https://osm.org/node/11492963388)
  * authentication:contactless
  * authentication:membership_card
* [Jianguo Rd. / Minsheng Rd. (Southeast)](https://osm.org/node/11492963389)
  * authentication:contactless
  * authentication:membership_card
* [Chung-Cheng Elementary School (Ln. 179, Sec 2, Taiwan Blvd.)](https://osm.org/node/11492963390)
  * authentication:contactless
  * authentication:membership_card
* [Taiwan Blvd. / Ln. 179, Sec 2, Taiwan Blvd.](https://osm.org/node/11492963391)
  * authentication:contactless
  * authentication:membership_card
* [Da Yung Elementary School (Wuquan 8th St.)](https://osm.org/node/11492963392)
  * authentication:contactless
  * authentication:membership_card
* [Chungming Senior High School (Boguan Rd. / Huamei St.)](https://osm.org/node/11492963393)
  * authentication:contactless
  * authentication:membership_card
* [Liuchuan E. Rd. / Zizhi St.](https://osm.org/node/11492963394)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Municipal Chu Jen Junior High School](https://osm.org/node/11492963395)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Overpass (Linsen Rd.)](https://osm.org/node/11492963396)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Overpass (Kangle St.)](https://osm.org/node/11492963397)
  * authentication:contactless
  * authentication:membership_card
* [Jianguo Rd. / Taichung Rd.](https://osm.org/node/11492963398)
  * authentication:contactless
  * authentication:membership_card
* [Gongyi Park](https://osm.org/node/11492963399)
  * authentication:contactless
  * authentication:membership_card
* [The Affiliated Taichung Senior Agricultural Vocational High School of NCHU (Jianzhong St.)](https://osm.org/node/11492963400)
  * authentication:contactless
  * authentication:membership_card
* [Hanping Park](https://osm.org/node/11492963401)
  * authentication:contactless
  * authentication:membership_card
* [Family Education Center (Sanxian St.)](https://osm.org/node/11492963402)
  * authentication:contactless
  * authentication:membership_card
* [First Square Park](https://osm.org/node/11492963403)
  * authentication:contactless
  * authentication:membership_card
* [Daxing St. / Jiangong St.](https://osm.org/node/11492963404)
  * authentication:contactless
  * authentication:membership_card
* [Dajhih Park (Lide E. St.)](https://osm.org/node/11492963405)
  * authentication:contactless
  * authentication:membership_card
* [Renhe Park](https://osm.org/node/11492963406)
  * authentication:contactless
  * authentication:membership_card
* [Xingda Rd. / Qizhong Rd.](https://osm.org/node/11492963407)
  * authentication:contactless
  * authentication:membership_card
* [Dongying Rd. / Ziyou Rd.](https://osm.org/node/11492963408)
  * authentication:contactless
  * authentication:membership_card
* [Wonderful Manor_Chagall Community](https://osm.org/node/11492963409)
  * authentication:contactless
  * authentication:membership_card
* [Taichung Bus Station](https://osm.org/node/11492963410)
  * authentication:contactless
  * authentication:membership_card
* [Jiancheng Rd./Leye Rd.](https://osm.org/node/11492963411)
  * authentication:contactless
  * authentication:membership_card
* [Le Cheng Temple](https://osm.org/node/11492963412)
  * authentication:contactless
  * authentication:membership_card
* [Show Time Square](https://osm.org/node/11492963413)
  * authentication:contactless
  * authentication:membership_card
* [The Affiliated Taichung Senior Agricultural Vocational High School of NCHU](https://osm.org/node/11492963414)
  * authentication:contactless
  * authentication:membership_card
* [Dazhi Elementary School](https://osm.org/node/11492963415)
  * authentication:contactless
  * authentication:membership_card
* [Jin-De Elementary School](https://osm.org/node/11492963416)
  * authentication:contactless
  * authentication:membership_card
* [Liuyuan Presbyterian Church](https://osm.org/node/11492963417)
  * authentication:contactless
  * authentication:membership_card
* [Pingtung City Office](https://osm.org/node/11771503985)
  * name:zh-Hant
  * network:wikidata
  * payment:ep_easycard
* [Pingtung City Office](https://osm.org/node/11771503986)
  * name:zh-Hant
  * network:wikidata
  * payment:ep_easycard
* [屏東菸廠站](https://osm.org/node/11846819162)
  * name:en
  * network:wikidata
  * payment:ep_easycard
* [NPTU Minsheng Campus](https://osm.org/node/11888727720)
  * network:wikidata
  * payment:ep_easycard
* [MRT Muzha Sta. (West)](https://osm.org/node/12020437367)
  * authentication:contactless
  * authentication:membership_card
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [榮耀兒童公園](https://osm.org/node/12039432234)
  * fee
  * name:en
* [Ln. 125, DeChang St.](https://osm.org/node/12041261616)
  * authentication:contactless
  * authentication:membership_card
  * payment:credit_cards
  * payment:ep_easycard
  * payment:ep_ipass
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [彰化高商](https://osm.org/node/12141739423)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [員東晴雨球場](https://osm.org/node/12141739424)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [員林地稅分局](https://osm.org/node/12141739425)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [和群國中](https://osm.org/node/12141739426)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [花壇火車站](https://osm.org/node/12141739427)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [花壇鄉公所](https://osm.org/node/12141739428)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [花壇圖書館](https://osm.org/node/12141739429)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [花壇國小](https://osm.org/node/12141739430)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [花壇國中](https://osm.org/node/12141739431)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [虎山巖](https://osm.org/node/12141739432)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [文德宮](https://osm.org/node/12141739433)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
* [Tianzhong High School](https://osm.org/node/12141739434)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Tianzhong Township Farmers' Association](https://osm.org/node/12141739435)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Santian Elementary School](https://osm.org/node/12141739436)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Tianzhong Township Office](https://osm.org/node/12141739437)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Tianzhong Police Station](https://osm.org/node/12141739438)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Xinmin Elementary School](https://osm.org/node/12141739439)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Tianzhong Train Station](https://osm.org/node/12141739440)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [High-Speed Rail Changhua Station](https://osm.org/node/12141739441)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yanping Park](https://osm.org/node/12141739442)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Changhua Girls' Senior High School](https://osm.org/node/12141739443)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Carrefour Station](https://osm.org/node/12141739444)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Nan Hsing Elementary School](https://osm.org/node/12141739445)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Train Station Rear Station](https://osm.org/node/12141739446)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Show Chwan Memorial Hospital](https://osm.org/node/12141739447)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Arts High School](https://osm.org/node/12141739448)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Changhua Senior High School](https://osm.org/node/12141739449)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Ping-He Elementary School](https://osm.org/node/12141739450)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Minzu Road Lane 219 Intersection](https://osm.org/node/12141739451)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Public Health Bureau](https://osm.org/node/12141739452)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Chang'an & Zhonghua Intersection (Changhua Christian Hospital Chunghua Road Campus)](https://osm.org/node/12141739453)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Government Second Office Building](https://osm.org/node/12141739454)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yanping Park (North Side)](https://osm.org/node/12141739455)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Christian Hospital](https://osm.org/node/12141739456)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Chung-Hsiao Elementary School](https://osm.org/node/12141739457)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Chienkuo Technology University](https://osm.org/node/12141739458)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [ChingCheng High School](https://osm.org/node/12141739459)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yangming Junior High School](https://osm.org/node/12141739460)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Hanming Christian Hospital](https://osm.org/node/12141739461)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [ChangSing Junior High School](https://osm.org/node/12141739462)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Changhua University of Education](https://osm.org/node/12141739463)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [The Affiliated Industrial Vocational High School of National Changhua University of Education](https://osm.org/node/12141739464)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Aborigine's Life Museum](https://osm.org/node/12141739465)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [ChungShan Elementary School](https://osm.org/node/12141739466)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Public Library](https://osm.org/node/12141739467)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Government](https://osm.org/node/12141739468)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Police Department](https://osm.org/node/12141739469)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Chenggong Community Activity Center](https://osm.org/node/12141739470)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Hsin-Yi Public Junior High and Elementary School of Changhua County](https://osm.org/node/12141739471)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [North-South Pipe Music Opera House](https://osm.org/node/12141739472)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Huaxing Park](https://osm.org/node/12141739473)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Tai-He Elementary School](https://osm.org/node/12141739474)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Da-Cheng Elementary School](https://osm.org/node/12141739475)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua Train Station Front Station](https://osm.org/node/12141739476)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [ChengGong Senior High School](https://osm.org/node/12141739477)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Xihu First Public Retail Market](https://osm.org/node/12141739478)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Sihu Senior High School](https://osm.org/node/12141739479)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Xihu Junior High School](https://osm.org/node/12141739480)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Xihu Sugar Factory](https://osm.org/node/12141739481)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Xizhou Forest Park](https://osm.org/node/12141739482)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Kaiyuan Square](https://osm.org/node/12141739483)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Wenkai Elementary School](https://osm.org/node/12141739484)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Wenwu Temple](https://osm.org/node/12141739485)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Township Library](https://osm.org/node/12141739486)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Precinct Changhua County Police Bureau](https://osm.org/node/12141739487)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Lukang Senior High School](https://osm.org/node/12141739488)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Fuxing First Parking](https://osm.org/node/12141739489)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Folk Arts Museum](https://osm.org/node/12141739490)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Library & Art Center](https://osm.org/node/12141739491)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang First Liti Parking](https://osm.org/node/12141739492)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Guihua Lane Artists Village](https://osm.org/node/12141739493)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Lukang Elementary School](https://osm.org/node/12141739494)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua County Tourist Information Center](https://osm.org/node/12141739495)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Huiming Parking](https://osm.org/node/12141739496)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Dah-Chin Commercial and Industrial Vocational High School](https://osm.org/node/12141739497)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [GongShiWu Park](https://osm.org/node/12141739498)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [GongShiSi Park](https://osm.org/node/12141739499)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Shimamura](https://osm.org/node/12141739500)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Nanqu Civil Sports Center](https://osm.org/node/12141762301)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [South Changhua Civil Sports Center](https://osm.org/node/12141762302)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Bai Guo Mountain](https://osm.org/node/12141762303)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [ZhangShiYuan Station](https://osm.org/node/12141762304)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [YuanlinYuan Station](https://osm.org/node/12141762305)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Yuanlin Home-Economics and Commercial Vocational Senior High School](https://osm.org/node/12141762306)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Performance Hall](https://osm.org/node/12141762307)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Bus Station](https://osm.org/node/12141762308)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Changhua District Procecutors Office](https://osm.org/node/12141762309)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Elementary School](https://osm.org/node/12141762310)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Taishan Parking](https://osm.org/node/12141762311)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Elderly Recreation Center](https://osm.org/node/12141762312)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Yuanlin Agricultural and Industrial Vocational High School](https://osm.org/node/12141762313)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Di Yi Market](https://osm.org/node/12141762314)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Post Office](https://osm.org/node/12141762315)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin City Office](https://osm.org/node/12141762316)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Train Station Front Plaza](https://osm.org/node/12141762317)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Zhishan Park](https://osm.org/node/12141762318)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Yuanlin ChungShih Industrial Vocational High School](https://osm.org/node/12141762319)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Yuanlin Precinct Changhua County Police Bureau](https://osm.org/node/12141762320)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Triangle Park](https://osm.org/node/12141762321)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [National Yuanlin Senior High School](https://osm.org/node/12141762322)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Sanhua Park](https://osm.org/node/12141762323)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Sanduo Park](https://osm.org/node/12141762324)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Hedong Elementary School](https://osm.org/node/12141762325)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Daodong Tutorial Academy](https://osm.org/node/12141762326)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Hemei Township Office](https://osm.org/node/12141762327)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Shin Chuang Elementary School](https://osm.org/node/12141762328)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [Wuxi Riverside](https://osm.org/node/12141762329)
  * authentication:contactless
  * authentication:membership_card
  * capacity
  * network:wikidata
  * payment:jcb
* [臺大禮賢樓西側](https://osm.org/node/12243937343)
  * name:en
  * ref
* [National Chung Cheng University (Faculty Housing II)](https://osm.org/node/12322536501)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Chung Cheng University (Administration Building)](https://osm.org/node/12322537301)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dasyue Rd. / Shennong Rd.](https://osm.org/node/12326813701)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Chung Cheng University (Activity Center)](https://osm.org/node/12326813801)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [繽紛之丘(青溪二路)](https://osm.org/node/12422523359)
  * name:en
  * ref
* [Fengming Station (YingGe Rd.)](https://osm.org/node/12434852093)
  * payment:ep_ipass
  * payment:mastercard
* [Fengming Station](https://osm.org/node/12434852096)
  * payment:ep_ipass
  * payment:mastercard
* [Ln. 112, GuoJi 1st Rd.](https://osm.org/node/12434852097)
  * payment:ep_ipass
  * payment:mastercard
* [Feng 7th Rd. & Yingge Rd. Intersection](https://osm.org/node/12434852098)
  * payment:ep_ipass
  * payment:mastercard
* [Puzi City Office](https://osm.org/node/12608955328)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Changhe Temple](https://osm.org/node/12636016535)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shih-liou Elementary School](https://osm.org/node/12636016536)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shiliu Junior High School](https://osm.org/node/12636016537)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yaogou Basketball Court](https://osm.org/node/12636016538)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Futian Suspension Bridge](https://osm.org/node/12636016539)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shihliou Train Station](https://osm.org/node/12636016540)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Labor Recreation Center](https://osm.org/node/12636016541)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jia Dong Vil.](https://osm.org/node/12636016542)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Night Market](https://osm.org/node/12636016543)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Precinct, Yunlin County Police Bureau](https://osm.org/node/12636016544)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Water Resources Park No.2](https://osm.org/node/12636016545)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yunlin Xi Food Plaza](https://osm.org/node/12636016546)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Longtan Community Activity Center](https://osm.org/node/12636016547)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Train Station Rear Station](https://osm.org/node/12636016548)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Yunlin University of Science & Technology (Longtan Road)](https://osm.org/node/12636016549)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Carrefour Station](https://osm.org/node/12636016550)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Douliou Commercial and Business Vocational High School](https://osm.org/node/12636016551)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Cheng Kung University Hospital](https://osm.org/node/12636016552)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Tou-Liu Senior High School](https://osm.org/node/12636016553)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gongcheng Park](https://osm.org/node/12636016554)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhen-Si Elementary School](https://osm.org/node/12636016555)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nansheng Temple](https://osm.org/node/12636016556)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yunlin County Government](https://osm.org/node/12636016557)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [House of Citizen-Memorial Hall of Attendance](https://osm.org/node/12636016558)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zhongshan Memorial Park](https://osm.org/node/12636016559)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yunlin County Stadium](https://osm.org/node/12636016560)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Yunlin University of Science & Technology](https://osm.org/node/12636016561)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuian Riverside Park](https://osm.org/node/12636016562)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dou Liou Junior High School](https://osm.org/node/12636016563)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Baseball Stadium](https://osm.org/node/12636016564)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [蔦松藝術高中](https://osm.org/node/12636016565)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [番薯村東營公廟](https://osm.org/node/12636016566)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [水林鄉運動公園](https://osm.org/node/12636016567)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [湖仔內農塘公園](https://osm.org/node/12636016568)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [顏思齊故事館](https://osm.org/node/12636016569)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [水林通天府](https://osm.org/node/12636016570)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [水林鄉海埔活動中心](https://osm.org/node/12636016571)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [水林鄉公所](https://osm.org/node/12636016572)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [小雨燕](https://osm.org/node/12636016573)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [文光國小](https://osm.org/node/12636016574)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [下崙福安宮](https://osm.org/node/12636016575)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [興安代天府](https://osm.org/node/12636016576)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [口湖成龍安龍宮](https://osm.org/node/12636016577)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [湖口濕地(椬梧滯洪池)](https://osm.org/node/12636016578)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [口湖遊客中心](https://osm.org/node/12636016579)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [鄭豐喜紀念圖書館](https://osm.org/node/12636016580)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [馬光國中](https://osm.org/node/12636016581)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [馬光順安宮](https://osm.org/node/12636016582)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫高商](https://osm.org/node/12636016583)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫中山堂](https://osm.org/node/12636016584)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [後埔國小](https://osm.org/node/12636016585)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫鎮農會](https://osm.org/node/12636016586)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫國中](https://osm.org/node/12636016587)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫順天宮](https://osm.org/node/12636016588)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫長青公園](https://osm.org/node/12636016589)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [土庫鎮公所](https://osm.org/node/12636016590)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港圓環](https://osm.org/node/12636016591)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [雲林地方法院北港簡易庭](https://osm.org/node/12636016592)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [同仁夜市](https://osm.org/node/12636016593)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港文化中心](https://osm.org/node/12636016594)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [新厝里受天宮](https://osm.org/node/12636016595)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港鎮公所](https://osm.org/node/12636016596)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [家樂福北港店](https://osm.org/node/12636016597)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [嘉義客運(北港站)](https://osm.org/node/12636016598)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港兒十一公園](https://osm.org/node/12636016599)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港遊客中心](https://osm.org/node/12636016600)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港國中](https://osm.org/node/12636035701)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港武德宮](https://osm.org/node/12636035702)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港高中](https://osm.org/node/12636035703)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港農工](https://osm.org/node/12636035704)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港媽祖醫院](https://osm.org/node/12636035705)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港果菜市場](https://osm.org/node/12636035706)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港溪畔河堤停車場](https://osm.org/node/12636035707)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北港觀光大橋](https://osm.org/node/12636035708)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾帥興宮](https://osm.org/node/12636035709)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾公安路立體停車場](https://osm.org/node/12636035710)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [平和里](https://osm.org/node/12636035711)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [延平老街停車場(蝴蝶公園臨時站)](https://osm.org/node/12636035712)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Douliou Commercial and Business Vocational High School 2](https://osm.org/node/12636035713)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [新興路](https://osm.org/node/12636035714)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [新社路](https://osm.org/node/12636035715)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六名門](https://osm.org/node/12636035716)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [福懋一廠](https://osm.org/node/12636035717)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [福懋二廠](https://osm.org/node/12636035718)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [雲林縣政府(文化觀光處)](https://osm.org/node/12636035719)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [東市場停車場](https://osm.org/node/12636035720)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [頂柴福德宮](https://osm.org/node/12636035721)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [瓦厝玄安宮](https://osm.org/node/12636035722)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [江厝路](https://osm.org/node/12636035723)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [九老爺(久安南路)](https://osm.org/node/12636035724)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [保長路](https://osm.org/node/12636035725)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [保竹李天宮](https://osm.org/node/12636035726)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [崙仔集會所](https://osm.org/node/12636035727)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾溪福龍宮](https://osm.org/node/12636035728)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎溪豐泰](https://osm.org/node/12636035729)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [長平里](https://osm.org/node/12636035730)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [長安派出所](https://osm.org/node/12636035731)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [古坑綠色隧道公園](https://osm.org/node/12636035732)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [龍潭鄰里公園](https://osm.org/node/12636035733)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六進天宮](https://osm.org/node/12636035734)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [保長國小](https://osm.org/node/12636035735)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六保清宮](https://osm.org/node/12636035736)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [明德北路與保源六街路口](https://osm.org/node/12636035737)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [南京西路](https://osm.org/node/12636035738)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六德龍宮](https://osm.org/node/12636035739)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六玄太宮(將爺廟)](https://osm.org/node/12636035740)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [福興路](https://osm.org/node/12636035741)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [德安社區](https://osm.org/node/12636035742)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [榴中福德宮](https://osm.org/node/12636035743)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [十三里社區多功能活動中心](https://osm.org/node/12636035744)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [中南里](https://osm.org/node/12636035745)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [萬年庄](https://osm.org/node/12636035746)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [明德里社區多功能活動中心](https://osm.org/node/12636035747)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六消防局第一大隊斗六消防分局](https://osm.org/node/12636035748)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六永樂公園](https://osm.org/node/12636035749)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六文德公園](https://osm.org/node/12636035750)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六鎮東公園](https://osm.org/node/12636035751)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六承天宮](https://osm.org/node/12636035752)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [菜公庄](https://osm.org/node/12636035753)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [波麗士公園](https://osm.org/node/12636035754)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗六大智公園](https://osm.org/node/12636035755)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [鎮南國小](https://osm.org/node/12636035756)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [雲林國中](https://osm.org/node/12636035757)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [雲林故事館(擴站)](https://osm.org/node/12636035758)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [三合里(天乙福德宮)](https://osm.org/node/12636035759)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [同心公園](https://osm.org/node/12636035760)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾圓環](https://osm.org/node/12636035761)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾鎮公兒六公園](https://osm.org/node/12636035762)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾嘉南大圳自行車道糖鐵路口](https://osm.org/node/12636035763)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾嘉南大圳自行車道光復路口](https://osm.org/node/12636035764)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾嘉南大圳自行車道林森路口](https://osm.org/node/12636035765)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾嘉南大圳自行車道弘道路口](https://osm.org/node/12636035766)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾嘉南大圳自行車道立德路口](https://osm.org/node/12636035767)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [大成商工](https://osm.org/node/12636035768)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [興南里活動中心](https://osm.org/node/12636035769)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [中華特區](https://osm.org/node/12636035770)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [北溪厝龍安宮](https://osm.org/node/12636035771)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [新吉里](https://osm.org/node/12636035772)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾立仁國小](https://osm.org/node/12636035773)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾科技大學(八德街)](https://osm.org/node/12636035774)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾科技大學(文化路)](https://osm.org/node/12636035775)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾安慶國小](https://osm.org/node/12636035776)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [中華電信虎尾服務中心](https://osm.org/node/12636035777)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [法務部矯正署雲林第二監獄](https://osm.org/node/12636035778)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [堀頭里社區活動中心](https://osm.org/node/12636035779)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [興隆毛巾觀光工廠](https://osm.org/node/12636035780)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [虎尾安溪清雲宮](https://osm.org/node/12636035781)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [興中社區活動中心](https://osm.org/node/12636035782)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [廉使社區活動中心](https://osm.org/node/12636035783)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [學府一路與松園路口](https://osm.org/node/12636035784)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗南車站前站](https://osm.org/node/12636035785)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [僑真國小](https://osm.org/node/12636035786)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [雙鐵綠廊(延平路二段554巷)](https://osm.org/node/12636035787)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗南順安宮](https://osm.org/node/12636035788)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗南夜市](https://osm.org/node/12636035789)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗南田徑場](https://osm.org/node/12636035790)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [斗南國小](https://osm.org/node/12636035791)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [西螺鎮農會](https://osm.org/node/12636035792)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [西螺森活武樹園區](https://osm.org/node/12636035793)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [西螺大橋公園](https://osm.org/node/12636035794)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [中山路公有收費停車場](https://osm.org/node/12636035795)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [西螺轉運站](https://osm.org/node/12636035796)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [西螺鎮公所](https://osm.org/node/12636035797)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [振文公園](https://osm.org/node/12636035798)
  * capacity
  * name:en
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Leinei Vil. (Gongyun Temple)](https://osm.org/node/12636035799)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Xiaxi Community Activity Center](https://osm.org/node/12636035800)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dingxi Community Sports and Leisure Center](https://osm.org/node/12636035801)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Junior High School](https://osm.org/node/12636035802)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Anxi Community Activity Center](https://osm.org/node/12636035803)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dongren Junior High School](https://osm.org/node/12636035804)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Hu-Wei Senior High School](https://osm.org/node/12636035805)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Formosa University (Jing Guo Stadium)](https://osm.org/node/12636035806)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Formosa University (First Student Dormitory)](https://osm.org/node/12636035807)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yang-Tze High School](https://osm.org/node/12636035808)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Da-Tun Elementary School](https://osm.org/node/12636035809)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taiwan High Speed Rail Yunlin Station](https://osm.org/node/12636035810)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Taiwan University Hospital Yunlin Branch, Hu-Wei Region](https://osm.org/node/12636035811)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nongbo Park](https://osm.org/node/12636035812)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jian Guo Jiuan Tsuen](https://osm.org/node/12636035813)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huweiren Sports Park](https://osm.org/node/12636035814)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Town Nursery School](https://osm.org/node/12636035815)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Huwei Agricultural and Industrial Vocational Senior High School](https://osm.org/node/12636035816)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Japanese-style Old Water Tower](https://osm.org/node/12636035817)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yunlin Storyhouse](https://osm.org/node/12636035818)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Township Office](https://osm.org/node/12636035819)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Town Multifunctional Activity Center](https://osm.org/node/12636035820)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Township Library](https://osm.org/node/12636035821)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Huwei Ping-He Elementary School](https://osm.org/node/12636035822)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Douliu Industrial Park Service Center](https://osm.org/node/12636035823)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Sacred Hearts High School](https://osm.org/node/12636035824)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jiuan Vil. (Sanding Temple)](https://osm.org/node/12636035825)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Lunfeng Vil.](https://osm.org/node/12636035826)
  * capacity
  * network:wikidata
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dakuwei Bus Station](https://osm.org/node/12640028849)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nanjing Xinfa Temple](https://osm.org/node/12640028850)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Longde Vil. Fude Temple](https://osm.org/node/12640028851)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Wuzhuzijiao Bus Station](https://osm.org/node/12640028852)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dalun Sinsing Temple](https://osm.org/node/12640028853)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Kuanshi Vil.](https://osm.org/node/12640028854)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Wuzhong Gong](https://osm.org/node/12640028855)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zhongzhuang Vil. Community Center](https://osm.org/node/12640028856)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Xidong Park](https://osm.org/node/12640028857)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Deyuan Temple](https://osm.org/node/12640028858)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yising Vil. Park](https://osm.org/node/12640028859)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Wailin Fongan Temple](https://osm.org/node/12640028860)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dalun Bus Station](https://osm.org/node/12640028861)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Junior High School](https://osm.org/node/12640028862)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Kuti Temple Bus Station](https://osm.org/node/12640028863)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Waixizhou Basketball Court Under The Bridge](https://osm.org/node/12640028864)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nanjing Station](https://osm.org/node/12640028865)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shumu Park](https://osm.org/node/12640028866)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Wan-Neng Senior Industrial & Commercial Vocational School](https://osm.org/node/12640028867)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Land Office](https://osm.org/node/12640028868)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Township Public Health Center](https://osm.org/node/12640028869)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Township Zhongxing Rd. Parking Lot](https://osm.org/node/12640028870)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Township Office](https://osm.org/node/12640028871)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Township Min Sheng Park](https://osm.org/node/12640028872)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Shuishang Station](https://osm.org/node/12640028873)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Solar Exploration Center](https://osm.org/node/12640028874)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Solar Exploration Center 2](https://osm.org/node/12640028875)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Wufong Park](https://osm.org/node/12640028876)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zhongpu Vil. Community Center](https://osm.org/node/12640028877)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zhongpu Visitor Center](https://osm.org/node/12640028878)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Hakka Culture Museum](https://osm.org/node/12640028879)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongpu Township Office](https://osm.org/node/12640028880)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongpu Tangye](https://osm.org/node/12640028881)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gongguan Park](https://osm.org/node/12640028882)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongpu Gong 9 Park](https://osm.org/node/12640028883)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongshan Rd. / Jhongyi Rd.](https://osm.org/node/12640028884)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongpu Sport Park](https://osm.org/node/12640028885)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Hemei Rd./ Singfu 1st St.](https://osm.org/node/12640028886)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Hemei Rd./ Renyi Rd. (Southwest)](https://osm.org/node/12640028887)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dayi Rd. / Jhongyi Rd. (Southeast)](https://osm.org/node/12640028888)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jhongpu Township Farmer's Association Hemei Office](https://osm.org/node/12640028889)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Xiduolang 15 Manor community](https://osm.org/node/12640028890)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Xuande Temple](https://osm.org/node/12640028891)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jingpu Community Center](https://osm.org/node/12640028892)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dasyue Rd. / Singnong Rd.](https://osm.org/node/12640028893)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Beishizih Park](https://osm.org/node/12640028894)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Fuxing Vil. Huaxing Bridge](https://osm.org/node/12640028895)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Fuyue Park](https://osm.org/node/12640028896)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Hostel](https://osm.org/node/12640028897)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zhongzheng Dazhen Community Center](https://osm.org/node/12640028898)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Wufeng University](https://osm.org/node/12640028899)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Township Office](https://osm.org/node/12640028900)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dongrong Primary School](https://osm.org/node/12640062101)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi Performing Arts Center](https://osm.org/node/12640062102)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Zuoan Park](https://osm.org/node/12640062103)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National MinShyong Vocational High School of Agriculture & Industry](https://osm.org/node/12640062104)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [National Chiayi University (Minsyong Campus)](https://osm.org/node/12640062105)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Township Sports Park](https://osm.org/node/12640062106)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Township Zaoan Park](https://osm.org/node/12640062107)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Station (Rear Station)](https://osm.org/node/12640062108)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Minsyong Station (Front Station)](https://osm.org/node/12640062109)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Cingjiang Park](https://osm.org/node/12640062110)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Sinji Jhuang Dean Temple](https://osm.org/node/12640062111)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Sinjiayu](https://osm.org/node/12640062112)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi City Senior Citizens’ Activity Center](https://osm.org/node/12640062113)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi City Embroidery Culture Center](https://osm.org/node/12640062114)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Elementary school](https://osm.org/node/12640062115)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi Chang Gung Memorial Hospital (Dormitory)](https://osm.org/node/12640062116)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gaoming Temple](https://osm.org/node/12640062117)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nantong Rd./ Zhongxing Rd.](https://osm.org/node/12640062118)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Datong Elementary school](https://osm.org/node/12640062119)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gonger 6th Environmental Park](https://osm.org/node/12640062120)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Bohou Vil.、YongHe Vil. Assembly Hall](https://osm.org/node/12640062121)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dakanglang Bus Station](https://osm.org/node/12640062122)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jixiang 3rd St Park](https://osm.org/node/12640062123)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Jinjhen Library](https://osm.org/node/12640062124)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Railway Station Park](https://osm.org/node/12640062125)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzih Railway Park (Nantung Rd.)](https://osm.org/node/12640062126)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzih Railway Park (Bowun St.)](https://osm.org/node/12640062127)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Changgeng Medical Building](https://osm.org/node/12640062128)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Hospital](https://osm.org/node/12640062129)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Junior High School](https://osm.org/node/12640062130)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Nantong Rd. / Kaiyuan Rd.](https://osm.org/node/12640062131)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi Bus Station](https://osm.org/node/12640062132)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Pei Ten Temple Parking Lot](https://osm.org/node/12640062133)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Tung Shih Senior High School](https://osm.org/node/12640062134)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Tung Shih Junior High School](https://osm.org/node/12640062135)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Puzi City Art Park](https://osm.org/node/12640062136)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chang Gung Memorial Hospital (Bus Station)](https://osm.org/node/12640062137)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Government (Bus Station) South Side](https://osm.org/node/12640062138)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Government (Bus Station) North Side](https://osm.org/node/12640062139)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jiabu Rd. W. Sec. / Puzi 3rd Rd.](https://osm.org/node/12640062140)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Track and Field Stadium (Puzi 3rd Rd.)](https://osm.org/node/12640062141)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi Motor Vehicles Office](https://osm.org/node/12640062142)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Government Plaza](https://osm.org/node/12640062143)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Hongdu Panshih Community](https://osm.org/node/12640062144)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Human Resources Development Center](https://osm.org/node/12640062145)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Culture & Tourism Bureau of Chiayi County](https://osm.org/node/12640062146)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Dongshih Park](https://osm.org/node/12640062147)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jiabu Rd.E.Sec / Taibao 2nd Rd.](https://osm.org/node/12640062148)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Beiji Temple](https://osm.org/node/12640062149)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Laborer Community (Bus Station)](https://osm.org/node/12640062150)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Matai Rd. / Sanhe Rd.](https://osm.org/node/12640062151)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Gugong Blvd. / Taizih Blvd. (Southeast)](https://osm.org/node/12640062152)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Matai Rd. / Beigang Rd.](https://osm.org/node/12640062153)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Southern Branch of the National Palace Museum (Sugar Train Station)](https://osm.org/node/12640062154)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Southern Branch of the National Palace Museum (Visitor Center)](https://osm.org/node/12640062155)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao City Arts Center](https://osm.org/node/12640062156)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Baseball Field](https://osm.org/node/12640062157)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao City Tongxin Park](https://osm.org/node/12640062158)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao Futian Temple](https://osm.org/node/12640062159)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Yungching Senior High School](https://osm.org/node/12640062160)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [HSR Chiayi Station (Baotie 6th Rd.)](https://osm.org/node/12640062161)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Farglory Guobao Community](https://osm.org/node/12640062162)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chiayi County Baseball field](https://osm.org/node/12640062163)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao 5th Rd. / Xianfu 3rd St.](https://osm.org/node/12640062164)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao City Dongshih Community Center](https://osm.org/node/12640062165)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao Primary School](https://osm.org/node/12640062166)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Chunjhu Village Community Center](https://osm.org/node/12640062167)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao City The Traditional Market](https://osm.org/node/12640062168)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Taibao City Office](https://osm.org/node/12640062169)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [Jiasin Junior High School](https://osm.org/node/12640062170)
  * payment:jcb
  * payment:mastercard
  * payment:visa
* [NanHsin Primary School](https://osm.org/node/12640062171)
  * payment:jcb
  * payment:mastercard
  * payment:visa

<!--END-->
