---
title: "Koordinierungsstelle Stolpersteine Berlin"
date: 2024-09-09T20:00:00+02:00
lastmod: 2024-09-09T20:00:00+02:00
tags: [Memorials]
areas: [Germany]
---

<!--more-->

* URL: https://www.stolpersteine-berlin.de/
* License: unknown 🤷
  * Compatible with OpenStreetMap: unknown
