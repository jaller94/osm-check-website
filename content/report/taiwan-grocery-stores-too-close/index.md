---
title: "Duplicate grocery stores in Taiwan by their position"
date: 2024-04-08T22:00:00+08:00
lastmod: 2025-03-10T19:59:40.796Z
tags: [Businesses]
areas: [Taiwan]
datasets: [osm-grocery-stores]
toc: true
---

Do chain grocery stores without a ref=* value have no nearby duplicates?

<!--START-->
⚠️ No, there are 55 stores too close to another.

<!--more-->

## Downloads
* [All duplicates as GPX](taiwan-grocery-stores-too-close.gpx)

## Issues

### 7-Eleven (28)
#### 0.00 meters apart
* [7\-Eleven](https://osm.org/node/12392903002)
* [7\-Eleven](https://osm.org/node/12392907723)
#### 0.00 meters apart
* [7\-Eleven](https://osm.org/node/12528652668)
* [7\-Eleven](https://osm.org/node/12528684871)
#### 0.00 meters apart
* [7\-Eleven](https://osm.org/node/12528652668)
* [7\-Eleven](https://osm.org/node/12528699259)
#### 0.00 meters apart
* [7\-Eleven](https://osm.org/node/12528684871)
* [7\-Eleven](https://osm.org/node/12528699259)
#### 3.46 meters apart
* [7\-Eleven](https://osm.org/node/3997437762)
* [7\-Eleven](https://osm.org/way/1103147812)
#### 4.74 meters apart
* [7\-Eleven](https://osm.org/way/1245019475)
* [7\-Eleven](https://osm.org/node/2179271487)
#### 5.47 meters apart
* [7\-Eleven](https://osm.org/node/4377177486)
* [7\-Eleven](https://osm.org/node/12183722130)
#### 7.95 meters apart
* [7\-Eleven](https://osm.org/way/1213001975)
* [7\-Eleven](https://osm.org/node/2291329683)
#### 8.26 meters apart
* [7\-Eleven](https://osm.org/node/10076653789)
* [7\-Eleven](https://osm.org/node/10053402392)
#### 15.27 meters apart
* [7\-Eleven](https://osm.org/node/6112609212)
* [7\-Eleven](https://osm.org/node/7694301751)
#### 15.92 meters apart
* [7\-Eleven](https://osm.org/node/3849103543)
* [7\-Eleven](https://osm.org/node/4570262479)
#### 16.00 meters apart
* [7\-Eleven 福林店](https://osm.org/way/617553270)
* [7\-Eleven](https://osm.org/node/5933187017)
#### 18.79 meters apart
* [7\-Eleven](https://osm.org/node/1487087835)
* [7\-Eleven](https://osm.org/way/588277337)
#### 22.20 meters apart
* [7\-Eleven](https://osm.org/node/7255209815)
* [7\-Eleven](https://osm.org/node/4354215432)
#### 24.70 meters apart
* [7\-Eleven](https://osm.org/node/3621163360)
* [7\-Eleven](https://osm.org/node/4256696957)
#### 27.24 meters apart
* [7\-Eleven](https://osm.org/node/8118095197)
* [7\-Eleven](https://osm.org/node/11361169584)
#### 28.27 meters apart
* [7\-Eleven](https://osm.org/node/2358058423)
* [7\-Eleven](https://osm.org/node/2859959519)
#### 30.12 meters apart
* [7\-Eleven](https://osm.org/node/2293386990)
* [7\-Eleven](https://osm.org/node/2293387324)
#### 31.05 meters apart
* [7\-Eleven](https://osm.org/node/1839331330)
* [7\-Eleven](https://osm.org/node/8118206928)
#### 31.45 meters apart
* [7\-Eleven](https://osm.org/node/5266028752)
* [7\-Eleven](https://osm.org/node/7759792510)
#### 31.47 meters apart
* [7\-Eleven](https://osm.org/node/3245714479)
* [7\-Eleven](https://osm.org/node/4551202897)
#### 32.75 meters apart
* [7\-Eleven](https://osm.org/node/2124843041)
* [7\-Eleven](https://osm.org/node/2124843042)
#### 34.43 meters apart
* [7\-Eleven](https://osm.org/node/4360429228)
* [7\-Eleven](https://osm.org/node/11394010230)
#### 34.48 meters apart
* [7\-Eleven](https://osm.org/way/383636297)
* [7\-Eleven](https://osm.org/node/3869466883)
#### 34.68 meters apart
* [7\-Eleven](https://osm.org/node/1839327973)
* [7\-Eleven](https://osm.org/node/6310130886)
#### 35.41 meters apart
* [7\-Eleven](https://osm.org/node/2302317261)
* [7\-Eleven](https://osm.org/node/2302317276)
#### 36.69 meters apart
* [7\-Eleven](https://osm.org/node/4538275036)
* [7\-Eleven](https://osm.org/node/2776702819)
#### 36.94 meters apart
* [7\-Eleven](https://osm.org/node/843415677)
* [7\-Eleven](https://osm.org/node/2619667223)

### FamilyMart (17)
#### 0.01 meters apart
* [FamilyMart](https://osm.org/node/12096415491)
* [FamilyMart](https://osm.org/node/12096467783)
#### 14.15 meters apart
* [FamilyMart](https://osm.org/node/8118272890)
* [FamilyMart](https://osm.org/node/6417187088)
#### 14.62 meters apart
* [FamilyMart](https://osm.org/way/383635710)
* [FamilyMart](https://osm.org/node/3869466961)
#### 15.52 meters apart
* [FamilyMart](https://osm.org/way/1319837811)
* [FamilyMart](https://osm.org/node/11390926419)
#### 17.23 meters apart
* [FamilyMart](https://osm.org/node/12438080537)
* [FamilyMart](https://osm.org/node/5451321101)
#### 17.42 meters apart
* [FamilyMart](https://osm.org/node/8118272919)
* [FamilyMart](https://osm.org/way/929918158)
#### 17.88 meters apart
* [FamilyMart](https://osm.org/node/8118272827)
* [FamilyMart](https://osm.org/node/10074104090)
#### 18.15 meters apart
* [FamilyMart](https://osm.org/node/1260947927)
* [FamilyMart](https://osm.org/node/5160470722)
#### 18.58 meters apart
* [FamilyMart](https://osm.org/node/2367268690)
* [FamilyMart](https://osm.org/way/1112214391)
#### 21.81 meters apart
* [FamilyMart](https://osm.org/node/8118272872)
* [FamilyMart](https://osm.org/node/10057481775)
#### 23.63 meters apart
* [FamilyMart](https://osm.org/node/10076653810)
* [FamilyMart](https://osm.org/node/6184687485)
#### 23.83 meters apart
* [FamilyMart](https://osm.org/node/2473958421)
* [FamilyMart](https://osm.org/node/8118272841)
#### 29.35 meters apart
* [FamilyMart](https://osm.org/node/3541338394)
* [FamilyMart](https://osm.org/node/7839827874)
#### 33.56 meters apart
* [FamilyMart](https://osm.org/node/12104212437)
* [FamilyMart](https://osm.org/node/11182179210)
#### 33.59 meters apart
* [FamilyMart](https://osm.org/node/10131697264)
* [FamilyMart](https://osm.org/node/10131698219)
#### 35.49 meters apart
* [FamilyMart](https://osm.org/node/615736716)
* [FamilyMart](https://osm.org/node/8118272967)
#### 35.55 meters apart
* [FamilyMart](https://osm.org/node/4524898420)
* [FamilyMart](https://osm.org/node/6176549690)

### Hi-Life (4)
#### 11.71 meters apart
* [Hi\-Life](https://osm.org/node/6627300475)
* [Hi\-Life](https://osm.org/way/470485407)
#### 16.89 meters apart
* [Hi\-Life](https://osm.org/node/3797651233)
* [Hi\-Life](https://osm.org/way/929935122)
#### 20.49 meters apart
* [Hi\-Life](https://osm.org/node/3232474107)
* [Hi\-Life](https://osm.org/node/3232573077)
#### 29.44 meters apart
* [Hi\-Life](https://osm.org/node/7699945609)
* [Hi\-Life](https://osm.org/node/7700005286)

### OK Mart (3)
#### 3.41 meters apart
* [OK mart](https://osm.org/way/703341342)
* [OK mart](https://osm.org/node/6604982519)
#### 4.10 meters apart
* [Circle K](https://osm.org/node/4524863674)
* [OK mart](https://osm.org/node/2672538244)
#### 21.89 meters apart
* [OK mart](https://osm.org/node/4410823893)
* [Ok便利商店](https://osm.org/node/6051249619)

### PxMart (0)

### Simple Mart (3)
#### 4.82 meters apart
* [Simple Mart](https://osm.org/node/4647782989)
* [Simple Mart](https://osm.org/node/10053402399)
#### 20.99 meters apart
* [Simple Mart](https://osm.org/node/4584753701)
* [Simple Mart](https://osm.org/node/7916358957)
#### 32.36 meters apart
* [Simple Mart](https://osm.org/node/5761673544)
* [Simple Mart](https://osm.org/node/7916339582)

<!--END-->
