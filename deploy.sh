#!/bin/sh
set -e
HUGO_ENVIRONMENT=production hugo --cleanDestinationDir --minify
rsync -avzP --delete ./public/ chrpaul.de:~/website/osm-check.chrpaul.de/
