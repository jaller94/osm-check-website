---
title: "Stolpersteine in Berlin"
date: 2024-09-16T20:00:00+02:00
lastmod: 2025-03-10T19:59:44.552Z
tags: [Memorials]
areas: [Germany]
datasets: [stolpersteine-berlin]
---

<!--START-->
Eine Analyse von Stolpersteinen auf OpenStreetMap in Berlin.
<!--more-->
* 3433 Stolpersteine
## Fehlende Tags
* 16 ohne Namen
* 628 ohne Inschrift (`inscription`)
* 860 ohne Webseite
## Großartige Tags
* 6 verlinken auf einen Wikipedia-Artikel
* 295 verlinken einen Wikidata-Eintrag zum Stolperstein
* 106 verlinken einen Wikidata-Eintrag zur Person
## Unerwünschte Tags
* 2 haben eine URL (bitte `website` oder `image` benutzen)
* 179 hat einen `addr:*`-Wert (bitte `memorial:addr:*` verwenden)
## Kein Name
* [1769856845](https://osm.org/node/1769856845)
* [1769856850](https://osm.org/node/1769856850)
* [1769856857](https://osm.org/node/1769856857)
* [1769856859](https://osm.org/node/1769856859)
* [1769856860](https://osm.org/node/1769856860)
* [3531015207](https://osm.org/node/3531015207)
* [5176773541](https://osm.org/node/5176773541)
* [6299277587](https://osm.org/node/6299277587)
* [9676046187](https://osm.org/node/9676046187)
* [12044069290](https://osm.org/node/12044069290)
* [12044072806](https://osm.org/node/12044072806)
* [12269162736](https://osm.org/node/12269162736)
* [12269171697](https://osm.org/node/12269171697)
* [12329220493](https://osm.org/node/12329220493)
* [12389823159](https://osm.org/node/12389823159)
* [12389823160](https://osm.org/node/12389823160)
## Nach Webseite gruppiert
* https://www.stolpersteine.eu/ (2 mal)
  * [Fam. Jacoby](https://osm.org/node/10670080334)
  * [Fam. Adler](https://osm.org/node/10674077333)
## Nach URL gruppiert

<!--END-->
