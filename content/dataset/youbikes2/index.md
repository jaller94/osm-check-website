---
title: "YouBikes 2.0"
date: 2024-03-23T00:50:00+08:00
lastmod: 2024-03-23T02:15:00+08:00
tags: [YouBike, Public Transport]
---

<!--more-->

* URL: https://apis.youbike.com.tw/json/station-yb2.json
* License: unknown 🤷
  * Compatible with OpenStreetMap: no
