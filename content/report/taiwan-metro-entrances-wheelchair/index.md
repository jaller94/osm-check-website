---
title: "Wheelchair accessibility on all Metro entrances"
date: 2024-03-22T02:00:00+08:00
lastmod: 2025-03-10T19:59:26.581Z
tags: [Metro, Public Transport, Wheelchair access]
areas: [Taiwan]
datasets: [osm-taiwan-subway]
---

Does every Metro station entrance have its wheelchair accessibility mapped?

🏗️ This page is new or broken. It's likely inaccurate.

<!--more-->
<!--START-->
⚠️ No, 405 station entrances have no wheelchair=* tag.

<!--more-->

## Downloads
* [As GPX](taiwan-metro-entrances-missing-wheelchair.gpx)

## Issues
* [3號出入口](https://osm.org/node/533725015)
* [側門](https://osm.org/node/632564723)
* [捷運昆陽站 (4號出口)](https://osm.org/node/870131998)
* [1號出入口](https://osm.org/node/1022709451)
* [3號出入口](https://osm.org/node/1022709516)
* [2號出入口](https://osm.org/node/1022709544)
* [1號出入口](https://osm.org/node/1166364341)
* [1號出入口](https://osm.org/node/1179035791)
* [2](https://osm.org/node/1224128466)
* [4號出入口](https://osm.org/node/1270318446)
* [3號出入口](https://osm.org/node/1270318447)
* [7號出入口](https://osm.org/node/1270318449)
* [4號出入口](https://osm.org/node/1270318450)
* [3號出入口](https://osm.org/node/1270318451)
* [5號出入口](https://osm.org/node/1270318452)
* [2號出入口](https://osm.org/node/1270318457)
* [1號出入口](https://osm.org/node/1270318459)
* [3號出入口](https://osm.org/node/1272252944)
* [2號出入口](https://osm.org/node/1272252946)
* [Entrance 7](https://osm.org/node/1272252949)
* [2號出入口](https://osm.org/node/1272252950)
* [10號出入口](https://osm.org/node/1272252956)
* [Entrance 6](https://osm.org/node/1272252957)
* [9號出入口](https://osm.org/node/1272252959)
* [2號出入口](https://osm.org/node/1272252962)
* [1號出入口](https://osm.org/node/1272252967)
* [1號出入口](https://osm.org/node/1272252975)
* [Entrance 5](https://osm.org/node/1272252988)
* [2號出入口](https://osm.org/node/1272253007)
* [3號出入口](https://osm.org/node/1272253013)
* [4號出入口](https://osm.org/node/1272253016)
* [4號出入口](https://osm.org/node/1272253017)
* [Entrance 2](https://osm.org/node/1272253026)
* [Entrance 3](https://osm.org/node/1272253031)
* [11號出入口](https://osm.org/node/1272253034)
* [3號出入口](https://osm.org/node/1272297496)
* [4號出入口](https://osm.org/node/1272297499)
* [2號出入口](https://osm.org/node/1272297518)
* [5號出入口](https://osm.org/node/1272297520)
* [4號出入口](https://osm.org/node/1272297527)
* [3號出入口](https://osm.org/node/1272297531)
* [1號出入口](https://osm.org/node/1272297557)
* [1號出入口](https://osm.org/node/1272297562)
* [2號出入口](https://osm.org/node/1272297565)
* [1號出入口](https://osm.org/node/1272674265)
* [4號出入口](https://osm.org/node/1272674278)
* [1號出入口](https://osm.org/node/1272674281)
* [2號出入口](https://osm.org/node/1272674282)
* [1號出入口](https://osm.org/node/1272674305)
* [1號出入口](https://osm.org/node/1272674309)
* [4號出入口](https://osm.org/node/1272674316)
* [2號出入口](https://osm.org/node/1272674322)
* [3號出入口](https://osm.org/node/1272674336)
* [3號出入口](https://osm.org/node/1272674339)
* [1號出入口](https://osm.org/node/1274273305)
* [4號出入口](https://osm.org/node/1274273310)
* [3號出入口](https://osm.org/node/1274273318)
* [4號出入口](https://osm.org/node/1274273319)
* [3號出入口](https://osm.org/node/1274273329)
* [2號出入口](https://osm.org/node/1274273333)
* [2號出入口](https://osm.org/node/1274273334)
* [4號出入口](https://osm.org/node/1278171336)
* [2號出入口](https://osm.org/node/1278171337)
* [2號出入口](https://osm.org/node/1278171341)
* [2號出入口](https://osm.org/node/1278171343)
* [1號出入口](https://osm.org/node/1278171347)
* [1號出入口](https://osm.org/node/1278171348)
* [6號出入口](https://osm.org/node/1278171351)
* [3號出入口](https://osm.org/node/1278171354)
* [3號出入口](https://osm.org/node/1278171355)
* [3號出入口](https://osm.org/node/1278171362)
* [1號出入口](https://osm.org/node/1278171363)
* [1號出入口](https://osm.org/node/1278171367)
* [5號出入口](https://osm.org/node/1278171374)
* [4號出入口](https://osm.org/node/1278171377)
* [3號出入口](https://osm.org/node/1279923258)
* [2號出入口](https://osm.org/node/1279923269)
* [MRT Sun Yatsen Memorial Hall Station Exit 1](https://osm.org/node/1336186416)
* [MRT Sun Yat-Sen Memorial Hall Station Exit 5](https://osm.org/node/1336186433)
* [二號出口](https://osm.org/node/1448642325)
* [後站出口](https://osm.org/node/2174882509)
* [菜寮站出口3](https://osm.org/node/2325438252)
* [菜寮站出口1](https://osm.org/node/2325438253)
* [菜寮站出口2](https://osm.org/node/2325438255)
* [MRT Touqianzhuang Sta. (Exit 4)](https://osm.org/node/2399952151)
* [MRT Banqiao Station Exit 2](https://osm.org/node/2405739533)
* [出口3](https://osm.org/node/2417265249)
* [三重捷運站1號出口](https://osm.org/node/2457951069)
* [三重捷運站2號出口](https://osm.org/node/2457951075)
* [三重捷運站3號出口](https://osm.org/node/2457951078)
* [MRT Xinpu Station Exit 2](https://osm.org/node/2461753319)
* [MRT Xinpu Station Exit 3](https://osm.org/node/2461753324)
* [MRT Xinpu Station Exit 1](https://osm.org/node/2461753325)
* [MRT Xinpu Station Exit 5](https://osm.org/node/2461753331)
* [no name](https://osm.org/node/2480231491)
* [Yongning MRT station Exit 2](https://osm.org/node/2494951251)
* [Yongning MRT station Exit 1](https://osm.org/node/2494951252)
* [Yongning MRT station Exit 3](https://osm.org/node/2494951253)
* [Yongning MRT station Exit 4](https://osm.org/node/2494951254)
* [no name](https://osm.org/node/2511203020)
* [no name](https://osm.org/node/2511203021)
* [no name](https://osm.org/node/2511203022)
* [MRT Exit 1](https://osm.org/node/2511220967)
* [捷運出入口2](https://osm.org/node/2511224837)
* [捷運出入口1](https://osm.org/node/2511224954)
* [捷運出入口1](https://osm.org/node/2511228131)
* [捷運出入口2](https://osm.org/node/2511228637)
* [no name](https://osm.org/node/2564616739)
* [no name](https://osm.org/node/2564616740)
* [no name](https://osm.org/node/2564616741)
* [no name](https://osm.org/node/2590379147)
* [no name](https://osm.org/node/2590379843)
* [no name](https://osm.org/node/2590381142)
* [MRT Jiangzicui Station Exit 5](https://osm.org/node/2615789340)
* [MRT Jiangzicui Station Exit 6](https://osm.org/node/2615814286)
* [no name](https://osm.org/node/2622770959)
* [前站出口](https://osm.org/node/2631871645)
* [1號出口 (中央北路)](https://osm.org/node/2713678334)
* [MRT Taipei 101/World Trade Center Station Exit 4](https://osm.org/node/2718780534)
* [忠孝敦化站出入口3](https://osm.org/node/2787383688)
* [內湖捷運站1號出口](https://osm.org/node/2806281129)
* [Haishan Station Exit 1](https://osm.org/node/2820050058)
* [皓月樓出入口](https://osm.org/node/2982429251)
* [捷運市府站出口3](https://osm.org/node/3019870848)
* [MRT Taipei City Hall Station Exit 1](https://osm.org/node/3019870850)
* [no name](https://osm.org/node/3034546863)
* [八德路緊急出口](https://osm.org/node/3050253123)
* [新生南路緊急出口](https://osm.org/node/3050255126)
* [七張站1號出口](https://osm.org/node/3065312804)
* [西門地下街5號出入口](https://osm.org/node/3076882277)
* [忠孝敦化站出入口6](https://osm.org/node/3105610982)
* [忠孝敦化站出入口8](https://osm.org/node/3105611480)
* [忠孝敦化站出入口5](https://osm.org/node/3105665740)
* [忠孝敦化站出入口7](https://osm.org/node/3105665933)
* [no name](https://osm.org/node/3105666330)
* [Exit 3](https://osm.org/node/3131389227)
* [出口1](https://osm.org/node/3168240174)
* [出口2](https://osm.org/node/3168240175)
* [捷運小巨蛋站2號出口](https://osm.org/node/3182488912)
* [南京復興站5號出口](https://osm.org/node/3182488913)
* [no name](https://osm.org/node/3182488915)
* [no name](https://osm.org/node/3182488916)
* [捷運小巨蛋站五號出口](https://osm.org/node/3182488919)
* [no name](https://osm.org/node/3182488920)
* [no name](https://osm.org/node/3182488921)
* [8號出口](https://osm.org/node/3182488922)
* [1號出入口](https://osm.org/node/3182565879)
* [5號出入口](https://osm.org/node/3182588061)
* [出口1](https://osm.org/node/3185537679)
* [no name](https://osm.org/node/3186011785)
* [no name](https://osm.org/node/3186011786)
* [no name](https://osm.org/node/3186011787)
* [no name](https://osm.org/node/3186011829)
* [no name](https://osm.org/node/3192472090)
* [no name](https://osm.org/node/3198206532)
* [no name](https://osm.org/node/3198206550)
* [no name](https://osm.org/node/3198206553)
* [no name](https://osm.org/node/3198206554)
* [no name](https://osm.org/node/3216077271)
* [no name](https://osm.org/node/3216077385)
* [Tucheng Station Exit 2](https://osm.org/node/3220179013)
* [Tucheng Station Exit 1](https://osm.org/node/3220179014)
* [Tucheng Station Exit 3](https://osm.org/node/3220179015)
* [後門](https://osm.org/node/3232548565)
* [延平北路緊急出口](https://osm.org/node/3262986269)
* [西門地下街1號出入口](https://osm.org/node/3263840837)
* [西門](https://osm.org/node/3268491750)
* [no name](https://osm.org/node/3270311563)
* [no name](https://osm.org/node/3270313561)
* [no name](https://osm.org/node/3270313764)
* [no name](https://osm.org/node/3270313864)
* [no name](https://osm.org/node/3270313865)
* [no name](https://osm.org/node/3270339880)
* [no name](https://osm.org/node/3270339881)
* [no name](https://osm.org/node/3270343269)
* [MRT Muzha Station Exit 1](https://osm.org/node/3273405873)
* [no name](https://osm.org/node/3273605568)
* [no name](https://osm.org/node/3273605569)
* [MRT Wanfang Community Station Exit 1](https://osm.org/node/3273605570)
* [no name](https://osm.org/node/3273605571)
* [no name](https://osm.org/node/3273605573)
* [no name](https://osm.org/node/3273605575)
* [no name](https://osm.org/node/3273605576)
* [no name](https://osm.org/node/3273605577)
* [no name](https://osm.org/node/3273605578)
* [no name](https://osm.org/node/3273605590)
* [no name](https://osm.org/node/3273605592)
* [no name](https://osm.org/node/3273605593)
* [1](https://osm.org/node/3298764690)
* [2](https://osm.org/node/3298764691)
* [no name](https://osm.org/node/3307234991)
* [no name](https://osm.org/node/3307243384)
* [MRT Jiangzicui Station Exit 1](https://osm.org/node/3343597134)
* [MRT Jiangzicui Station Exit 2](https://osm.org/node/3343597135)
* [MRT Daan Station Exit 4](https://osm.org/node/3355535193)
* [MRT Daan Station Exit 1](https://osm.org/node/3355535194)
* [北3門](https://osm.org/node/3431547428)
* [Fuzhong Station Exit 2](https://osm.org/node/3444714021)
* [Fuzhong Station Exit 1](https://osm.org/node/3444714022)
* [Fuzhong Station Exit 3](https://osm.org/node/3444714023)
* [南出入口](https://osm.org/node/3456860886)
* [no name](https://osm.org/node/3505143594)
* [no name](https://osm.org/node/3505143693)
* [no name](https://osm.org/node/3505143793)
* [no name](https://osm.org/node/3505143993)
* [no name](https://osm.org/node/3505144000)
* [MRT Taipei 101/World Trade Center Station Exit 3](https://osm.org/node/3550277944)
* [MRT Taipei 101/World Trade Center Station Exit 2](https://osm.org/node/3550280123)
* [no name](https://osm.org/node/3550879485)
* [no name](https://osm.org/node/3550879486)
* [no name](https://osm.org/node/3550879487)
* [no name](https://osm.org/node/3550879488)
* [no name](https://osm.org/node/3550879489)
* [no name](https://osm.org/node/3669444089)
* [出口1（松信路）](https://osm.org/node/3689300568)
* [出口2（松山工農）](https://osm.org/node/3689309410)
* [出口1（忠孝東路七段）](https://osm.org/node/3689350529)
* [出口2](https://osm.org/node/3689381738)
* [出口1](https://osm.org/node/3689390420)
* [出口2](https://osm.org/node/3689390421)
* [內湖捷運站2號出口](https://osm.org/node/3699438548)
* [捷運唭哩岸站出口1](https://osm.org/node/3699456778)
* [no name](https://osm.org/node/3699813329)
* [Far Eastern Hospital Exit 2](https://osm.org/node/4085141466)
* [Far Eastern Hospital Exit 3](https://osm.org/node/4085141468)
* [Far Eastern Hospital Exit 1](https://osm.org/node/4085141470)
* [no name](https://osm.org/node/4114252289)
* [MRT Nanshijiao Station Exit 4](https://osm.org/node/4236824111)
* [內湖捷運站1號出口](https://osm.org/node/4311433973)
* [1號出口](https://osm.org/node/4329769450)
* [no name](https://osm.org/node/4330703773)
* [no name](https://osm.org/node/4330703774)
* [1號出口](https://osm.org/node/4330704411)
* [一號出口](https://osm.org/node/4330704425)
* [1號出入口](https://osm.org/node/4349642718)
* [1號出入口](https://osm.org/node/4349642719)
* [2號出入口](https://osm.org/node/4349642720)
* [2號出入口](https://osm.org/node/4349642721)
* [捷運昆陽站(3號出口)](https://osm.org/node/4359016296)
* [Y14](https://osm.org/node/4406419436)
* [Y16](https://osm.org/node/4406419438)
* [東區地下街出入口2](https://osm.org/node/4524864003)
* [東區地下街出入口1](https://osm.org/node/4524864004)
* [工地門](https://osm.org/node/4720474757)
* [出口1](https://osm.org/node/4812440884)
* [no name](https://osm.org/node/4838087777)
* [no name](https://osm.org/node/4838646737)
* [no name](https://osm.org/node/4899551431)
* [no name](https://osm.org/node/4958026454)
* [1號出入口](https://osm.org/node/5021873865)
* [2號出入口](https://osm.org/node/5021873866)
* [3號出入口](https://osm.org/node/5021873867)
* [5號出入口](https://osm.org/node/5021873868)
* [6號出入口](https://osm.org/node/5021873869)
* [7號出入口](https://osm.org/node/5021873870)
* [4號出入口](https://osm.org/node/5023330234)
* [2號出口 (和信醫院)](https://osm.org/node/5283420224)
* [Y20](https://osm.org/node/5318551122)
* [南](https://osm.org/node/5565751597)
* [北](https://osm.org/node/5565751598)
* [MRT Hongshulin Station Exit 1](https://osm.org/node/5832304210)
* [MRT Hongshulin Station Exit 2](https://osm.org/node/5837289609)
* [中正東路二段出口](https://osm.org/node/6138830471)
* [Exit 1](https://osm.org/node/6181694198)
* [MRT Xinpu Station Exit 4](https://osm.org/node/6426139794)
* [MRT Nanshijiao Station Exit3](https://osm.org/node/6482849462)
* [MRT Nanshijiao Station Exit 2](https://osm.org/node/6482849463)
* [MRT Nanshijiao Station Exit 1](https://osm.org/node/6482849464)
* [no name](https://osm.org/node/6681649179)
* [no name](https://osm.org/node/6816860441)
* [no name](https://osm.org/node/6816861288)
* [3號出口](https://osm.org/node/6865431285)
* [no name](https://osm.org/node/6886468241)
* [no name](https://osm.org/node/7417629527)
* [no name](https://osm.org/node/7417629528)
* [no name](https://osm.org/node/7417629543)
* [no name](https://osm.org/node/7417629544)
* [no name](https://osm.org/node/7417629545)
* [no name](https://osm.org/node/7662746686)
* [芝山站2號出口](https://osm.org/node/7707251148)
* [no name](https://osm.org/node/8037678315)
* [no name](https://osm.org/node/8037678316)
* [no name](https://osm.org/node/8037678317)
* [no name](https://osm.org/node/8037700605)
* [no name](https://osm.org/node/8037700606)
* [no name](https://osm.org/node/8037700607)
* [no name](https://osm.org/node/8044369929)
* [no name](https://osm.org/node/8044395053)
* [no name](https://osm.org/node/8044395064)
* [no name](https://osm.org/node/8044417763)
* [no name](https://osm.org/node/8044437776)
* [no name](https://osm.org/node/8044438028)
* [no name](https://osm.org/node/8044442692)
* [no name](https://osm.org/node/8044442693)
* [no name](https://osm.org/node/8059637614)
* [no name](https://osm.org/node/8059637615)
* [no name](https://osm.org/node/8061230347)
* [no name](https://osm.org/node/8061230349)
* [no name](https://osm.org/node/8061243058)
* [no name](https://osm.org/node/8061384206)
* [no name](https://osm.org/node/8064151684)
* [no name](https://osm.org/node/8064152885)
* [no name](https://osm.org/node/8111570615)
* [no name](https://osm.org/node/8124791972)
* [no name](https://osm.org/node/8124791973)
* [no name](https://osm.org/node/8124791974)
* [no name](https://osm.org/node/8124815568)
* [no name](https://osm.org/node/8124815569)
* [no name](https://osm.org/node/8166395977)
* [no name](https://osm.org/node/8166395983)
* [出口1 (頂埔街)](https://osm.org/node/8794571808)
* [no name](https://osm.org/node/8794754595)
* [2號出口](https://osm.org/node/8805718361)
* [no name](https://osm.org/node/9252275766)
* [no name](https://osm.org/node/9373325210)
* [no name](https://osm.org/node/9373325211)
* [no name](https://osm.org/node/9467515364)
* [no name](https://osm.org/node/9651741455)
* [2號入口](https://osm.org/node/9853258083)
* [no name](https://osm.org/node/9952975582)
* [no name](https://osm.org/node/9952975595)
* [台北校區大門](https://osm.org/node/10175134121)
* [no name](https://osm.org/node/10225441146)
* [no name](https://osm.org/node/10225464104)
* [no name](https://osm.org/node/10225464105)
* [no name](https://osm.org/node/10232995004)
* [no name](https://osm.org/node/11081144271)
* [no name](https://osm.org/node/11081144272)
* [no name](https://osm.org/node/11387164678)
* [no name](https://osm.org/node/11387164680)
* [no name](https://osm.org/node/11539784030)
* [no name](https://osm.org/node/11539784031)
* [no name](https://osm.org/node/11671537842)
* [no name](https://osm.org/node/11671537850)
* [no name](https://osm.org/node/11671537876)
* [no name](https://osm.org/node/11671537896)
* [no name](https://osm.org/node/11671537900)
* [Ｂ棟緊急](https://osm.org/node/11671537911)
* [大門](https://osm.org/node/11671537923)
* [no name](https://osm.org/node/11671537951)
* [no name](https://osm.org/node/11671538030)
* [no name](https://osm.org/node/11671638800)
* [no name](https://osm.org/node/11671677539)
* [no name](https://osm.org/node/11671677540)
* [no name](https://osm.org/node/11671677541)
* [no name](https://osm.org/node/11671677555)
* [no name](https://osm.org/node/11671677561)
* [Ｃ棟緊急](https://osm.org/node/11672438091)
* [no name](https://osm.org/node/11672438148)
* [no name](https://osm.org/node/11683491809)
* [no name](https://osm.org/node/11683506830)
* [no name](https://osm.org/node/11683517806)
* [no name](https://osm.org/node/11683517825)
* [no name](https://osm.org/node/11745477141)
* [no name](https://osm.org/node/11745477144)
* [no name](https://osm.org/node/11745529890)
* [no name](https://osm.org/node/11760859751)
* [no name](https://osm.org/node/11775002769)
* [no name](https://osm.org/node/11775002774)
* [no name](https://osm.org/node/11775021622)
* [no name](https://osm.org/node/11802195603)
* [no name](https://osm.org/node/11802195604)
* [no name](https://osm.org/node/11811212312)
* [no name](https://osm.org/node/11811913759)
* [no name](https://osm.org/node/11822096947)
* [no name](https://osm.org/node/11822096948)
* [no name](https://osm.org/node/11822096951)
* [no name](https://osm.org/node/11822096954)
* [no name](https://osm.org/node/11822096956)
* [no name](https://osm.org/node/11822096984)
* [no name](https://osm.org/node/11822096998)
* [no name](https://osm.org/node/11822097004)
* [no name](https://osm.org/node/11822097059)
* [no name](https://osm.org/node/11822097060)
* [no name](https://osm.org/node/11849101154)
* [no name](https://osm.org/node/11865634502)
* [no name](https://osm.org/node/11936467870)
* [no name](https://osm.org/node/11951476068)
* [1號出入口](https://osm.org/node/11983481845)
* [2號出入口](https://osm.org/node/11983481846)
* [北出入口](https://osm.org/node/12176042284)
* [Ｃ棟緊急](https://osm.org/node/12379085319)
* [Fong Ming](https://osm.org/node/12385961679)
* [側門](https://osm.org/node/12407136442)
* [中央](https://osm.org/node/12407136466)
* [西](https://osm.org/node/12407136484)
* [東](https://osm.org/node/12407136485)
* [垃圾場門](https://osm.org/node/12407393155)
* [北側門](https://osm.org/node/12407393158)
* [北大門](https://osm.org/node/12407393175)
* [南大門](https://osm.org/node/12407393183)
* [南側門](https://osm.org/node/12407393189)
* [幼兒園門](https://osm.org/node/12407407832)
* [後門](https://osm.org/node/12407407834)
* [游泳池](https://osm.org/node/12407407837)
* [大門](https://osm.org/node/12407407852)
* [側門](https://osm.org/node/12407407853)
* [no name](https://osm.org/node/12416217860)
* [no name](https://osm.org/node/12416217878)
* [no name](https://osm.org/node/12416217892)
* [no name](https://osm.org/node/12416364103)
* [no name](https://osm.org/node/12416364114)
* [ＡＴＭ](https://osm.org/node/12420712923)
* [大門](https://osm.org/node/12420712926)
* [Y22](https://osm.org/node/12433935207)

<!--END-->
